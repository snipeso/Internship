clear
close all

%%% Set parameters
Parameters
Gender = {Genders{1}(1), Genders{2}(1)}; % gets first letters


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load Trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set general variables
Trials = struct();
Test_Trials_Table = readtable([CSV_Folder, Types{1}, Trials_Filename]);
Tot_Trials = Trials_Per_Condition * (Unique_Conditions + 1); % includes tactile
Tot_Unique_Trials = Trials_Per_Condition * Unique_Conditions; %
Runs = floor(Tot_Trials / (Trials_Per_Condition_Per_Run * (Unique_Conditions + 1) )); % maximum number of Runs to have even conditions

Dummy_Trials_Table = readtable([CSV_Folder, Types{2}, Trials_Filename]);
Dummy_Tot = size(Dummy_Trials_Table, 1);

Test_Trials_Table = [Test_Trials_Table; Dummy_Trials_Table];
Tot_Unique_Trials = Dummy_Tot + Tot_Unique_Trials;
Trials_Per_Condition_Per_Run = Trials_Per_Condition_Per_Run + sum(Dummy_Trials)/Unique_Conditions;

disp(['Creating ' num2str(Runs), ' Runs'])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% split trials into 7 groups, corresponding to conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% randomize all trials
Test_Trials_Table = Test_Trials_Table(randperm(Tot_Unique_Trials), :);

for Indx_G = 1:2 % loop through genders
    
    % guaranteed correct gender trial
    Trials_Table_G = Test_Trials_Table(ismember(Test_Trials_Table.Gender, Indx_G), :);
    
    %%% select trials for each gender to add to each group
    Indx_T_End = [0 0]; % keep track of both test and dummy indexes
    for Indx_C = 1:Unique_Conditions % loop through groups
        
        Trials_C = [];
        for Indx_Type = 1:2 % loop through test and dummy trials
            Trials_Table_Type = Trials_Table_G(Trials_Table_G.Type == Indx_Type, :); % get subset of type by gender
            Tot_Trials_Type = size(Trials_Table_Type, 1);
            
            % range of trials to add to group
            Indx_T_Start = Indx_T_End(Indx_Type) + 1;
            Indx_T_End(Indx_Type) = Indx_T_Start + Tot_Trials_Type/Unique_Conditions - 1;
            
            % append to list of trial indeces
            Trials_C = [Trials_C; Trials_Table_Type.Trial_Number(Indx_T_Start:Indx_T_End(Indx_Type))];
        end
        
        % save to struct
        Trials(Indx_C).(['Trial_Indexes_' Gender{Indx_G}]) = Trials_C;
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% counterbalance and randomize conditions for each group across sessions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% makes an ordered matrix with no repeated values across a row or column
Sessions_Conditions = gallery('circul', 1:Unique_Conditions);

% randomizes that
Sessions_Conditions = Sessions_Conditions(randperm(Unique_Conditions), randperm(Unique_Conditions));




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Randomize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Randomize Runs for each trial
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_C = 1:Unique_Conditions % loops through groups
    
    % loads randomized conditions to the structure
    Trials(Indx_C).Sessions_Conditions = Sessions_Conditions(Indx_C, :);
    
    for Indx_G = 1:2 % loops through genders
        
        % get trials in condition in gender
        Trials_G = Trials(Indx_C).(['Trial_Indexes_', Gender{Indx_G}]);
        Trials_G_Size = size(Trials_G, 1);
        
        
        % assigns each trial to a different Run for each session
        New_Session = repmat([1:Runs]', (Trials_Per_Condition_Per_Run)/2, 1); % template column of runs per trial
        New_Session = New_Session(1:Trials_G_Size); % cutoff at number of trials
        
        Session_Runs = gallery('circul', New_Session);
        Session_Runs = Session_Runs(randperm(Trials_G_Size), randperm(Trials_G_Size)); % randomize
        
        % saves to structure
        Trials(Indx_C).(['Run_Matrix_', Gender{Indx_G}]) = Session_Runs(:, 1:Unique_Conditions); %original was a sqaure, this only takes sesisons needed
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% group trials by sessions and Runs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_S = Sessions_Range % loop through sessions
    PRT = table();
    Indx_Group = Indx_S - Start_Session + 1; % gets one of the seven groups
    
    
    %%% gather test trials
    for Indx_R = 1:Runs % loop through Runs
        for Indx_G = 1:2 % loop through structure
            for Indx_C = 1:Unique_Conditions % loop through groups
                
                % load relevent trials
                Trials_temp = Trials(Indx_C).(['Trial_Indexes_', Gender{Indx_G}]); % get all trials
                Run_Matrix_temp = Trials(Indx_C).(['Run_Matrix_', Gender{Indx_G}])(:, Indx_Group); % load matrix of Runs
                Trials_temp = Trials_temp(Run_Matrix_temp == Indx_R); % reduce trials to ones for the current Run
                
                % get relevant variables
                Condition_temp = Trials(Indx_C).Sessions_Conditions(Indx_Group); % identify condition of current session
                Trials_temp = [repmat([Indx_S, Indx_R, Condition_temp], size(Trials_temp, 1),  1), Trials_temp]; % add columns indicating current session, Run and Run order (to be determined)
                
                % load to PRT table
                Trial_Table_temp = array2table(Trials_temp, 'VariableNames', {'Session', 'Run', 'Condition', 'Run_Order'});
                Trial_Table_temp = [Test_Trials_Table(ismember(Test_Trials_Table.Trial_Number, Trials_temp(:, end)), :), Trial_Table_temp ]; % add trial information from main trial table
                PRT = [PRT; Trial_Table_temp];
            end
        end
        
        % add tactile only trials
        Unimodal = PRT(PRT.Condition == Unique_Conditions & PRT.Run ==Indx_R & PRT.Type == 1, :); % get all trials with condition 7
        Unimodal.Condition(:) = Unique_Conditions + 1; % change the condition to 8
        Unimodal.Run = Unimodal.Run(randperm(length(Unimodal.Run))); % randomize Runs
        PRT = [PRT; Unimodal]; % add the 8 trials to the whole PRT
    end
    
    
    %%%  randomize within Runs
    for Indx_R = 1:Runs
        Run_Trials_Indx = PRT.Run == Indx_R;
        Run_Trials = PRT(Run_Trials_Indx, :); % trials of current Run
        Tot_Trials_Run = size(Run_Trials, 1); % how many
        
        % keep generating random orders until conditions aren't repeated consecutively
        while true
            rand_order = randperm(Tot_Trials_Run, Tot_Trials_Run); % new order
            Run_Trials.temp_Order = rand_order'; % add random order as column
            Run_Trials = sortrows(Run_Trials, 'temp_Order'); % sort by new random order
            
            % when there are no more repeats of both trial numbers and conditions, break loop
            if all(diff(Run_Trials.Trial_Number(:))) && all(diff(Run_Trials.Condition(:)))
                break
                %             elseif toc > 10
                %                 warning(['This is taking too long for Run ' num2str(Indx_R) ])
                %                 break
            end
        end
        
        % replace redundant Run trials with the new order
        PRT.Run_Order(Run_Trials_Indx) = rand_order;
    end
    
    PRT = sortrows(PRT, 'Run_Order'); % put in new order
    PRT = sortrows(PRT, 'Run'); % restore to order of overall Runs
    PRT.Onsets = zeros(size(PRT, 1), 1);
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Create threshold PRT
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % set variables
    CSVs_Thr = table();
    Tot_Ratios = length(envRatios);
    Tot_Trials_Thr = Threshold_Trials_Per_Gender*2;
    Trials_per_Ratio = Threshold_Trials_Per_Gender/Tot_Ratios;
    
    % get tables per gender
    Trial_Start = [1, Threshold_Trials_Per_Gender + 1];
    for Indx_G = 1:2 % loop through genders
        
        % load gender threshold CSV
        CSV_temp = readtable([CSV_Folder, 'Threshold_Sentences_', Genders{Indx_G}, '.csv']);
        
        % reassign trial numbers so that they are unique across genders
        Trial_End = Trial_Start(Indx_G) + Threshold_Trials_Per_Gender - 1;
        CSV_temp.Trial_Number = [Trial_Start(Indx_G):Trial_End]';
        
        % assign random condition to the sentences
        Order = repmat(1:5, 1, Trials_per_Ratio)';
        Order = Order(randperm(Threshold_Trials_Per_Gender, Threshold_Trials_Per_Gender));
        CSV_temp.Condition = Order;
        
        % add to general table, specifying gender
        CSV_temp.Gender = Indx_G*ones(Threshold_Trials_Per_Gender, 1);
        CSVs_Thr = [CSVs_Thr; CSV_temp];
    end
    
    % convert condition to corresponding envRatio
    for Indx_T = 1:Tot_Trials_Thr
        CSVs_Thr.Condition(Indx_T) = envRatios(CSVs_Thr.Condition(Indx_T));
    end
    
    % randomize all trials
    Order = randperm(Tot_Trials_Thr, Tot_Trials_Thr)';
    CSVs_Thr.Order = Order;
    CSVs_Thr = sortrows(CSVs_Thr, 'Order');
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% save
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % create folder structure for eventual data and prt files
    Destination = ['Sessions\S', num2str(Indx_S, Padding_2) '\'];
    if exist(Destination, 'dir') == 0
        mkdir(Destination)
    end
    
    writetable(PRT, [Destination, 'S', num2str(Indx_S, Padding_2), '_PRT.csv']);
    writetable(CSVs_Thr, [Destination 'Threshold_', num2str(Indx_S, Padding_2), '_PRT.csv' ]) % save threshold PRT
end

