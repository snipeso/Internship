clear
tic
%% load all stimuli

% Cols: Corpus Index, Gender Index, File Indx, s (to end)
% Rows: each file

% load parameters
Parameters

All_Signals = nan(3000, 100000); % excessively large nan matrix to hold all signals
All_Filenames = struct();
Row = 1;
for Indx_C = 1:size(Corpus_Folders, 2) % loop through corpus folders
    
    for Indx_G = 1:size(Genders, 2) % loop through gender folders
        Path = [Main_Folder, Corpus_Folders{Indx_C}, Gender_Folder{1, Indx_G}];
        
        Filenames = ls(Path); % get all filenames in folder
        Filenames = Filenames(3:end, :);
        All_Filenames.(Corpus_Folders{Indx_C}(1:end-1)).(Genders{1, Indx_G}) = Filenames;
        for Indx_F = 1:size(Filenames, 1) % loop through files
            [s, fs] = audioread([Path, Filenames(Indx_F, :)]);
            s = s(:, 1)';
            
            %%%%%
            %%%%%
            %%%%%
            % normalize RMS
            s = s/rms(s);
            %%%%%
            %%%%%
            %%%%%
            
            All_Signals(Row, 1) = Indx_C; % set first number in row to Corpus index (1 or 2)
            All_Signals(Row, 2) = Indx_G; % set gender index
            All_Signals(Row, 3) = Indx_F; % set file index
            All_Signals(Row, 4:length(s)+3) = s; % insert whole signal
            
            Row = Row + 1; % move on to next row
        end
    end
    disp(['Finished loading for Norming_All' Corpus_Folders{Indx_C}])
end

% remove extra nans. Warning: nans are still in there
All_Signals(~any(All_Signals, 2), :) = []; % remove rows of all nans
All_Signals(:, ~any(All_Signals)) = [];   % remove columns of all nans


%% center all signals between -1 and 1
%%%%%
%%%%%
%%%%%
All_Max = max(max(abs(All_Signals(:, 4:end)))); % find overall maximum of maximums of the signals
All_Signals(:, 4:end) = All_Signals(:, 4:end)/All_Max; % divide every signal by the overall maximum
All_Signals(All_Signals(:, 4:end) > 1) = 1;
All_Signals(All_Signals(:, 4:end) < -1) = -1;
%%%%%
%%%%%
%%%%%


%% modify audiofiles
for Trial = 1:size(All_Signals, 1)
    CF = Corpus_Folders{All_Signals(Trial, 1)};
    GF = Gender_Folder{All_Signals(Trial, 2)};
    FileName = All_Filenames.(CF(1:end-1)).(GF(1:end-1))(All_Signals(Trial, 3), :);
    Signal = All_Signals(Trial, 4:end);
    Signal(isnan(Signal)) = [];
    Path = [Main_Folder, CF, GF, FileName];
    audiowrite(Path, Signal, fs)
    
end

disp('Finished Norming')

% log
Time = datetime('now');
RMS = rms(Signal);
save([Main_Folder, 'Log\', 'Normed_All_', datestr(Time, 'dd-mmm-yyyy_HH-MM'), '.mat'], 'RMS', 'All_Filenames')

clear
