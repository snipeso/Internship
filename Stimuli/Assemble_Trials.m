function Assemble_Trials(Main_Folder, Trial_Folder, Trials_CSV)

%%% Parameters
Destination = [Main_Folder, 'Trials\', Trial_Folder]; %TODO: get rid of Main_Folder part in function inputs
Parameters
Max_Trial_Length = 30; % seconds. For vector for trial, without knowing exact duration.

%%% log

Current_Script = 'Assemble_Trials';
disp(['Start ', Current_Script])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Start
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%create destination folder
if exist(Destination, 'dir') == 0
    mkdir(Destination)
end

CSV = readtable([Main_Folder, 'CSVs\', Trials_CSV]); % get full csv
Rows = size(CSV, 1);
s_blank = Noise_Gain*randn(fs*Max_Trial_Length, 1)-Noise_Gain/2; % noise in silent gaps so that when real noise is added, it's not too silent
s = s_blank'; 

for Row = 1:Rows % loop through sentences
    
    % Load audio file
    Corpus = CSV.Corpus{Row};
    Gender = CSV.Gender{Row};
    Audio_Path = [Main_Folder, Corpus, '\', Gender, '\', CSV.Audio_Filename{Row}];
    [s_temp, fs] = audioread(Audio_Path);
    
    % insert in noisy signal array at appropriate times
    Start = round(CSV.Start_Time(Row)*fs);
    End = Start+length(s_temp)-1;
    s(Start:End) = s_temp;
    
    % if end of trial, save it, and create new blank signal
    if CSV.Test_1(Row) == 1
        audiowrite([Destination, CSV.Trial_Filename{Row}], s(1:End), fs)
        s = s_blank';
    end
end


disp(['End ', Current_Script])
