%% Load parameters
Time = datetime('now');
Main_Folder = 'C:\Users\Sophia\Projects\Internship\';
tic


%% Threshold_Test

%%%%%%%%%%%%%%%%
Session = 14;%%%%
%%%%%%%%%%%%%%%%

Assemble_Threshold_Test(Session)

%% set ratio

%%%%%%%%%%%%%%%
envRatio =  0.59608;  % def:[0.5 0.6 0.7 0.8 0.9]}
%%%%%%%%%%%%%%%

%% generate first Run
Assemble_Runs(Session, 1, 0, envRatio)
Session_Data(Session).Create_Practice = toc;

%% generate first half of Runs
Assemble_Runs(Session, 2:5, envRatio)
Session_Data(Session).Create_Runs = toc;

%% generate second half of Runs
Assemble_Runs(Session, 6:10, envRatio)
