function Assemble_Threshold_Test(Session_Number)
tic


%%% Load Variables
Parameters
Session_Path = [Main_Folder, 'Sessions\S', num2str(Session_Number, Padding_2)];
CSV_Location = [Session_Path, '\Threshold_', num2str(Session_Number, Padding_2), '_PRT.csv'];
CSV = readtable(CSV_Location);


%%% Create Variables
Tot_Trials = size(CSV, 1);
CSV.Onsets = zeros(Tot_Trials, 1);

Duration = sum(CSV.Duration) + (1+Tot_Trials)*Answer_Gap;
stimuli = zeros(5, round(Duration*new_fs));

% Trigger for starting Presentation
% [range, triggers] = Triggers(Record_Trg, 0, 0, new_fs*0.1); %  indicates start of run
% stimuli(5, range) = triggers;

%%%%%%%%%%%%%%%%%%%%
%%% Create Threshold audio
%%%%%%%%%%%%%%%%%%%%
Start = Answer_Gap*new_fs;
for Indx_T = 1:Tot_Trials % loop through sentences
    
    % Load trial info
    Gender = Gender_Folder{CSV.Gender(Indx_T)};
    audio_filename = CSV.Audio_Filename{Indx_T};
    Ratio = CSV.Condition(Indx_T);
    Trial_Number = CSV.Trial_Number(Indx_T);
    
    % generate audio signal
    [s, fs] = audioread([Main_Folder, Corpus_Folders{1}, Gender, audio_filename]);
    [TFS, ~] = getTFSandENV(s, fs, Ratio); % add associated noise
    
    End = Start + length(s) - 1;
    stimuli(1:2, Start:End) = repmat(TFS_Loudness*TFS(:)', 2, 1);
    
    % add start triggers
    [range, triggers] = Triggers(Start_Trg, Trial_Number, Unique_Conditions, Start);
    stimuli(5, range) = triggers;
    
    % add stop triggers
    [range, triggers] = Triggers(Record_Trg, Trial_Number, Unique_Conditions, End);
    stimuli(5, range) = triggers;
    
    CSV.Onsets(Indx_T) = Start/fs; % save Start
    Start = End + Answer_Gap*fs; % reset Start to after answer gap
end

% add trigger to end Presentation
[range, triggers] = Triggers(End_Presentation_Trg, 0, 0, Start); % 15 quits Presentation, Start is the end of the last answer period
stimuli(5, range) = triggers;

% Save
stimuli = single(stimuli); % check
save([Session_Path, '\S', num2str(Session_Number, Padding_2),'_Threshold.mat'], 'stimuli')
writetable(CSV, CSV_Location) 


% End log
disp(['End Threshold'])
toc