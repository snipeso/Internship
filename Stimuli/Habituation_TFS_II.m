Parameters
tic
Env_Ratios = [0.9, 0.85, 0.8, 0.775, 0.75, 0.7, 0.65, 0.6];
Filepath = [Main_Folder, 'Habituation\Long_Story\'];
Indexes = 1558:1565;

Noised = zeros(1, fs*60*60);
Start = 1;
Max = 16.8842; % discovered from running through script without TFSing
for Indx = 1:length(Indexes) % loop through CGN wavs
    Name = ['fn00', num2str(Indexes(Indx))];
    Filename = [Name, '.wav'];
    disp(Filename)
    [s, fs] = audioread([Filepath, Filename]);
    s = s/rms(s); % same normalization as with other stimuli
    
    %%%% this was to find the overall max
    %     if max(s) > Max
    %         Max = max(s);
    %     end
    
    s = s/Max;
    s(s > 1) = 1;
    s(s<-1) = -1;
    
    [TFS, ENV] = getTFSandENV(s,fs,Env_Ratios(Indx)); % Tfs is audio, ENV is tactile
%     TFS = s; % this is when finding max
    TFS = TFS(:)';
    
    End = Start + length(TFS) - 1;
    Noised(Start:End) = TFS;
    
    Start = End + 1;
end

Noised = Noised(1:End); % removes extra 0s

audiowrite([Filepath, 'Noisy_Story.wav'], Noised, fs)
toc