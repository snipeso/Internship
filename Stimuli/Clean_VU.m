clear
close all
tic


%%% Set Parameters
Parameters

% log
Time = datetime('now');
Current_Script = 'Clean_VU';
save([Main_Folder, 'Log\', Current_Script, '_' datestr(Time, 'dd-mmm-yyyy_HH-MM'), '.mat']);
disp(['Start ', Current_Script])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_G = 1:2 % loop through genders
    Gender = Gender_Folder{Indx_G};
    Old_Folder = [Main_Folder, VU_Old_Path, Gender];
    New_Folder = [Main_Folder, 'VU\', Gender];
    CSV_Path = [VU_Old_Path, 'VU_Sentences_', Genders{Indx_G}, '.csv'];
    
    if exist(New_Folder, 'dir') == 0
        mkdir(New_Folder)
    end
    
    % Get audio filenames
    Audio_File_Names = ls(Old_Folder);
    Audio_File_Names = Audio_File_Names(3: end, :); % removes initial . and ..
    Rows = size(Audio_File_Names, 1);
    
    % get audio table and create new empty table
    CSV = readtable(CSV_Path);
    CSV.Properties.VariableNames{1} = 'Order'; % because csv is weird and gives junk strings
    
    Indx = 1:Rows; % array of index numbers
    New_Filename = cell(Rows, 1); % empty cells for filename strings
    Old_Filename = cell(Rows, 1);
    Duration = zeros(Rows, 1);
    Sentence = cell(Rows, 1);
    New_CSV = table(Indx', New_Filename, Old_Filename, Duration, Sentence);
    
    %%% modify audio
    for Row = 1:Rows
        
        % Load
        Old_Filename =  Audio_File_Names(Row, :);
        [s, fs] = audioread([Old_Folder, Old_Filename]);
        
        % filter out high frequencies
        [b,a] = butter(4, 8000/(fs/2), 'low'); % 4th-order lowpass filter with cutoff frequency 8000Hz
        s = filtfilt(b, a, s);
        
        % Resample tone to 16kHz
        t_temp = timeseries(s,[0:1/fs:(numel(s)/fs)-1/fs]);
        t = resample(t_temp,[0:1/new_fs:(numel(s)/fs)-1/fs]);
        s = squeeze(t.Data);
        fs = new_fs;
        
        % Remove initial and final silence
        Start = find(abs(s) > VU_Silence_Thr, 1, 'first');
        End = find(abs(s) > VU_Silence_Thr, 1, 'last');
        s = s(Start:End, :);
        
        % Save new file
        Number = extractBefore(Old_Filename, '.wav');
        Number = Number(end-2:end); % get number in name rather than rely on order
        
        New_Filename = ['VU_', Gender(1), '_', Number, '.wav'];
        audiowrite([New_Folder, New_Filename], s, fs)
        
        New_CSV.New_Filename{Row} = New_Filename;
        New_CSV.Old_Filename{Row} = Old_Filename;
        New_CSV.Duration(Row) = length(s)/fs;
        New_CSV.Sentence(Row) = CSV.Sentence(Row);
    end
    
    writetable(New_CSV, [CSV_Folder, 'VU_Sentences_', Genders{Indx_G}, '.csv'])
end

% Finish log
save([Main_Folder, 'Log\', Current_Script, '_' datestr(Time, 'dd-mmm-yyyy_HH-MM'), '.mat'], 'Rows', '-append');
disp(['End ', Current_Script])

clear
