% Session
Unique_Conditions = 7;
Start_Session = 9; % change to 8 after 7th participant
Sessions_Range = Start_Session:Start_Session+Unique_Conditions-1; % determines the names of the sessions. Please just change the start number

% Conditions
SOAs = [-0.3, -0.2, -0.1, 0, 0.1, 0.2]; % positive are audio leading. in seconds
envRatios = [0.5, 0.6, 0.7, 0.8, 0.9]; % 5 values between 0:1 {def:[0.5 0.6 0.7 0.8 0.9]}

% Number of Trials
Trials_Per_Condition = 40; % has to be a specific number (see posible_trials excel)
Trials_Per_Condition_Per_Run = 4; % see possible trials excel sheet
Dummy_Trials = [10, 3, 1]; % percent of trials that are less than the lowest number of trials
Threshold_Trials_Per_Gender = 55;

% Time
Filler_Time = 16; % trial time in seconds, minimum for create trials
Filler_Window = 4; % the sum of these two gives the max trial length
Dummy_Times = [1, 6, 11];
Sentence_Gap = 0.5; % in seconds
Answer_Gap = 5; % for assemble Runs
Start_Run_Gap = 1; % for assemble Runs
Trg_Dur = 0.01; % each trigger duration
Tactile_Pulse = 0.1; % pulse at start of tactile only condition

% .3 for <6s, .2 < 10s, .1 < 16s 3.85

% Constants
new_fs = 16000;
fs = new_fs; % some scripts use one, some the other
CGN_Silence_Thr = 0.01; % for cleaning CGN, minimum value at start to count as starting audio
VU_Silence_Thr = 0.000;
Padding = '%03.f'; % for filename number, adds 0s at beginning TODO: change to Padding_3
Padding_2 = '%02.f'; % for session and run number
Carrier_Frequency = 128; % Hz, for assemble Runs
ENV_Loudness = 1; % value from 0 to 1 (percentage)
TFS_Loudness = 0.075;
Noise_Gain = 0.0005; % small noise in silence gaps in trial assembly

% Trigger Type Codes
Start_Trg = 2; % start of main trial
Record_Trg = 1; % End
Dummy_Trg = 3; % start of dummy
Stop_Trg = 4; % end trial without recording, for tactile condition
End_Presentation_Trg = 14;

% Folders
Internship_Folder = 'C:\Users\Sophia\Projects\Internship\';
Main_Folder = [Internship_Folder, 'Stimuli\'];
CSV_Folder = [Main_Folder, 'CSVs\'];
Threshold_Folder = [Main_Folder, 'Threshold\'];
Trials_Folder = [Main_Folder, 'Trials\'];
Gender_Folder = {'Male\', 'Female\'};
Corpus_Folders = {'VU\', 'CGN\'}; % TODO: change to singular
CGN_Old_Path = 'Filtered_CGN_Snips\';
VU_Old_Path = 'VU98\spraak\';

% Category Names
Genders = {'Male', 'Female'}; % TODO: in Threshold script, make it so they pull out {}
Types = {'Test', 'Dummy'}; % the order matters!

% File Names
CGN_CSV_Name = 'All_CGN_Sentences.csv'; % for clean CGN
Trials_Filename = '_Trials_Summary.csv'; % for Run generator

% Column Names
Header = {'Trial_Number', 'Trial_Filename', 'Gender', 'Duration', 'Sentence', 'Type'}; % for trial creater
Full_Header = [Header, 'Audio_Filename', 'Test_1', 'Order', 'Corpus', 'Old_Number', 'Start_Time', 'End_Time'];  % for trial creater
Summary_Header = [Header, 'All_Sentences'];  % for trial creater



