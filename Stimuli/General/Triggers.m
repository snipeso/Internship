% creates trigger signal with 5 triggers
function [range, signal] = Triggers(Type, Trial_Number, Condition, Trigger_Start)
% Type: 1 for End (start recording), 2 for Start trial

% Load variables
Parameters
Trigger_Duration = round(fs * Trg_Dur); % timepoints needed for each trigger

if Type == End_Presentation_Trg
    % create single last trigger
    Trigger_Code = repmat([Type, 0, 0, 0, 0], Trigger_Duration, 1);
else
    %%% convert trial number to 3 triggers numbers 0-9 -> 3-11
    Trial_Number = num2str(Trial_Number, Padding); % convert to string with 0s in front when needed
    Trial_Numbers = zeros(1, length(Trial_Number));
    
    for n = 1:length(Trial_Number) % loop through digits
        Trial_Numbers(n) = str2double(Trial_Number(n)) + 2; % add 2 to digit so 1 is only used for type
    end
    
    Condition = Condition + 2; % same conversion
    
    % create trigger vector
    Trigger_Code = repmat([Type, 0, ...
        Trial_Numbers(1), 0, Trial_Numbers(2), 0, Trial_Numbers(3), 0, ...
        Condition, 0], Trigger_Duration, 1); % repeat each code the above number of timepoints
end

%%% output vectors
range = Trigger_Start:(Trigger_Start + numel(Trigger_Code) - 1); % location in matrix
signal = Trigger_Code(:);
