Parameters

Trigger_Starts = [1:2:20];

t = linspace(0, 20, fs * 20);

Stimulus = sin(2*pi*4*t) .* sin(2*pi*Carrier_Frequency*t);
Trigger = zeros(size(t));

for Indx_T = 1:length(Trigger_Starts)
    
    [range, triggers] = Triggers(Indx_T, 0, 0, fs*Trigger_Starts(Indx_T));
    Trigger(range) = triggers;
end

[range, triggers] = Triggers(End_Presentation_Trg, 0, 0, 19.5*fs); % 14 quits Presentation, Start is the end of the last answer period
Trigger(range) = triggers;
stimuli = [repmat(Stimulus, 3, 1); Trigger];
stimuli = single(stimuli);

save('Test_Stimuli.mat', 'stimuli')
plot(Trigger)