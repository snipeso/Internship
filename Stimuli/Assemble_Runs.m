function Assemble_Runs(Session_Number, Runs, Trials, envRatio)
% Assemble_Runs(1, 1, 0.7);

%%% Set Parameters
tic
Parameters


disp(['Start Assemble_Runs'])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%



for Indx_R = Runs % loop through requested Runs
    
    
    % Names
    Session_Path = [Main_Folder, 'Sessions\', 'S', num2str(Session_Number, Padding_2) '\'];
    Session_CSV = ['S' num2str(Session_Number, Padding_2), '_PRT.csv'];
    Destination = Session_Path;
    
    %%% Load PRT
    Session_Table = readtable([Session_Path, Session_CSV]); % PRT of session
    Session_Table.Properties.VariableNames{1} = 'Trial_Number';
    
    % select only trials of Run
    Run_Table = Session_Table(Session_Table.Run == Indx_R, :);
    
    if Trials > 0
        Run_Table = Run_Table(Trials, :);
        Matrix_Name = [Destination, 'S', num2str(Session_Number, Padding_2),'_R', num2str(Indx_R, Padding_2), '_T', num2str(Trials(1)), '-', num2str(Trials(end))  '.mat'];
    else
        Matrix_Name = [Destination, 'S', num2str(Session_Number, Padding_2),'_R', num2str(Indx_R, Padding_2), '.mat'];
    end
    
    % create template matrix
    Tot_Trials = size(Run_Table, 1);
    Tot_Duration = ceil((sum(Run_Table.Duration) + Tot_Trials*Answer_Gap + 1)*new_fs); % overall trial duration
    Mat_Matrix = zeros(5, Tot_Duration);
    
    
    disp(['Run ', num2str(Indx_R), ' lasts ', num2str(Tot_Duration/(new_fs*60))]) % inform of duration in minutes
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% fill matrix
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    Start = Start_Run_Gap*new_fs; % start audio after small delay
    for Indx_T = 1:Tot_Trials % loop through trials in Run
        
        % Select parameters
        Indx_Type = Run_Table.Type(Indx_T); % whether dummy or test trial
        Condition = Run_Table.Condition(Indx_T);
        Trial_Number = Run_Table.Trial_Number(Indx_T);
        Audio_File = Run_Table.Trial_Filename{Indx_T};
        Run_Order = Run_Table.Run_Order(Indx_T);
        
        % load trial audio
        [s, fs] = audioread([Main_Folder, 'Trials\', Types{Indx_Type}, '\', Audio_File ]); %TODO: if this is the only thing that needs typ names, include \ in cell list
        End = Start + length(s) - 1;
        
        % add noise and create tactile stimulus
        [TFS, ENV] = getTFSandENV(s,fs,envRatio); % Tfs is audio, ENV is tactile
        TFS = TFS_Loudness*TFS(:)';
        ENV = ENV_Loudness*ENV(:)';
        
        t = linspace(0, length(ENV)/fs, length(ENV)); % time vector
        Carrier_s = sin(2*pi*Carrier_Frequency*t); % carrier signal
        ENV = ENV.*Carrier_s; % carrier modulated by envelope
        
        
        %%%%%%%%%%%
        %%% get onsets and offsets, set conditions
        %%%%%%%%%%%
        
        ENV_Start = Start; % default
        ENV_End = End;
        Trigger_Starts = [End, Start];
        
        if Condition <= length(SOAs) % Bimodal conditions
            SOA = SOAs(Condition);
            if SOA ~= 0 % conditions with different start times for tactile
                ENV_Start = Start + SOA*fs;
                ENV_End = End + SOA*fs;
            end
            
        elseif Condition == length(SOAs) + 1 % Auditory only
            ENV = zeros(1, length(s)); % removes tactile
            
        elseif Condition == length(SOAs) + 2 % tactile only
            TFS = zeros(1, length(s)); % removes auditory
            t_pulse = linspace(0, Tactile_Pulse, Tactile_Pulse*fs);
            Pulse = sin(2*pi*Carrier_Frequency*t_pulse);
            ENV(1:length(t_pulse)) = Pulse; % adds short pulse at start of trial
            
        end
        
        
        %%%%%%%%%%%
        %%% upload to matrix
        %%%%%%%%%%%
        
        % Set triggers for End and Start
        for T = [Record_Trg, Start_Trg]
            Type_Code = T;
            if Indx_Type == 2 && T == Start_Trg % check if start of dummy trial
                Type_Code = Dummy_Trg;
            elseif Condition == length(SOAs) + 2 && T == Record_Trg % check if end of tactile only trial
                Type_Code = Stop_Trg;
            end
            [range, triggers] = Triggers(Type_Code, Trial_Number, Condition, Trigger_Starts(T));
            Mat_Matrix(5, range) = triggers;
        end
        
        Mat_Matrix(1:2, Start:End) = [TFS; TFS]; % load audio
        Mat_Matrix(3:4, ENV_Start:ENV_End) = [ENV; ENV]; % load tactile
        
        % Save trial onsets to sessions table
        Session_Table.Onsets(Session_Table.Run == Indx_R & Session_Table.Run_Order == Run_Order) = Start/fs;
        
        % add gap for answers, except for tactile only condition
        if Condition == length(SOAs) + 2
            Start = End + 1*fs;
        else
            Start = End + round(Answer_Gap*fs);
        end
        
        disp(Indx_T)
    end
    
    [range, triggers] = Triggers(End_Presentation_Trg, 0, 0, Start); % 15 quits Presentation, Start is the end of the last answer period
    Mat_Matrix(5, range) = triggers;
    stimuli = single(Mat_Matrix);
    
    % save matrix to mat file
        save(Matrix_Name, 'stimuli')

    
    % save session table with onsets
    writetable(Session_Table, [Destination, ['S' num2str(Session_Number, Padding_2), '_PRT.csv'];])
end

toc


end
