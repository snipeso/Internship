clear
clc
close all

if exist('Log', 'dir') == 0
    mkdir('Log')
end

if exist('CSVs', 'dir') == 0
    mkdir('CSVs')
end

% Save parameters used
Time = datetime('now');
Parameters
% save([Internship_Folder, 'Experiment\Parameters_', datestr(Time, 'dd-mmm-yyyy'), '.mat']);


% Clean_VU
% Clean_CGN
% Norm_All
% Create_Trials
% Generate_Practice_PRT


Generate_Sessions_PRT % this gets done multiple times


disp('Finished Creating Trials')
