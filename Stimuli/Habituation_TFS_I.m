%%%%%%%%%% Warning! %%%%%%%%%%%%
% These sentences have to be hand picked from the ones that weren't chosen
% for any other part of the experiment! 

Parameters
Folder = 'C:\Users\Sophia\Projects\Internship\Stimuli\Habituation\Unused_CGN\';
Extras = ls(Folder);

Extras = Extras(3:end, :);
Tot_Extras = size(Extras, 1);
Extras = Extras(randperm(Tot_Extras, Tot_Extras), :);


Signal = zeros(2*3000000, 1);
Gap = zeros(Sentence_Gap * fs, 1);

Start = Sentence_Gap * fs;
for Indx_T = 1 %:Tot_Extras
    Filename = [Folder, Extras(Indx_T, :)];
    [s, fs] = audioread(Filename);
    
    Env_Ratio = 0.623;
    [TFS, ~] = getTFSandENV(s,fs, Env_Ratio); % Tfs is audio, ENV is tactile
    Signal_temp = [Gap; TFS; Gap; s; Gap; TFS; Gap];
    length_temp = length(Signal_temp);
    End = Start+length_temp-1;
    Signal(Start:End, :) = Signal_temp;
    Start = End + Sentence_Gap * fs;
end

Signal = Signal(1:End);
audiowrite([Folder, 'Example.wav'], Signal, fs)
plot(Signal, 'Color', [0.216 0.451 0.608])
set(gca,'visible','off')