function f = ERBn_number_to_Hz(n)

% n = 21.4*log10(4.37*f/1e3+1);

f = (10.^(n/21.4)-1)/4.37*1e3;