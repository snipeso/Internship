function [tfs,env]=getTFSandENV(x,fs,envRatio)

% returns TFS and ENV for an input signal
% uses a bank of 30 gammatone filters (CFs from 87-6930Hz) with fixed bandwidth=ERBn 
% applies Hilbert trafo to each filter output
% s should be sampled at 16kHz


%%% extract signal, tfs, env per channel
nbChns=30; % 3:30 {def:30}
fcs=ERBn_number_to_Hz(linspace(3,32,nbChns));
[y,p]=gammatone_fb(x,fs,fcs);
tfs=zeros(size(y));
env=zeros(size(y));
for i=1:length(fcs)
    h=hilbert(y(:,i));
    env(:,i)=abs(h);
    tfs(:,i)=cos(angle(h));
end
clear fcs y i h

%%% filter channel-ENV
filterOrder=4; % {def:4}
envLP = zeros(size(env));
envHP = zeros(size(env));
[b1, a1] = butter(filterOrder, 16*2/fs, 'low');  % see Drullman JASA 1994a
[b2, a2] = butter(filterOrder, 64*2/fs, 'high'); % see Drullman JASA 1994b
for i=1:length(p.center_frequencies)
    envLP(:,i) = filtfilt(b1, a1, env(:,i)); % LP-filtering within each channel for env (TCS)
    envHP(:,i) = filtfilt(b2, a2, env(:,i)); % HP-filtering within each channel for tfs (audio)
end

%%% composite ENV
env = sqrt(sum(envLP.^2,2));
env = env/max(abs(env(:)));
envHP = envHP/max(abs(envHP(:)));

%%% composite TFS with envRatio applied
tfs = tfs.*((1-envRatio)+envRatio*sqrt(abs(envHP))); % restore only HP-portion of ENV
tfs = sum(tfs,2);
tfs = (tfs/rms(tfs))*rms(x); % RMS of TFS matches the RMS of the original signal
tfs = cosgate(tfs, fs, 0.025); % apply 25ms-ON/OFF ramps to TFS
clear filterOrder p envLP envHP b1 a1 b2 a2
