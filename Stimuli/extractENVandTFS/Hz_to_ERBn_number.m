function n = Hz_to_ERBn_number(f)

n = 21.4*log10(4.37*f/1e3+1);