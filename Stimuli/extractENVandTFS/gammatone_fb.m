function [y, p] = gammatone_fb(x, fs, fcs, p, verbose)

%GAMMATONE_FB: Filters a signal through a gammatone filterbank
%
% Y = GAMMATONE_FB(X, FS, FCS)
%
%   X  : the signal to filter
%   FS : its sampling frequency (Hz)
%   FCS: a vector containing the center frequencies of the different channels
%
%   Group delays and channel delays (so that all channels are time aligned)
%   will be determined automatically.
%
%
% [Y, P] = GAMMATONE_FB(X, FS, FCS)
% 
%   Returns a structure P containing the group delay and channel delays
%   (see below). If X is empty, only the group and channel delay
%   determination is done.
%
%
%      Y = GAMMATONE_FB(X, FS, FCS, P)
% [Y, P] = GAMMATONE_FB(X, FS, FCS, P)
%   
%   P is a struct with the following fields:
%       - 'group_delay'   : the group delay in samples
%       - 'channel_delays': delay (in samples) for individual channels
%       - 'center_frequencies': must be identical to FCS
%
%   If P does not contain these fields, or if they are empty, the group and
%   channel delay determination will be performed.
%
% The underlying gammatone function was implemented by Ning Ma:
% http://staffwww.dcs.shef.ac.uk/people/N.Ma/resources/gammatone/.
%
% The alignment procedure is adapted from Hohmann (2002) Acta Acustica
% United with Acustica.
%
% SEE ALSO Hz_to_ERBn_number, ERBn_number_to_Hz


%----
% Etienne Gaudrain <etienne.gaudrain@cnrs.fr>, 2016-03-23
%----

compute_gd = true; % Do we have to compute the group delay?

if nargin<4
    p = struct();
end

if nargin<5
    verbose = 0;
end

if isstruct(p) && isfield(p, 'group_delay') && ~isempty(p.group_delay) && isfield(p, 'channel_delays') && ~isempty(p.channel_delays)
    compute_gd = false;
    if ~isfield(p, 'center_frequencies') || length(p.center_frequencies) ~= length(fcs) || any(p.center_frequencies ~= fcs)
        warning('GammatoneFB:fcs_mismatch', 'The center frequencies provided in the P struct do not match the provided center frequencies, so group delay needs to be calculated again.');
        compute_gd = true;
    end
end

%-----------------
% Compute group delay if necessary

if compute_gd
    if verbose>0
        fprintf('Computing group delay from impulse response... ');
    end
    
    if verbose>1
        tic();
    end
    
    p = compute_group_delay(fs, fcs, verbose);
    
    if verbose>1
        fprintf('Done in %.1f ms\n', toc()*1e3);
    end
end

%-----------------
% Filter

if isempty(x)
    y = [];
    return
end

y = zeros(length(x), length(fcs));
p.raw_output = y;

t = (0:length(x)-1)'/fs;

for i=1:length(fcs)
    [bm, env, ~, instf] = gammatone_c(x, fs, fcs(i));
    
    env = [zeros(p.channel_delays(i),1); env'];
    env = env(1:length(x));
    
    %y(:,i) = env .* cos(2*pi*instf'.*(t-(p.group_delay-1)/fs));
    y(:,i) = env .* cos(2*pi*(cumsum(instf'/fs)-(p.group_delay-1)/fs));
    p.raw_output(:,i) = bm';
end


%==========================================================================
function p = compute_group_delay(fs, fcs, verbose)

n = round(3*fs/min(fcs));
x = zeros(n,1);
x(1) = 1;

env_a  = zeros(length(x), length(fcs));

for i=1:length(fcs)
    [~, env] = gammatone_c(x, fs, fcs(i));

    env_a(:,i) = env';
end

[~, k_a] = max(env_a);

gd = max(k_a);

p = struct();
p.group_delay = gd;
p.channel_delays = gd-k_a;
p.center_frequencies = fcs;

