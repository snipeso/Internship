clear
cd(fileparts(which(mfilename)));

%%% get tfs and env
envRatio=0.7; % controls the magnitude of the residual HF-ENV 
              % 0.0 = no HF-ENV = unintelligible
              % 1.0 = strong HF-ENV = more intelligible
% [s,fs]=audioread('Man001.wav');
[tfs,env]=getTFSandENV(s,fs,envRatio); % extract TFS and ENV
% sound(tfs,fs);

%%% compute onset env 
stepSize=1;   
envSlope=diff(env)/stepSize; % 1st derivative
onsetEnv=envSlope;
onsetEnv(onsetEnv<0)=0; % halfwave rectification

%%% plot
envSlope=envSlope./max(abs(envSlope));
onsetEnv=onsetEnv./max(abs(onsetEnv));
plot(env,'k'); hold on;
plot([0;envSlope],'r');
plot([0;onsetEnv],'b');

