clear
close all
tic

%%% Set Parameters
Parameters

% Log
Time = datetime('now');
Current_Script = 'Clean_CGN';
save([Main_Folder, 'Log\', Current_Script, '_' datestr(Time, 'dd-mmm-yyyy_HH-MM'), '.mat']);
disp(['Start ', Current_Script])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_G = 1:2 % loop through genders
    Gender = Gender_Folder{Indx_G};
    Old_Folder = [Main_Folder, CGN_Old_Path, Gender];
    New_Folder = [Main_Folder, 'CGN\', Gender];
    
    if exist(New_Folder, 'dir') == 0
        mkdir(New_Folder)
    end
    
    % Get audio filenames
    Audio_File_Names = ls(Old_Folder);
    Audio_File_Names = Audio_File_Names(3: end, :); % removes initial . and ..
    
    % get audio table and create new empty table
    CSV = readtable([CGN_Old_Path, CGN_CSV_Name]);
    CSV.Properties.VariableNames{1} = 'Indx'; % because csv is weird and gives junk strings
    
    New_CSV_Col = {'Indx', 'New_Filename', 'Original_txt', 'Start_Time', 'Sentence'};
    Rows = size(Audio_File_Names, 1);
    New_CSV = CSV(1:Rows, New_CSV_Col);
    New_CSV.Properties.VariableNames{'Original_txt'} = 'Old_Filename';
    New_CSV.Properties.VariableNames{'Start_Time'} = 'Duration';
    
    
    %%% modify audio
    for Row = 1:Rows % loop through audio files
        
        % Load
        Old_Filename =  strtrim(Audio_File_Names(Row, :));
        [s, fs] = audioread([Old_Folder, Old_Filename]);
        
        % Remove initial silence
        Start = find(abs(s) > CGN_Silence_Thr, 1, 'first');
        s = s(Start:end, :);
        
        % Save new file
        New_Filename = ['CGN_', Gender(1), '_', num2str(Row, Padding), '.wav'];
        audiowrite([New_Folder, New_Filename], s, fs)
        
        % update table
        CSV_Row = find(contains(CSV.New_Filename, Old_Filename));
        New_CSV.Indx(Row) = Row;
        New_CSV.New_Filename{Row} = New_Filename;
        New_CSV.Old_Filename(Row) = CSV.New_Filename(CSV_Row);
        New_CSV.Duration(Row) = length(s)/fs;
        New_CSV.Sentence(Row) = CSV.Sentence(CSV_Row);
    end
    
    writetable(New_CSV, [CSV_Folder, 'CGN_Sentences_', Genders{Indx_G}, '.csv'])
    disp(['Finished Clean CGN ', Gender, num2str(toc)])
end

% End log
save([Main_Folder, 'Log\', Current_Script, '_' datestr(Time, 'dd-mmm-yyyy_HH-MM'), '.mat'], 'Rows', '-append');
disp(['End ', Current_Script])

clear