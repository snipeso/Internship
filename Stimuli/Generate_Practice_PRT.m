
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create Practice Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters

Practice_Trials_Table = readtable([CSV_Folder, Types{3}, Trials_Filename]);
Practice_Tot = size(Practice_Trials_Table, 1);
Zeros = zeros(Practice_Tot, 1);
Practice_Trials_Table.Session = Zeros;
Practice_Trials_Table.Run = Zeros;
Practice_Trials_Table.Condition = ones(Practice_Tot, 1)*find(SOAs == 0); % This needs to correspond to SOA0
Practice_Trials_Table.Run_Order = randperm(Practice_Tot, Practice_Tot)';
Practice_Destination = 'Sessions\Session_0\';
Practice_Trials_Table = sortrows(Practice_Trials_Table, 'Run_Order');

if exist(Practice_Destination, 'dir') == 0
    mkdir(Practice_Destination)
end

writetable(Practice_Trials_Table, [Practice_Destination, 'Session_PRT_0.csv'])

clear
