clear
close all

%%% WARNING: terrible code up ahead. I'm really sorry.

%%% Set Parameters
Parameters
Max_Filler_Time = Filler_Time + Filler_Window;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Upload audio file information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% General variables
Files = struct(); % location of all the information. Divided in Files.Corpus(Gender).Variables
Tot_Trials =  Trials_Per_Condition*Unique_Conditions; % unique trials
Half_Trials = round(Tot_Trials/2); % trials per gender (now needs to include dummy trials

Runs = floor((Trials_Per_Condition * (Unique_Conditions + 1))/ (Trials_Per_Condition_Per_Run * (Unique_Conditions + 1) )); % maximum number of Runs to have even conditions
Half_Dummy_Trials = Runs * sum(Dummy_Trials)/2; % total number of extra trials


for Indx_C = 1:size(Corpus_Folders, 2) % loop through corpus folders
    CF = Corpus_Folders{Indx_C}; % shorten variable name
    
    for Indx_G = 1:size(Genders, 2) % loop through gender folders
        GF = Gender_Folder{Indx_G};
        CSV = readtable([CSV_Folder, CF(1:end-1), '_Sentences_', GF(1:end-1), '.csv']); % file with durations
        
        % load audio file names
        Path = [Main_Folder, CF, GF];
        Filenames = ls(Path);
        Filenames = Filenames(3:end, :);
        
        %%% select thresholding sentences
        if Indx_C == 1 % if VU corpus
            
            %split CSVs into threshold and not
            CSV_Threshold = CSV(1:Threshold_Trials_Per_Gender, :);
            CSV = CSV(Threshold_Trials_Per_Gender+1:end, :);
            
            % recreate threshold CSV to match column names % mild TODO: use header values
            CSV_Thr = CSV_Threshold(:, 1); % makes table of just indexes
            CSV_Thr.Properties.VariableNames = {'Trial_Number'}; % changes name of column
            CSV_Thr.Audio_Filename = CSV_Threshold.New_Filename;
            CSV_Thr.Duration = CSV_Threshold.Duration;
            CSV_Thr.Sentence = CSV_Threshold.Sentence;
            CSV_Thr.Type = zeros(size(CSV_Threshold, 1), 1); % sets type to 0
            
            % save threshold table
            writetable(CSV_Thr, [CSV_Folder, 'Threshold_Sentences_', GF(1:end-1), '.csv'])
            
            % reduce filenames to just available ones
            Filenames = Filenames(Threshold_Trials_Per_Gender+1:end, :);
        end
        
        % identify maximum files per gender per corpus
        Max_Trials = size(Filenames, 1); % uses filenames and not csv in case audiofile is missing
        if Half_Trials > Max_Trials
            warndlg(['Not Enough audio files in ' CF, GF])
        end
        
        % create randomized trial matrix of Index, Corpus (1 == Test), Gender (1 == Male), Sentence Duration(s))
        Test_Sentences = [randperm(Max_Trials, Max_Trials)', ... %TODO: change "Trials" to sentences
            ones(Max_Trials, 1)*Indx_C, ...
            ones(Max_Trials, 1)*Indx_G, ...
            zeros(Max_Trials, 1)];
        
        for Trial = 1:Max_Trials
            Test_Sentences(Trial, end) = CSV.Duration(Test_Sentences(Trial, 1)); % get duration from table
        end
        
        % Save information to Files Structure
        Files.(CF(1:end-1))(Indx_G).Table = CSV;
        Files.(CF(1:end-1))(Indx_G).Trials = Test_Sentences;
    end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Randomize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Collect all filler trials, from all CGN and remaining VU, randomize
for Indx_G = 1:2
    Filler_Trials =  [Files.(Corpus_Folders{2}(1:end-1))(Indx_G).Trials; ...
        Files.(Corpus_Folders{1}(1:end-1))(Indx_G).Trials(Half_Trials+Half_Dummy_Trials+1:end, :)]; % what
    Tot_Fillers = size(Filler_Trials, 1); % how many
    Files.Fillers(Indx_G).Trials = Filler_Trials(randperm(Tot_Fillers, Tot_Fillers), :); % randomize
end

%%% create dummy times
Dummy_Min_Times = [];
for Indx_D = 1:length(Dummy_Trials)
    Dummy_Min_Times = [Dummy_Min_Times, repmat(Dummy_Times(Indx_D), 1, Runs*Dummy_Trials(Indx_D))];
end

Dummy_Min_Times = Dummy_Min_Times(1, randperm(size(Dummy_Min_Times, 2)));


%%% Assemble trials
All_Trials = struct(); % hold trials for both test (1) and dummy (2)

% Set counters to 0
Indx_T = 0; % overall test trial number
Indx_D = 0; % overall dummy trial number
Indx_All = 0; % overall sentence number (later used to create max full csv file)
Indx_F1 = 1;
Indx_F2 = 1;
tic

for Indx_G = 1:2 % loop through genders
    
    % get Tot/2 trials from VU gender
    Test_Sentences = Files.(Corpus_Folders{1}(1:end-1))(Indx_G).Trials(1:Half_Trials + Half_Dummy_Trials, :);
    
    for Row = 1:Half_Trials + Half_Dummy_Trials %loop through half of total trials
        
        % set different variables for dummy or test trial
        if Row <= Half_Trials % test trials
            Indx_T = Indx_T + 1;
            Type = Types{1}; % get 'Test'
            Indx_Type = Indx_T;
            Min_Time = Filler_Time;
            
        else % dummy trials
            Indx_D = Indx_D + 1;
            Indx_Type = Indx_D;
            Type = Types{2}; % gets 'Dummy'
            Min_Time = Dummy_Min_Times(Indx_D); % randomly sets maximum at each trial to something lower than the filler time             
        end
        
        Trial = Test_Sentences(Row, :); % start trial matrix with test sentence
        
        % keep adding trials until minimum duration has been reached
        while sum(Trial(:, end)) + size(Trial, 1)*Sentence_Gap < Min_Time % duration of sentences + duration of gaps
            
            % switch gender for each additional filler sentence
            if Trial(end, 3) == 1 % if male
                Filler_Gender = 2;
                Indx_F2 = Indx_F2 + 1; % advance to next sentence in fillers
                Indx_G_F = 2;
            else % if female
                Filler_Gender = 1;
                Indx_F1 = Indx_F1 + 1;
                Indx_G_F = 1;
            end
            Indx_F = [Indx_F1, Indx_F2];
            
            % get sentence row from filler trial matrix
            Filler = Files.Fillers(Indx_G_F).Trials(Indx_F(Indx_G_F), :); % if an error message occurs here, you've run out of sentences
            
            % Checks that max duration hasn't been reached
            Tot_Dur = Filler(1, end) + sum(Trial(:, end)) + size(Trial, 1)*Sentence_Gap;
            if toc > 5 % escape in case this takes too long
                Trial = [Trial; Filler];
                disp('extra_long_trial')
            elseif Tot_Dur > Max_Filler_Time % if goes over filler time, replaces a trial with the overshooting one
                Switch_Trial = Trial(end-1, :); % gets second to last trial
                Trial(end-1, :) = Filler; % replaces overshooting trial with second to last
                Files.Fillers(Indx_G_F).Trials(end+1, :) = Switch_Trial; % appends switch trial to end of filler for later use
            else
                Trial = [Trial; Filler]; % add sentence to trial
            end
        end
        
        All_Trials.(Type)(Indx_Type).Matrix = Trial;
        Indx_All = Indx_All + size(Trial, 1);
    end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reorder & Save to CSV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load trial information to tables
Indexes = [Indx_T, Indx_D];
Trial_Number = 0;
All_Sentences = table();

for Indx_Type = 1:2 % loop through trial and dummy
    % Create empty tables
    Full_CSV = cell2table(cell([Indx_All, numel(Full_Header)]), 'VariableNames', Full_Header);
    Summary_CSV = cell2table(cell([Tot_Trials, numel(Summary_Header)]), 'VariableNames', Summary_Header);
    
    Indx_All = 0; % keeps track of the row in the Full CSV
    Type = Types{Indx_Type};
    Indx = Indexes(Indx_Type); % get max trials for current trial type
    
    for Indx = 1:Indx % loop through trials
        Trial_Number = Trial_Number + 1;
        
        % Retrieve trials from matrix
        Matrix = All_Trials.(Type)(Indx).Matrix; % Trial Index, Corpus, Gender, Duration
        Rows = size(Matrix, 1);
        Matrix = Matrix(Rows:-1:1, :); % flip matrix, so test is last sentence
        
        % create new file name
        Trial_Filename = [Type, '_Trial_', num2str(Trial_Number, Padding), '_', Genders{Matrix(end, 3)}(1), '.wav'];
        
        % Load data to full CSV table
        Start = Sentence_Gap;
        for Row = 1:Rows % loop through each sentence in trial
            Indx_All = Indx_All + 1;
            
            % Retrieve sentence properties from matrix
            Corpus = Corpus_Folders{Matrix(Row, 2)}(1:end-1);
            Gender = Genders{Matrix(Row, 3)};
            Index = Matrix(Row, 1);
            Table = Files.(Corpus)(Matrix(Row, 3)).Table;
            Duration = Table.Duration(Index);
            End = Start + Duration;
            
            % Sentence type
            Test = 0; % when sentence is a filler
            if Row == Rows % last sentence is the test sentence
                Test = 1;
            end
            
            % Fill Full CSV
            Full_CSV.Trial_Number{Indx_All} = Trial_Number;
            Full_CSV.Trial_Filename{Indx_All} = Trial_Filename;
            Full_CSV.Audio_Filename(Indx_All) = Table.New_Filename(Index);
            Full_CSV.Type{Indx_All} = Indx_Type;
            
            Full_CSV.Test_1{Indx_All} = Test;
            Full_CSV.Order{Indx_All} = Row;
            Full_CSV.Gender{Indx_All} = Gender;
            Full_CSV.Corpus{Indx_All} = Corpus;
            Full_CSV.Old_Number{Indx_All} = Index;
            
            Full_CSV.Start_Time{Indx_All} = Start;
            Full_CSV.Duration{Indx_All} = Duration;
            Full_CSV.End_Time{Indx_All} = End;
            Full_CSV.Sentence(Indx_All) = Table.Sentence(Index);
            
            Start = End + Sentence_Gap; % next sentence starts after end and gap
        end
        
        % fill Summary CSV
        Summary_CSV.Trial_Number{Indx} = Trial_Number;
        Summary_CSV.Trial_Filename{Indx} = Trial_Filename;
        Summary_CSV.Gender{Indx} = Matrix(end, 3);
        Summary_CSV.Duration{Indx} = End;
        Summary_CSV.All_Sentences{Indx} = Rows;
        Summary_CSV.Sentence(Indx) = Table.Sentence(Index);
        Summary_CSV.Type{Indx} = Indx_Type;
    end
    
    
    % reduce to max number of rows
    Full_CSV = Full_CSV(1:Indx_All, :); % removes extra spaces
    Summary_CSV = Summary_CSV(1:Indx, :); % removes extra spaces
    
    % save to big table
    All_Sentences = [All_Sentences; Full_CSV];
    
    % save to csv
    writetable(Summary_CSV, [CSV_Folder, Type, '_Trials_Summary.csv'])
    writetable(Full_CSV, [CSV_Folder, Type, '_Trials_Full.csv'])
end

% save csv of all sentences used
writetable(All_Sentences, [CSV_Folder, 'All_Sentences.csv'])



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Save Audio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_F = Types
    Assemble_Trials(Main_Folder, [Indx_F{1}, '\'], [Indx_F{1}, '_Trials_Full.csv'])
end

% clear

