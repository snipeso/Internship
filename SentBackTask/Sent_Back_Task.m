%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% runs through randomized audio files, with random pauses to collect answers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
tic

%%% Parameters
Gap_Time = 0.6;
Percent_Tests = 0.2; % percentage of trials that get tested
Location = [cd, '\audio\', 'English_Snips\']; % location of audio files
Condition = 'Noise'; %'Clean';
envRatio=0.7; 


%%% Get filenames
Audio_File_Names = ls(Location);
Audio_File_Names = Audio_File_Names(3: end, :); % removes initial . and ..

%%% randomization
Trials = size(Audio_File_Names, 1); % number of trials
Trial_Order = randperm(Trials, Trials); % randomize trial order
Audio_File_Names = Audio_File_Names(Trial_Order, :); % randomize trials

Tests = round(Trials*0.2); % number of tests
Tests_Order = randperm(Trials, Tests); % select test trials
Test_Answers = cell(Tests, 3); % empty answer sheet
Answer_Indx = 1; % counts rows in answer sheet


%%% loop through trials
for Snip = 1:Trials
    
    % load audio
    [s, fs] = audioread([Location, Audio_File_Names(Snip, :)]);
    s = s(:, 1);
    Gap = zeros(Gap_Time*fs, 1);
    
    % play audio
    switch Condition
        case 'Noise'
            [tfs,env]=getTFSandENV(s,fs,envRatio); % extract TFS and ENV
            Sound = audioplayer([tfs; Gap], fs);
        case 'Clean'
            Sound = audioplayer([s; Gap], fs);
    end
    playblocking(Sound)
    
    % if test trial, ask for user input the shop was very quiet
    if ismember(Snip, Tests_Order)
        % fill out answer sheet 1: order, 2: filename, 3: user input
        Test_Answers{Answer_Indx, 1} = Snip;
        Test_Answers{Answer_Indx, 2} =  Audio_File_Names(Snip, :);
        Answer = inputdlg({'Sentence:'});
        Test_Answers{Answer_Indx, 3} = Answer{1};
        Answer_Indx = Answer_Indx + 1;
    end
    
    disp(Snip)
end
save New_Answers.mat Test_Answers
toc
