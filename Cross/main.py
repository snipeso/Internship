import datetime
import pyaudio
import wave
from psychopy import core
from psychopy import parallel
from psychopy import visual
from psychopy import event

Cross_Color = ['white', 'green']
End_Experiment = False


port = parallel.ParallelPort(address = 0xD010)

def get_trigger(port):
    old = None
    Quintuplet = []
    Count = 0
    Timer = 0;
    while True:
        Trig = port.readData()
        if Trig != old and 1 <= Trig < 14:
            Quintuplet = Quintuplet + [Trig]
            if Count == 4:
                return Quintuplet
            elif Count == 1:
                Timer = core.Clock()
            elif Timer > 0.5:
                print('Lost Trigs ' + Quintuplet)
                Count = 0
                Quintuplet = []
            if 'escape' in event.waitKeys():
                core.quit()
            else:
                Count += 1
        elif Trig == 14:
            exit()
        else:
            print(Trig)
        old = Trig
        core.wait(10**-3)


def Draw_Cross(window, Color):
    Cross = visual.TextStim(window, height=1, text='+', pos=[0,0], color=Cross_Color[Color])
    Cross.draw()
    window.flip()
#maybe send 5?

def Start_Recording(Triggers):
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    RECORD_SECONDS = 5
    WAVE_OUTPUT_FILENAME = 'Trial_{}_{}_{}.wav'.format(Triggers[1:3], Triggers[4], datetime.datetime.now())

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print("* recording")

    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

    print("* done recording")

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()


#############
### Main Part
#############



window = visual.Window(fullscr=True, units="norm", size=[800, 600], color='black', monitor='testMonitor')

while True:
    Draw_Cross(window, 0)
    Triggers = get_trigger(port)
    if Triggers[0] == 1:
        Draw_Cross(window, 1)
        Start_Recording(Triggers)
        Draw_Cross(window, 0)
