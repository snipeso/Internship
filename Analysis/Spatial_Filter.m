close all
%%% Set parameters
Main_Folder = 'C:\Users\Sophia\Projects\Internship\Experiment\';
Session_Num = 14;
Analysis_Parameters

Dataset = ['S', num2str(Session_Num, Padding), '_All_From_Start.set'];

Conditions = [7, 8]; % which conditions to get filters of
ERP_Limits = [-0.4, 1];
Parameters = struct();
N1_Window = [.180, .280];

% auditory
Parameters(7).Merge_Channels = [16 47 17 ]; %[42, 43, 47]; % Electrodes to average FC1, FC2, Cz
Parameters(7).Plot_Channels = []; % plot ERP of specific channels
Parameters(7).Split_by_Sentence = false; % only use if you've already run "chop _sentences" % TODO: call chop sentences function

% tactile
Parameters(8).Merge_Channels =  [42 12 43];  % Electrodes to average FC1, FC2, Cz
Parameters(8).Plot_Channels = []; 
Parameters(8).Split_by_Sentence = false;

% plot parameters
Interval = .02; % seconds spacing of topologies

%%% Load data

% brain data
EEG = pop_loadset(Dataset, EEG_Folder);

% Session trial information
Trials = readtable(CSV_Path);
Trials = Trials(Trials.Type == Type & ismember(Trials.Run, Runs) & Trials.EEG == 1, :);

% get sentence information (not always used)
% [Times, Tot_New_Epochs] = Chop_Sentences;

% initialize variables
fs = EEG.srate;
epochLimits= [EEG.xmin, EEG.xmax]; % epoch onset and offset times relative to trigger
Whole_Window = epochLimits(2)-epochLimits(1); % time of epoch
Start_Points = (N1_Window(1)-epochLimits(1))*fs:fs*Interval:(N1_Window(2)-epochLimits(1))*fs;
ERP_Window = fs*(ERP_Limits(2)-ERP_Limits(1)); % converts limits to range
Figure_Indx = 0;
Indx_Ch = 1;
chWeights = ones(size(EEG.chanlocs))';
Peak = zeros(1, length(Conditions));

%%%%%%%%%%%%%%%%%
%%% Safety checks
%%%%%%%%%%%%%%%%%

% check that number of trials in CSV and epoch numbers match
CSV_Trials = size(Trials, 1);
Epochs = size(EEG.data, 3);
if CSV_Trials ~= Epochs
    error(['trial numbers do not match. CSV: ', num2str(CSV_Trials), ' EEG data: ', num2str(Epochs)])
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% average data into ERPs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_C = Conditions % loop through conditions
    
    % load condition specific variables
    Trial_IDs = Trials.Trial_Number(Trials.Condition == Indx_C); % in order of presentation
    Epoch_Indexes = find(Trials.Condition == Indx_C);
    EEG_C = EEG.data(:, :, Epoch_Indexes); % epochs of the current condition. chanels, by time by epoch
    
    %%% OPTIONAL: create new epochs based on sentence starts
    if Parameters(Indx_C).Split_by_Sentence
        
        % create empty matrix for new epochs
        New_Data = zeros(EEG.nbchan, ERP_Window); % creates 2D empty matrix Channels x Time
        New_Data(:, :, Tot_New_Epochs) = zeros(EEG.nbchan, ERP_Window); % creates 3D matrix of Channels x Time x Epochs
        
        % fill in new epochs
        Epoch_Counter = 0; % for later cropping
        for Indx_T = 1:length(Trial_IDs) % loop through trials of current condition
            Trial_ID = Trial_IDs(Indx_T);
            Starts = round(fs*(epochLimits(1) + Times(Trial_ID).Starts));  % get start positions for each sentence within the EEG large epoch
            for Start = 1:length(Starts) % loop through sentences
                Epoch_Counter = Epoch_Counter + 1;
                Epoch = EEG_C(:, Start:(Start + ERP_Window - 1), Indx_T); % cut out sentence
                New_Data(:, :, Epoch_Counter) = Epoch;
            end
        end
        EEG_C = New_Data(:, :, 1:Epoch_Counter); % select only used epochs
    else
        Start_Epoch = round((ERP_Limits(1) - epochLimits(1))*fs); %TODO: FIX time selection
        if Start_Epoch == 0
            Start_Epoch = 1;
        end
        EEG_C = EEG_C(:, Start_Epoch:(Start_Epoch + ERP_Window -1 ), :);
    end
    
    %%% average trials
    ERP_C = mean(EEG_C, 3); % averaging epochs
    
    
    %%% plot topologies at different onsets
    Figure_Indx = Figure_Indx + 1;
    figure(Figure_Indx)
    for Indx_P = 1:length(Start_Points)
        % average all timepoints in interval
        Map_Values = mean(ERP_C(:, Start_Points(Indx_P):(Start_Points(Indx_P)+Interval*fs)), 2);
        
        subplot(2, round(length(Start_Points)/2), Indx_P) % plot on two rows
        topoplot(Map_Values, EEG.chanlocs, 'electrodes', 'numbers');
%     topoplot(Map_Values, EEG.chanlocs, 'style', 'map', 'electrodes', 'on');
        title(num2str(Start_Points(Indx_P)/fs + epochLimits(1) - .01))
    end
    set(gcf,'color', [0.918 0.894 0.894]);
    
    %%% plot specific channel ERPs
    Check = Parameters(Indx_C).Plot_Channels;
    
    % get overal limits for plot
    Min = min(min(ERP_C));
    Max = max(max(ERP_C));
    Time = linspace(ERP_Limits(1), ERP_Limits(2), ERP_Window);
    
    % plot each channel
    for Indx_P = 1:length(Check)
        Figure_Indx = Figure_Indx + 1;
        figure(Figure_Indx)
        
        plot(Time, ERP_C(Check(Indx_P), :))
        ylim([Min, Max])
        
        title(num2str(Check(Indx_P)))
        disp(num2str(Check(Indx_P)))
    end
    
    
    %%% merge channels
    ERP_Sensory = mean(ERP_C(Parameters(Indx_C).Merge_Channels, :), 1); % averaging epochs
    Figure_Indx = Figure_Indx + 1;
    figure(Figure_Indx)
    subplot(2, 1, 1)
    plot(Time, ERP_Sensory, 'Color', [0.216 0.451 0.608], 'LineWidth', 2);
    [~, N1_Position] = max(ERP_Sensory(round((N1_Window(1)-epochLimits(1))*fs):round((N1_Window(2)-epochLimits(1))*fs)));
    N1_Position = N1_Position + round((N1_Window(1)-epochLimits(1))*fs) - 1;
    chWeights(:, Indx_Ch) = ERP_C(:, N1_Position);
    Peak(Indx_C) = Time(N1_Position);
    hold on
    plot(Time(N1_Position), ERP_Sensory(N1_Position), 'o', 'MarkerSize', 2)
    text(Time(N1_Position), double(ERP_Sensory(N1_Position)), num2str(Time(N1_Position)))
    hold off
    subplot(2, 1, 2)
    topoplot(chWeights(:, Indx_Ch), EEG.chanlocs, 'electrodes', 'numbers');
    xlabel('Time (s)')
    set(gcf,'color', [0.918 0.894 0.894]);
    Indx_Ch = Indx_Ch + 1;
end


chWeights = chWeights([1:62, 65], :);
Peak(Peak == 0) = [];
save([EEG_Folder, 'S' num2str(Session_Num, Padding), '_channelWeights.mat'], 'chWeights', 'Peak', 'Parameters')