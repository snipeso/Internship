function new_data = Get_Stimuli(stimuli, row, new_data, fsNew, triggers, Data_Range)

%%% set parameters
fs = 16000;
Filter_order = 3;
freq_range = 16;

new_data = zeros(1, Data_Range);

stimuli = double(stimuli); % filterning needs doubles
s = stimuli(row, :);

% crop stimuli from start of chosen trigger
% recreate original trigger
Trial = triggers(1).Trial; 
Condition = str2double(triggers(3).type);
End_Code = 1;
if Condition == 8
    End_Code = 4;
end


[~, Start_Trigger_Pattern] = Triggers(2, Trial, Condition, 1); % 2 is for start, 1 doesnt matter
[~, Stop_Trigger_Pattern] = Triggers(End_Code, Trial, Condition, 1); %  1 doesnt matter

% find position of first point of recreated trigger in stimuli trigger array
Start_Data = strfind(stimuli(5, :), Start_Trigger_Pattern'); % TODO: check that this is giving correct cutoff)
End_Data =  strfind(stimuli(5, :), Stop_Trigger_Pattern');

if isempty(Start_Data) || isempty(End_Data)
    warning(['Couldnt find trigger ', num2str([Trial, Condition])])
    [~, Start_Trigger_Pattern] = Triggers(2, Trial, -2, 1); % 2 is for start, 1 doesnt matter
    [~, Stop_Trigger_Pattern] = Triggers(End_Code, Trial, -2, 1); %  1 doesnt matter
    Start_Trigger_Pattern = Start_Trigger_Pattern(1:find(Start_Trigger_Pattern > 0, 1, 'last'));
    Stop_Trigger_Pattern = Stop_Trigger_Pattern(1:find(Stop_Trigger_Pattern > 0, 1, 'last'));
    
    % find position of first point of recreated trigger in stimuli trigger array
    Start_Data = strfind(stimuli(5, :), Start_Trigger_Pattern'); % TODO: check that this is giving correct cutoff)
    End_Data =  strfind(stimuli(5, :), Stop_Trigger_Pattern');
end

stimuli = stimuli(:, Start_Data:End_Data); % chop
s = stimuli(row, :);


if not(any((stimuli(row, :))))
    return
elseif (Condition == 7 && row > 2) || (Condition == 8 && row < 3)
    return
end

%%% calculate envelope
[b,a]=butter(Filter_order,freq_range/(fs/2),'low');
envel= abs(hilbert(s));
envel=filtfilt(b,a,envel); % zero-phase filtering (doubles the filter order)
s = envel;


%%%



%%% normalize
s = (s-min(s))/(max(s)-min(s));

%%% Resample stimuli
t_temp = timeseries(s,[0:1/fs:(numel(s)/fs)-1/fs]);
t = resample(t_temp,[0:1/fsNew:(numel(s)/fs)-1/fs]);
s = squeeze(t.Data);

%%% insert in data
new_data(1:length(s)) = s'; % replace new data in zero padded vector

if length(new_data) > Data_Range
     warndlg(['Stimuli is larger than eeg by ', num2str((length(new_data)-Data_Range)/fsNew), 'seconds'])
     new_data = new_data(1:Data_Range);
end

new_data = single(new_data);

