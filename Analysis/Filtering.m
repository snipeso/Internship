function Filtering(Session_Num, Runs)
% filter, rereference, and add stimuli to EEG datasets
% produces first a Cleaned set then a filtered.
% set parameters
Analysis_Parameters % needs to be after Session_Num

epochLimits= [-15.3, -0.3]; % [-15 0]; % epoch onset and offset times relative to trigger (def:[-15 0]s)
triggers={'Stop'};     % trigger from which to start

CSV = readtable(CSV_Path);
CSV = CSV(CSV.EEG == 1, :);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Clean Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_R = Runs % loop for runs
    
    %%% import data
    Run_Name = ['S', num2str(Session_Num, Padding), '_R', num2str(Indx_R, Padding)];
    
    Cleaned_Set = [Run_Name, '_Cleaned.set'];
            % get stimuli
        load([Stimuli_Folder, Run_Name, '.mat'], 'stimuli')
        
    if exist([EEG_Folder, Cleaned_Set])
        EEG = pop_loadset(Cleaned_Set, EEG_Folder);
        EEG = eeg_checkset(EEG);
    else
        Set_Name = [Run_Name, '_Raw.set'];

        % get raw set
        EEG = pop_loadset(Set_Name, EEG_Folder);
        EEG = eeg_checkset(EEG);




        %%% Preprocessing (could probably be in a better order)

        % bandpass filter for preprocessing
        EEG = pop_eegfiltnew(EEG,HPeeg,LPeeg);

        % Add channel information
        EEG.chanlocs(63).labels = 'EOGhorizontal';
        EEG.chanlocs(64).labels = 'EOGvertical';
        EEG = pop_chanedit(EEG,'lookup',[Main_Folder, 'standard-10-5-cap385.elp'],'append',64,'changefield',{65 'labels' 'TP9'},...
            'lookup',[Main_Folder 'standard-10-5-cap385.elp'],'setref',{'1:65' 'TP9'});

        % clean data with ASR
        EEG = clean_drifts(EEG,[.25 .75]);
        EEG = clean_asr(EEG);
        EEG = pop_interp(EEG,EEG.chanlocs,'spherical');

        % rereference
        EEG = pop_reref(EEG, [1:62 65] ,'refloc',struct('labels',{'TP9'},'type',{''},...
            'theta',{-108.393},'radius',{0.66489},'X',{-23.3016},'Y',{70.0758},'Z',{-42.0882},...
            'sph_theta',{108.393},'sph_phi',{-29.68},'sph_radius',{85},'urchan',{65},...
            'ref',{'TP9'},'datachan',{0}),'keepref','on');
        EEG = eeg_checkset(EEG);

        % save cleaned EEG
        pop_saveset(EEG,'filename',['S', num2str(Session_Num, Padding), '_R', num2str(Indx_R, Padding), '_Cleaned.set'],...
            'filepath', EEG_Folder, 'check','on',...
            'savemode','onefile','version','6');
    
    end
    
    % bandpass filter for envelope analysis
    EEG = pop_eegfiltnew(EEG,HPenv,LPenv);
    
    
    % downsample
    EEG = pop_resample(EEG,fsNew);
    EEG = eeg_checkset(EEG);
    
    
    %%% insert stimuli in EOG channels
    
    % whipe EOG data
    EEG.data(63, :) = 0;
    EEG.data(64, :) = 0;
    
    % get chopped stimuli per trial
    for Indx_E = 1:size(EEG.event, 2) % loop through all events
        
        if not(strcmp(EEG.event(Indx_E).type, 'Start')) || not(any(CSV.Trial_Number == EEG.event(Indx_E).Trial))% skip events that don't indicate start of trial or arent in CSV
            continue
        else
            % define trial position in array
            Start = round(EEG.event(Indx_E).latency);
            End = round(EEG.event(Indx_E + 3).latency);
            Data_Range = End - Start + 1;
            
            % define range of trial events
            Event_Range = Indx_E:Indx_E+5;
            
            % make sure there is a stop trigger and it's for the same trial as start trigger
            if not(strcmp(EEG.event(Indx_E + 3).type, 'Stop')) || EEG.event(Indx_E + 3).Trial ~= EEG.event(Indx_E).Trial
                warndlg(['There is a problem with triggers ', num2str( EEG.event(Indx_E).Trial), 'of run ', numstr(Indx_R)])
            end
            
            % replace EOG data for that trial with the stimuli data
            EEG.data(63, Start:End) = Get_Stimuli(stimuli, 1, EEG.data(63, :), fsNew, EEG.event(Event_Range), Data_Range); % audio
            EEG.data(64, Start:End) = Get_Stimuli(stimuli, 3, EEG.data(64, :), fsNew, EEG.event(Event_Range), Data_Range); % tactile
        end
    end
    
    % save as SET file
    pop_saveset(EEG,'filename',['S', num2str(Session_Num, Padding), '_R', num2str(Indx_R, Padding), '_Filtered.set'],...
        'filepath', EEG_Folder, 'check','on',...
        'savemode','onefile','version','6');
    
    clear EEG
end

