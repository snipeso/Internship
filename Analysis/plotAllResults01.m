clear all;clc;eeglab;close all;
subjInds=[1:5];


nbSubj=numel(subjInds);
indSubjN=0;
figure;

for indSubj=subjInds % loop for subjects
    if indSubj>9
        subjInd=num2str(indSubj);
    else
        subjInd=['0' num2str(indSubj)];
    end
    indSubjN=indSubjN+1;
    dp = 'C:\Users\Sophia\Projects\Internship\Analysis\Results_behavior_data\';
    dpIN = ['\\ca-um-nas201\fpn_rdm$\DM0703_LR_SSTASP\10_DataAnalysis\S' subjInd '\'];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    condLabels={'-300','-200','-100','0','+100','+200'};
    refSigLabels={'audStim','tacStim'};
  
    %% plot behavioral accuracy (benefit) 
    load([dp '\S' subjInd '_behavior_results.mat']); % imports 'behData'
    % 1 Sequence number (WAV)
    % 2 Gender (1:m, 2:f)
    % 3 Type (1:long, 2:short)
    % 4 Run
    % 5 Condition
    % 6 nbWordsCorrect
    % 7 totalNbWords
    for indCond=1:numel(condLabels)+1 % exclude T
        indTrials=find(behData(:,5)==indCond);
%         indTrials=intersect(find(behData(:,5)==indCond),find(behData(:,3)==1)); % use only long trials
        if indSubj==1 && indCond==7 % run 1 of subject 1 contained only A trials --> discard from current analysis
            indTrials=setdiff(indTrials,find(behData(:,4)==1));
        end
        avgAcc(indCond)=sum(behData(indTrials,6))/sum(behData(indTrials,7));
        clear indTrials
    end
    for indCond=1:numel(condLabels) % exclude A and T
        avgAcc(indCond)=avgAcc(indCond)-avgAcc(end); % subtract A from AT
    end
    subplot(4,nbSubj+1,indSubjN+3*(nbSubj+1));
    h=line([4 4],[-30 30]); set(h,'color',[0 0 0]); hold on;
    tempNans=repmat(nan,1,numel(condLabels));
    for indCond=1:numel(condLabels) % exclude A and T
        tempNansN=tempNans;
        tempNansN(indCond)=100*avgAcc(indCond);
        colors=repmat(0.3+(indCond-1)*(7/50),1,3);
%         if indCond==4;colors=[1 0 0];end; % red: SOA=0ms
        bar(tempNansN,'BarWidth',0.85,'FaceColor',colors,'linewidth',1); hold on;
    end
    title(['Recognition accuracy (BL: ' num2str(100*avgAcc(end),2) '%)'],'FontWeight','Bold');xlabel('Tactile lag (ms)');ylabel('Benefit (pp)');
    axis([0.5 numel(condLabels)+0.5 -20 20]);set(gca,'ytick',[-20:10:20],'xtick',1:numel(condLabels),'xticklabel',condLabels);
    subjData{1}(indSubjN,:)=avgAcc;
    clear avgAcc behData tempNans tempNansN

    
    %% plot neural decoding accuracy (benefit)
    load([dp,'\S' subjInd '_forwardModel_results.mat']); % imports 'TRF_R','TRF_P','TRF_model'
%     load([dp,'\S' subjInd '_forwardModel_results_tacAlign.mat']); % imports 'TRF_R','TRF_P','TRF_model'
    % {1} auditory stimulus
    % {2} tactile stimulus
    % {}(:,1) auditory channel
    % {}(:,2) tactile channel
    for indCond=1:numel(condLabels)+1 % exclude T
        avgAcc(indCond)=TRF_R{2}(indCond,1); % tactile stimulus, auditory channel
    end
    for indCond=1:numel(condLabels) % exclude A and T
        avgAcc(indCond)=avgAcc(indCond)-avgAcc(end); % subtract A from AT
    end
    subplot(4,nbSubj+1,indSubjN+2*(nbSubj+1));
    h=line([4 4],[-30 30]); set(h,'color',[0 0 0]); hold on;
    tempNans=repmat(nan,1,numel(condLabels));
    for indCond=1:numel(condLabels) % exclude A and T
        tempNansN=tempNans;
        tempNansN(indCond)=avgAcc(indCond);
        colors=repmat(0.3+(indCond-1)*(7/50),1,3);
%         if indCond==4;colors=[1 0 0];end; % red: SOA=0ms
        bar(tempNansN,'BarWidth',0.85,'FaceColor',colors,'linewidth',1); hold on;
    end
    title(['Encoding accuracy (BL: ' num2str(avgAcc(end),1) ')'],'FontWeight','Bold');xlabel('Tactile lag (ms)');ylabel('Benefit (delta R)');
    axis([0.5 numel(condLabels)+0.5 -0.1 0.1]);set(gca,'ytick',[-0.1:0.1:0.1],'xtick',1:numel(condLabels),'xticklabel',condLabels);
    subjData{2}(indSubjN,:)=avgAcc;
    clear avgAcc TRF_R TRF_P tempNans tempNansN
    
    
    %% plot TRF
    for indCond=1:numel(condLabels)+2
        avgAcc(indCond,:)=squeeze(TRF_model{2}(indCond,1,:))'; % tactile stimulus, auditory channel
    end
    for indCond=1:numel(condLabels)
        avgAcc(indCond,:)=avgAcc(indCond,:)-avgAcc(end-1,:); % subtract A 
    end
    subplot(4,nbSubj+1,indSubjN+1*(nbSubj+1));
    tmin = -400; % earliest lag used for tracking analysis
    tmax = 700;  % latest lag used for tracking analysis
    fs = 100; 
    lag_vec = tmin:(1000/fs):tmax; clear tmin tmax fs;
    h=line([0 0],[-5 5]); set(h,'color',[0 0 0]); hold on;
    h=line([min(lag_vec) max(lag_vec)],[0 0]); set(h,'color',[0 0 0]);
%     for indCond=1:numel(condLabels) % exclude A and T
%         colors=repmat((indCond-1)*(9/50),1,3);
%         if indCond==4;colors=[1 0 0];end; % red: SOA=0ms
%         plot(lag_vec,100*avgAcc(indCond,:),'color',colors,'linewidth',3); % darker gray = tactile leading more
%     end
%     plot(lag_vec,100*mean(avgAcc,1),'color',[0 0 0],'linewidth',3); % average difference curve
    plot(lag_vec,100*avgAcc(8,:),'b','linewidth',2); hold on; % blue: T condition
    plot(lag_vec,100*avgAcc(7,:),'r','linewidth',2);          % red:  A condition
%     legend('Auditory stimulus','Tactile stimulus');
    title('Envelope encoding model','FontWeight','Bold');xlabel('Cortical lag (ms)');ylabel('Response strength (au)');
    axis([-300 600 -3 3]);set(gca,'ytick',[-3:3:3]);
    subjData{3}(indSubjN,:,:)=avgAcc;
    clear avgAcc TRF_model
    
    
    %% plot spatial filter 
    load(fullfile(dp,'ChannelLocations.mat')); % imports 'CL'
    load(fullfile([dpIN 'S' subjInd '_channelWeights.mat'])); % imports 'chWeights'
    % 1 auditory-ERP filter
    % 2 tactile-ERP filter
    chWeightsNorm=chWeights(:,1)/sqrt(sum((chWeights(:,1).^2),1)); % Euclidean norm of channel weights
    subplot(4,nbSubj+1,indSubjN+0*(nbSubj+1));
    topoplot(chWeightsNorm,CL(setdiff(1:numel(CL),[63 64])));hold on;
    title('Scalp topography','FontWeight','Bold');
    h=colorbar;set(h,'YLim',[-0.3 0.3],'YTick',[-0.3:0.3:0.3],'YTickLabel',[-3:3:3]);ylabel(h,'Weight (~V)'); 
    subjData{4}(indSubjN,:)=chWeightsNorm;
    clear chWeights chWeightsNorm
    
end % loop for subjects
set(gcf,'color',[1 1 1]);


%% plot group data
indSubjN=indSubjN+1;

%% plot behavioral accuracy (benefit) 
subplot(4,nbSubj+1,indSubjN+3*(nbSubj+1));
h=line([4 4],[-30 30]); set(h,'color',[0 0 0]); hold on;
tempNans=repmat(nan,1,numel(condLabels));
for indCond=1:numel(condLabels) % exclude A and T
    tempNansN=tempNans;
    tempNansN(indCond)=100*mean(subjData{1}(:,indCond),1);
    colors=repmat(0.3+(indCond-1)*(7/50),1,3);
    bar(tempNansN,'BarWidth',0.85,'FaceColor',colors,'linewidth',2); hold on;
end
errorbar(100*mean(subjData{1}(:,1:numel(condLabels)),1), 100*std(subjData{1}(:,1:numel(condLabels)),1)./sqrt(numel(subjInds)), 'k.', 'linewidth',2);
title(['Recognition accuracy (BL: ' num2str(100*mean(subjData{1}(:,end),1),2) '%)'],'FontWeight','Bold');xlabel('Tactile lag (ms)');ylabel('Benefit (pp)');
axis([0.5 numel(condLabels)+0.5 -20 20]);set(gca,'ytick',[-20:10:20],'xtick',1:numel(condLabels),'xticklabel',condLabels);
clear tempNans tempNansS colors

%% statistical analysis of behavioral accuracy (benefit) 
X=100*subjData{1}(:,1:numel(condLabels));
% main effect of SOA?
[~,table]=anova_rm(X,'off');
disp(['main effect of SOA on behavioral benefit: p=' num2str(table{2,6},3)]); clear table;
% benefit>0 for any SOA?
for indCond=1:size(X,2)
    [~,p(indCond)]=ttest(X(:,indCond),0,0.05,'right'); 
end
[~,~,~,adj_p]=fdr_bh(p);
for indCond=1:size(X,2)
    disp(['behavioral benefit>0: condition ' condLabels{indCond} ': p=' num2str(adj_p(indCond),3) '   (uncorrected p=' num2str(p(indCond),3) ')']); 
end
clear adj_p p
% benefit>0 for pooled SOAs?
[~,p]=ttest(mean(X,2),0,0.05,'right'); 
disp(['behavioral benefit>0: pooled across conditions: p=' num2str(p,3)]); 
clear p    
disp(' ')

    
%% plot neural decoding accuracy (benefit)
subplot(4,nbSubj+1,indSubjN+2*(nbSubj+1));
h=line([4 4],[-30 30]); set(h,'color',[0 0 0]); hold on;
tempNans=repmat(nan,1,numel(condLabels));
for indCond=1:numel(condLabels) % exclude A and T
    tempNansN=tempNans;
    tempNansN(indCond)=mean(subjData{2}(:,indCond),1);
    colors=repmat(0.3+(indCond-1)*(7/50),1,3);
    bar(tempNansN,'BarWidth',0.85,'FaceColor',colors,'linewidth',2); hold on;
end
errorbar(mean(subjData{2}(:,1:numel(condLabels)),1), std(subjData{2}(:,1:numel(condLabels)),1)./sqrt(numel(subjInds)), 'k.', 'linewidth',2);
title(['Encoding accuracy (BL: ' num2str(mean(subjData{2}(:,end),1),1) ')'],'FontWeight','Bold');xlabel('Tactile lag (ms)');ylabel('Benefit (delta R)');
axis([0.5 numel(condLabels)+0.5 -0.1 0.1]);set(gca,'ytick',[-0.1:0.1:0.1],'xtick',1:numel(condLabels),'xticklabel',condLabels);
clear tempNans tempNansN     

%% statistical analysis of neural decoding accuracy (benefit)
X=100*subjData{2}(:,1:numel(condLabels));
% main effect of SOA?
[~,table]=anova_rm(X,'off');
disp(['main effect of SOA on neural benefit: p=' num2str(table{2,6},3)]); clear table;
% benefit>0 for any SOA?
for indCond=1:size(X,2)
    [~,p(indCond)]=ttest(X(:,indCond),0,0.05,'right'); 
end
[~,~,~,adj_p]=fdr_bh(p);
for indCond=1:size(X,2)
    disp(['neural benefit>0: condition ' condLabels{indCond} ': p=' num2str(adj_p(indCond),3) '   (uncorrected p=' num2str(p(indCond),3) ')']); 
end
clear adj_p p
% benefit>0 for pooled SOAs?
[~,p]=ttest(mean(X,2),0,0.05,'right'); 
disp(['neural benefit>0: pooled across conditions: p=' num2str(p,3)]); 
clear p    
disp(' ')


%% plot TRF
subplot(4,nbSubj+1,indSubjN+1*(nbSubj+1));
h=line([0 0],[-5 5]); set(h,'color',[0 0 0]); hold on;
h=line([min(lag_vec) max(lag_vec)],[0 0]); set(h,'color',[0 0 0]);
% plot(lag_vec,100*squeeze(mean(subjData{3}(:,8,:),1)),'b','linewidth',3); hold on;  % blue: T condition
% plot(lag_vec,100*squeeze(mean(subjData{3}(:,7,:),1)),'r','linewidth',3);           % red:  A condition
%     legend('Auditory stimulus','Tactile stimulus');
lineProps.width=3;
lineProps.col={[0 0 1],[0 0 1],[0 0 1]};
mseb(lag_vec,100*squeeze(mean(subjData{3}(:,8,:),1))',100*squeeze(std(subjData{3}(:,8,:),1))'./sqrt(numel(subjInds)),lineProps);hold on; % blue: T condition
lineProps.col={[1 0 0],[1 0 0],[1 0 0]};
mseb(lag_vec,100*squeeze(mean(subjData{3}(:,7,:),1))',100*squeeze(std(subjData{3}(:,7,:),1))'./sqrt(numel(subjInds)),lineProps);hold on; % red:  A condition
title('Envelope encoding model','FontWeight','Bold');xlabel('Cortical lag (ms)');ylabel('Response strength (au)');
axis([-300 600 -3 3]);set(gca,'ytick',[-3:3:3]);

%% include markers for statistical significance
colors=[1 0 0; 0 0 1; 1 0 1];
for indCond=7:8+1
    for indSmp=1:size(subjData{3},3)
        if indCond<9
            [~,p(indSmp)]=ttest(100*subjData{3}(:,indCond,indSmp),0,0.05,'both');
        else
            [~,p(indSmp)]=ttest(100*subjData{3}(:,7,indSmp)-100*subjData{3}(:,8,indSmp),0,0.05,'both');
        end
    end
    [~,~,~,adj_p]=fdr_bh(p);
    adj_p=p;
    signMarker=nan(size(adj_p));
    signMarker(find(adj_p<0.05))=1;
%     plot(signMarker*(-2.7+(indCond-7)*0.1),'k-','color',colors(indCond-6,:),'linewidth',4);
    clear adj_p p signMarker
end

    
%% plot spatial filter 
subplot(4,nbSubj+1,indSubjN+0*(nbSubj+1));
topoplot(mean(subjData{4},1),CL(setdiff(1:numel(CL),[63 64])));hold on;
title('Scalp topography','FontWeight','Bold');
h=colorbar;set(h,'YLim',[-0.3 0.3],'YTick',[-0.3:0.3:0.3],'YTickLabel',[-3:3:3]);ylabel(h,'Weight (~V)'); 


