function [Times, Max_Tot] = Chop_Sentences()
Main_Folder = 'C:\Users\Sophia\Projects\Internship\Experiment\';
Filename = 'Test_Trials_Full.csv';
Path = [Main_Folder, 'CSVs\' Filename];
Sentences = readtable(Path);

Trials = max(Sentences.Trial_Number);

Times = struct();

Max_Tot = 0;
for Indx_T = 1:Trials
    Starts = Sentences.Start_Time(Sentences.Trial_Number == Indx_T);
    Times(Indx_T).Starts = Starts';
    if Max_Tot < length(Starts)
        Max_Tot = length(Starts);
    end
end

save([Main_Folder, 'Start_Times.mat'], 'Times')

% TODO: eventually use all CSVs, not just main experiment