clear all;close all;clc;
eeglab;

folder='C:\Users\Sophia\Projects\Internship\Analysis\S1_B4\';
indSubj=1;
runInds=[4]; % indices of runs of the experiment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% set preprocessing parameters

LPeeg=45;   % upper cutoff frequency for EEG preprocessing (def:45Hz)
HPeeg=0.5;  % lower cutoff frequency for EEG preprocessing (def:0.5Hz)

LPenv=8;    % upper cutoff frequency for envelope analysis (def:8Hz)
HPenv=1;    % lower cutoff frequency for envelope analysis (def:1Hz)

fsNew=100;  % sampling rate for envelope analysis  (def:100Hz)

epochLimits=[-15 0]; % epoch onset and offset times relative to trigger (def:[-15 0]s)
triggers={'S  1'};     % triggers representing conditions (def: [1:7])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% set subject index
if indSubj<10; subjInd=[num2str(indSubj)]; else; subjInd=num2str(indSubj); end

for indRun=runInds % loop for runs
    
    %% import data
    if indRun<10; runInd=[num2str(indRun)]; else; runInd=num2str(indRun); end
    %     EEG = pop_loadbv([folder],['S' subjInd '_B' runInd '.vhdr']);
    EEG = pop_loadset(Set_Name, Main_Folder) % TODO
    EEG = eeg_checkset(EEG);
    
    %% bandpass filter for EEG preprocessing
    EEG = pop_eegfiltnew(EEG,HPeeg,LPeeg);
    
    %% provide right labels/coordinates for dataset (needed for automatic cleaning)
    EEG.chanlocs(63).labels = 'EOGhorizontal';
    EEG.chanlocs(64).labels = 'EOGvertical';
    EEG = pop_chanedit(EEG,'lookup',[folder 'standard-10-5-cap385.elp'],'append',64,'changefield',{65 'labels' 'TP9'},...
        'lookup',[folder 'standard-10-5-cap385.elp'],'setref',{'1:65' 'TP9'});
    
    %% clean data with ASR
    EEG = clean_drifts(EEG,[.25 .75]);
    EEG = clean_asr(EEG);
    EEG = pop_interp(EEG,EEG.chanlocs,'spherical');
    
    %% rereference
    EEG = pop_reref(EEG, [1:62 65] ,'refloc',struct('labels',{'TP9'},'type',{''},...
        'theta',{-108.393},'radius',{0.66489},'X',{-23.3016},'Y',{70.0758},'Z',{-42.0882},...
        'sph_theta',{108.393},'sph_phi',{-29.68},'sph_radius',{85},'urchan',{65},...
        'ref',{'TP9'},'datachan',{0}),'keepref','on');
    EEG = eeg_checkset( EEG );
    
    %% bandpass filter for envelope analysis
    EEG = pop_eegfiltnew(EEG,HPenv,LPenv);
    
    %% downsample
    EEG = pop_resample(EEG,fsNew);
    EEG = eeg_checkset(EEG);
    
    %% extract epochs
    EEG = pop_epoch(EEG,triggers,epochLimits);
    EEG = eeg_checkset(EEG);
    
    %% save as SET file
    pop_saveset(EEG,'filename',['S' subjInd '_B' runInd '_inclDummy.set'],...
        'filepath',[folder],'check','on',...
        'savemode','onefile','version','6');
    
    %% exclude dummy trials
    indDummyTrials=[3, 5]; % get indices of dummy trials (from PRT)
    EEG = pop_select( EEG,'notrial',indDummyTrials); % remove those trials
    pop_saveset(EEG,'filename',['S' subjInd '_B' runInd '.set'],...
        'filepath',[folder],'check','on',...
        'savemode','onefile','version','6');
    
    clear EEG
    
end % loop for runs

% % %%%
% % %%% append all runs using default settings
% % for r=1:length(filelist{s})    % loop for runs
% %     loadName      = [num2str(s),'_sce', num2str(r), '_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}];
% %     EEG           = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
% %     EEG           = eeg_checkset( EEG );
% %     allEEG(r)     = EEG;
% %     allIndices(r) = r;
% % end  % loop for runs
% % EEG = pop_mergeset( allEEG, allIndices, '0');
% % EEG = eeg_checkset( EEG );
% % clear allEEG;
% % clear allIndices;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% build matrices for decoding analysis

%% build EEG matrix
% load SET file
% make matrix: trial*channel*time (for EEG data)
% make matrix: trial*channel*time (for condition indices; channel and time are redundant)
% append subsequent runs
% repeat for regularization data


%% build tactile matrix
% load tactile stimuli
% downsample (100Hz)
% extract epochs as for EEG (see above)
% make matrix: trial*channel*time (for tactile stimuli; channel is redundant)
% append subsequent runs
% repeat for regularization data


%% build audio matrix
% load auditory stimuli
% extract envelope, lowpass (16Hz) and downsample (100Hz) it
% extract epochs as for EEG (see above)
% make matrix: trial*channel*time (for extracted envelopes; channel is redundant)
% append subsequent runs
% repeat for regularization data
