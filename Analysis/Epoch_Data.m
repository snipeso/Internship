function Epoch_Data(Session_Num, Runs, epochLimits, triggers)
% cuts up filtered data into epochs, and merges into one matrix per sesison

Analysis_Parameters

% epochLimits= [-15.3, -0.3]; % [-15 0]; % epoch onset and offset times relative to trigger (def:[-15 0]s)
% triggers={'Stop'};     % trigger from which to start


CSV = readtable(CSV_Path);
CSV = CSV(CSV.EEG == 1, :);

% load data for each run
for Indx_R = Runs
    Set_Name = ['S', num2str(Session_Num, Padding), '_R' ,num2str(Indx_R, Padding), '_Filtered.set'];
    EEG = pop_loadset(Set_Name, EEG_Folder);
    EEG = eeg_checkset(EEG);
    
    Events = struct2table(EEG.event); % maybe this process should be elsewhere?
    Events = Events(strcmp(Events.code, 'Stimulus'), :); % removes non related triggers
    Events.Trial = Events.Trial; % turns cells to array
    Events_Redux = Events(ismember(Events.Trial, CSV.Trial_Number), :);
    EEG.event = table2struct(Events_Redux);
    All_EEG(Indx_R) = EEG;
end

% merge
EEG = pop_mergeset(All_EEG, Runs, '0');

% chop
EEG = pop_epoch(EEG,triggers,epochLimits);
EEG = eeg_checkset(EEG);

% save
pop_saveset(EEG,'filename',['S', num2str(Session_Num, Padding), '_Epoched_Runs.set'],...
    'filepath', EEG_Folder, 'check','on',...
    'savemode','onefile','version','6');

