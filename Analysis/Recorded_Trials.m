%%% script that checks what was recorded and adds two collumns to the PRT: Recordings, EEG

for Session_Num = 14
    
    % load parameters
    Analysis_Parameters
    disp(Session)
    CSV_Path_temp =  [Main_Folder, Session, '\', Session, '_PRT.csv']; % temp for old sessions
    CSV = readtable(CSV_Path_temp);
    
    
    % create new columns
    Trials_Tot = size(CSV, 1);
    Runs_Tot = max(unique(CSV.Run));
    CSV.Recording = zeros(Trials_Tot, 1);
    CSV.EEG = zeros(Trials_Tot, 1);
    
    % loop through runs
    for Indx_R = unique(CSV.Run)'
        
        %%% load recordings info
        Recordings_Folder = [Main_Folder, Session, '\', Session, '_Recordings\', Session, '_R', num2str(Indx_R, Padding), '\'];
        
        % if no folder, skip run
        if not(exist(Recordings_Folder, 'dir')) 
            disp(['No run ', num2str(Indx_R)])
            continue
        end
        
        Filenames = ls(Recordings_Folder);
        Recordings = table(); 
        
        % extract info from filenames
        Recordings.Filename = Filenames(3:end, :);
        Recordings.Trial_Number = str2num(Filenames(3:end, 7:9));
        Recordings.Condition = str2num(Filenames(3:end, 10));
        Recordings.Timestamp = Filenames(3:end, 16:29);
        Recordings = sortrows(Recordings, 'Timestamp'); % these extras are in case I want to do fancy things later
        
        % check if there are extra recordings not in table
        if not(all(ismember(Recordings.Trial_Number, CSV.Trial_Number(CSV.Run == Indx_R , :))))
            warning(['Recording without a matching trial in PRT in Session ', Session, ' Run ', num2str(Indx_R)])
        end
        
        % warning if there is a condition outside range 1-7
        if unique(Recordings.Condition) > 7
            warning(['Recording with weird condition in ', Session, ' Run ', num2str(Indx_R)])
        end
        
        %%% load events in EEG data
        Set_Name = ['S', num2str(Session_Num, Padding), '_R',num2str(Indx_R, Padding), '_Raw.set'];
        EEG = pop_loadset(Set_Name, EEG_Folder);
        EEG = eeg_checkset(EEG);
        Events = struct2table(EEG.event);
        Events = Events(strcmp(Events.code, 'Stimulus'), :); % removes non related triggers
        Events.Trial = cell2mat(Events.Trial); % turns cells to array
        
        
        %%% loop through trials in PRT to see if there is corresponding data
        Trials_Run = size(CSV(CSV.Run == Indx_R, :), 1);
        for Indx_T = 1:Trials_Run
            Trial = CSV(CSV.Run == Indx_R & CSV.Run_Order == Indx_T, :);
            
            % check if there is a recording && if the condition matches (this skips errors and tactile conditions)
            if any(Recordings.Trial_Number == Trial.Trial_Number) && Recordings.Condition(Recordings.Trial_Number == Trial.Trial_Number) == Trial.Condition
                CSV.Recording(CSV.Run == Indx_R & CSV.Run_Order == Indx_T) = 1;
            end

            % checks if:
            % -  bimodal trials have 6 triggers
            % - unimodal trials have 12 triggers
            % - unimodal dummy trials have 6
            Epochs = Events(Events.Trial == Trial.Trial_Number, :);
            if (Trial.Condition < 7 && size(Epochs, 1) == 6) || (Trial.Condition >= 7 && size(Epochs, 1) == 12) || (Trial.Condition == 7 && Trial.Type == 2 && size(Epochs, 1) == 6)
                CSV.EEG(CSV.Run == Indx_R & CSV.Run_Order == Indx_T) = 1;
            end
        end
        
        
    end
    
    %%% save to PRT
    writetable(CSV, CSV_Path)
    writetable(CSV(CSV.Recording == 1, 1:end-2), [Main_Folder,  Session, '\', Session, '_PRT_Recordings.csv'])
end