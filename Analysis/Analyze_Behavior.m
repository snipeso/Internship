function test()

Sessions_Indexes = 1:13; %[2:5, 10]% [1, 6:9]; % for not using some sessions

if not(exist(Data_Table, 'var'))
Data_Table = table();
Figure_Indx = 0;
Bins = 3;

%%% load scored data
for Indx_S = Sessions_Indexes
    Session_Num = Indx_S;
    Analysis_Parameters
    
    Table = readtable(Scores_Path);
    Table.Properties.VariableNames{1} = 'Trial_Number';
    Table = Table(ismember(Table.Run, Session_Info(Indx_S).Runs), :);
    
    % set rank
    data = Table.Correct./Table.Total;
    data_sorted = sort(data);
    [~, rnk] = ismember(data,data_sorted);
    Table.Rank = rnk;
    Table.Order = [1:length(data)]';
    
    % Thirds
    data = Table;
    Third = floor(size(data, 1)/Bins);
    data.Bin = Bins*ones(size(data, 1), 1); % this makes sure the leftovers are also the max bin
    Start = 1;
    for Indx_B = 1:Bins
        End = Start + Third;
        data.Bin(Start:End) = Indx_B;
        Start = End + 1;
    end
    
    Table = sortrows(data, 'Order');
    
    Data_Table = [Data_Table; Table];
end

Data_Table = Data_Table(Data_Table.Trial_Number > 0, :); % remove possible empty spaces


Figure_Indx = Figure_Indx + 1;
figure(Figure_Indx)
Plot_All(Data_Table, 'Condition', 'Proportion', Condition_Labels)
title('% correct by condtion')
ylim([0, 1])

% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Data_Table.Random_Condition = Data_Table.Condition(randperm(size(Data_Table, 1)), 1);
% Plot_All(Data_Table, 'Random_Condition', 'Proportion', Condition_Labels)
% title('% correct randperm')

% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Plot_All(Data_Table, 'Run', 'Proportion', unique(Data_Table.Run))
% title('% correct by run')


% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Plot_All(Data_Table, 'Condition', 'Bin', Condition_Labels)
% title('Average bin by condition')

% get trial rank
% 
% Data_Table.Modality = ones(size(Data_Table, 1), 1);
% Data_Table.Modality(Data_Table.Condition == 7) = 2;
% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Plot_All(Data_Table,  'Modality', 'Bin', {'Bimodal', 'Unimodal'})
% title('Average bin by modality')
% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Plot_All(Data_Table,  'Modality', 'Proportion', {'Bimodal', 'Unimodal'})
% title('% correct by modality')
% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% [Groups, Values] = Group_Values(Data_Table, 'Condition', 'Proportion');
% [~, Max_Indx] = max(Values);
% Best = Groups(Max_Indx);
% Data_Table.Modality = 2*ones(size(Data_Table, 1), 1);
% Data_Table.Modality(Data_Table.Condition == Best) = 1;
% Plot_All(Data_Table, 'Modality', 'Proportion', {'Bimodal Best', 'Unimodal'})

% 
% [Groups, Values] = Group_Values(Data_Table, 'Trial_Number', 'Proportion')
% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Data_Table.Leading = ones(size(Data_Table, 1), 1);
% Data_Table.Leading(Data_Table.Condition < 4) = 2;
% Data_Table.Leading(Data_Table.Condition == 7) = 3;
% Plot_All(Data_Table,  'Leading', 'Bin', {'Audio Leading', 'Tactile Leading', 'Unimodal'})
% title('rank correct by modality')
% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Plot_All(Data_Table,  'Leading', 'Proportion', {'Audio Leading', 'Tactile Leading', 'Unimodal'})
% title('% correct by modality')


% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Data_Table.Proportion = Data_Table.Correct./Data_Table.Total;
% hist(Data_Table.Proportion)
% xlabel('Proportion Correct')

% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% Data_Table_1 = Data_Table(ismember(Data_Table.Session, [1, 6:9]), :);
% [Groups, Values] = Group_Values(Data_Table_1, 'Condition', 'Proportion');
% Split_Values = Values(:);
% Helpful_Average = sum(Data_Table_1.Correct)./sum(Data_Table_1.Total);
% 
% Data_Table_2 = Data_Table(ismember(Data_Table.Session, [2:5, 10]), :);
% Not_Helpful_Average = sum(Data_Table_2.Correct)./sum(Data_Table_2.Total);
% [Groups, Values] = Group_Values(Data_Table_2, 'Condition', 'Proportion');
% 
% Split_Values = [Split_Values, Values(:)];
% % Groups = categorical(Condition_Labels(1:end-1));
% B = bar(Split_Values);
% B(1).FaceColor = [0.216 0.451 0.608];
% B(2).FaceColor = [0.776 0.259  0.259];
% B(2).EdgeColor = [1 1 1];
% B(1).EdgeColor = [1 1 1];
% set(gca, 'XTickLabel', Condition_Labels(1:end-1))
% ylim([-0.1, 0.1])
% legend('Helpful', 'Not Helpful')


% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% [Groups, Values] = Group_Values(Data_Table, 'Condition', 'Proportion');
% bar(Values)
% set(gca, 'XTickLabel', Condition_Labels(1:end-1))

% 
% Figure_Indx = Figure_Indx + 1;
% figure(Figure_Indx)
% [Groups, Values] = Group_Values(Data_Table, 'Run', 'Proportion');
% bar(Values(1:10), 'FaceColor', [0.216 0.451 0.608], 'EdgeColor', [0.216 0.451 0.608])
% ylim([0, 1])


Condition_Max = zeros(7, 1);
Ranks = zeros(max(Sessions_Indexes), 7);



for Indx_S = Sessions_Indexes
    Table = Data_Table(Data_Table.Session == Indx_S, :);
    [Groups, Values] = Group_Values(Table, 'Condition', 'Proportion');
    data_sorted = sort(Values);
    [~, rnk] = ismember(Values, data_sorted);
    Condition_Max = Condition_Max + rnk;
    Ranks(Indx_S, :) = rnk;
end

 
Max_Rank = max(max(Ranks));
Firsts = Ranks == Max_Rank;
Firsts = sum(Firsts, 1);

Figure_Indx = Figure_Indx + 1;
figure(Figure_Indx)
bar(Firsts)
set(gca, 'XTickLabel', Condition_Labels)
ylabel('subjects max condition')

Figure_Indx = Figure_Indx + 1;
figure(Figure_Indx)
bar(mean(Ranks, 1))
set(gca, 'XTickLabel', Condition_Labels)
ylabel('mean of ranks')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Plot_All(Data_Table, Gp, DV, Labels)

%     Helpfulness= [3, 1, 1, 2, 2, 3, 3, 3, 4, 2];
%     Helpfulness= [0.25, 0.75, 0.75, 0.5, 0.5, 0.25, 0.25, 0.25, 0, 0.5];
        hold on
        [Groups, Values] = Group_Values(Data_Table, Gp, DV);
        plot(Groups, Values, 'Color', [0.776 0.259  0.259], 'LineWidth', 2)
        set(gca, 'xtick',1:numel(Labels),'xticklabel', Labels);

        for Indx_S = unique(Data_Table.Session)'
            Table_S = Data_Table(Data_Table.Session == Indx_S, :);
            [Groups, Values] = Group_Values(Table_S, Gp, DV);
%             Colors = [Helpfulness(Indx_S), Helpfulness(Indx_S), Helpfulness(Indx_S)];
            Colors = [0.7, 0.7, 0.7];
            plot(Groups, Values, 'Color', Colors, 'LineWidth', 1)
        end

        hold off

end

function [Groups, Values] = Group_Values(Data_Table, Gp, DV)
[G, Groups] = findgroups(Data_Table.(Gp));
switch DV
    case 'Proportion'
        Corrects = splitapply(@sum, Data_Table.Correct, G);
        Totals = splitapply(@sum, Data_Table.Total, G);
        Values = Corrects ./Totals;
    case 'Bin'
        Values = splitapply(@mean, Data_Table.Bin, G);
end
% 
% Unimodal = Values(end);
% Values = Values(1:end-1) - Unimodal;
% Groups = Groups(1:end-1);
end
