import os
import re
import pandas as pd

Session = '10'
Runs = range(1, 11) # needs to be 1 more than the number of runs
Main_Path = os.path.join('C:\\', 'Users', 'Sophia', 'Projects', 'Internship', 'Experiment', 'S' + Session)

CSV = pd.read_csv(os.path.join(Main_Path, 'S' + Session + '_PRT.csv'))

# for root, dirs, files in os.walk(os.path.join(Main_Path, 'S' + Session + '_Recordings',  )):
if set(CSV.Run) != set(Runs):
    print('Mismatch of Runs')

def Check_Runs():
    Problem = False
    for Run in Runs:
        CSV_Run = CSV[CSV.Run == Run][CSV.Condition < 8]
        Recording_Filenames = os.listdir(
            os.path.join(Main_Path, 'S' + Session + '_Recordings', 'S' + Session + '_R' + str(Run).zfill(2)))
        Recording_String = ' '.join(Recording_Filenames)
        Recording_Numbers = re.findall(r'Trial_(\d{3})', Recording_String)
        Recording_Set = set(map(int, Recording_Numbers))
        CSV_Set = set(CSV_Run.Trial_Number)
        Not_in_CSV = Recording_Set - CSV_Set
        Not_in_Recordings = CSV_Set - Recording_Set
        if Not_in_CSV:
            print('Missing trials in CSV of run ' + str(Run) + ': ', Not_in_CSV)
            Problem = True
        if Not_in_Recordings:
            print('Missing audiofiles of run ' + str(Run) + ': ', Not_in_Recordings)
            Problem = True
        if len(Recording_Filenames) != len(CSV_Run):
            print(str(len(Recording_Filenames)) + ' recordings and ' + str(len(CSV_Run)) + ' trials in CSV in run ' + str(Run))
            Problem = True
        if not(Problem):
            print('All good for run ' + str(Run) + '!')
Check_Runs()