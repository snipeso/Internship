%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%TODO: make it so that condition is included in all trigger data!

% configuration for run 1
Padding = '%02.f';
Codes = {'Stop', 'Start', 'Dummy_Start', 'Stop'}; % 1: starts recording, 2: start of trial, 3: start of dummy trial, 4: Stop tactile trial, without recording
Session_Num = 14;
Analysis_Parameters % needs to be after Session_Num
Session_Num = num2str(Session_Num, Padding);
Bad_Trials = []; % possibly delete now


for Indx_R = Session_Info(str2double(Session_Num)).Runs %[1:5, 7:10]
    %
    Run = num2str(Indx_R, Padding); % TODO: if there aren't any problems during experiment, turn this into loop
    %
    Name = ['S', Session_Num, '_R' Run]; %
    Set_Name = ['S', Session_Num, '_R', Run, '_Raw.set'];
    
    
    
    %%%%%%%%%%%%%%
    %%% Load data
    %%%%%%%%%%%%%%
    EEG = pop_loadbv(EEG_Folder, [Name, '.vhdr']);
    EEG = eeg_checkset(EEG);
    
%     EEG_Copy = EEG;
%      pop_saveset(EEG,'filename', 'New.set',...
%         'filepath', EEG_Folder,'check','on',...
%         'savemode','onefile','version','6');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Clean dataset
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%
    %%% Rename Triggers
    %%%%%%%%%%%%%%
    
    %%% set variables
    Trigger_Max = size(EEG.event, 2); % total number of triggers
    Prev_Latency = 0; % this holds the time of the last trigger, so starts with 0
    Dummy_Trial = false; % keeps track of whether current trial was dummy or not
    
    % create a placeholder for 5 consecutive triggers
    Quintuplet = zeros(1, 5); % location for the trigger values
    Quintuplet_Indx = zeros(1, 5); % original position of the trigger in EEG.events
    Quint_Indx = 1; % current trigger's position in the quintuplet. starts with 1
    
    % create new struct to hold the new triggers
    EEG.event(1).Trial = []; % add to event struct the field "Trial"
    New_Events = EEG.event(1); % set the new events to have the same fields as the old event struct
    New_Urevents = EEG.urevent(1); % TODO: find out what this is for
    
    
    %%% loop through triggers and load the desired ones to the new struct
    for Indx_Trg = 2:Trigger_Max
        Trigger = EEG.event(Indx_Trg); % current trigger info
        
        % skip any triggers that were not indicating stimuli
        if not(strcmp(Trigger.code, 'Stimulus'))
            disp([num2str(Indx_Trg), Trigger.code]) % in case there's anything interesting
            continue
        end
        
        % identify trigger position in quintuplet
        Time_Since_Last_Trigger = Trigger.latency - Prev_Latency;
        Prev_Latency = Trigger.latency; % to keep track at next iteration
        
        if Time_Since_Last_Trigger > 150 % if trigger is not 10ms apart, then it is the first of the quintuplet (actually, it should be 20!)
            Quint_Indx = 1;
        elseif Time_Since_Last_Trigger > 8 && Time_Since_Last_Trigger < 12 % when trigger is 10ms after the previous, it's any of the following
            Quint_Indx = Quint_Indx + 1; % it's one more than the previous
        else % in case the timing is somehow off
            warndlg(['Some weird trigger happened at ' Trigger.type, ' Trigger ' num2str(Indx_Trg), ' in run ', num2str(Indx_R)])
        end
        
        % load trigger to quintuplet variable
        Quintuplet_Indx(Quint_Indx) = Indx_Trg; % where in events
        Type = strsplit(Trigger.type); % get value from label
        Quintuplet(Quint_Indx) = str2double(Type{end}); % load value to quintuplet
        
        % save quintuplet as 3 triggers
        if Quint_Indx == 5 % initiate when reached last trigger
            Type = Codes{Quintuplet(1)}; % get type from first trigger
            Trial = num2str(Quintuplet(2:4) - 2); % get middle three trigger numbers
            Trial(Trial == ' ') = []; % join numbers into 1 three digit number
            Condition = num2str(Quintuplet(5) - 2); % get condition from last trigger
            
            %TEMP: remove tactile only trial 99
%                     if strcmp(Trial, '394') && Condition == '7'
%                         Trial = '600';
%                         disp(Trial)
%                     end
            
            % TEMP for when tactile doesnt work
            %         Condition = 7;
            
            
            % rename dummy stop triggers
            if Dummy_Trial && strcmp(Type, 'Stop')
                Type = 'Dummy_Stop';
            end
            
            % Save triggers to new trigger struct with new names
            Code = {Type, ['t', Trial], 0, 0, Condition}; % names to associate to triggers in quintuplet
            
            
            for Indx = [1, 2, 5]
                EEG.event(Quintuplet_Indx(Indx)).type = Code{Indx}; % new trigger name
                New_Events(end + 1) = EEG.event(Quintuplet_Indx(Indx)); % add new trigger to end
                New_Urevents(end + 1) = EEG.urevent(Quintuplet_Indx(Indx));
                New_Events(end).Trial = str2double(Trial); % save trial number to all triggers
            end
        else % for all other triggers, skip the remaining code
            continue
        end
        
        % set Dummy trial to true if this trial is the start of a dummy trial, so the end of the dummy trial can be renamed later
        if Quintuplet(1) == 3 % if start of dummy trial
            Dummy_Trial = true;
        else % reset to normal trial for all other cases
            Dummy_Trial = false;
        end
        
        % reset quintuplet values to default for next round
        Quint_Indx = 1;
        Quintuplet_Indx = zeros(1, 5);
        Quintuplet = zeros(1, 5);
    end
    
    
    %%%%%%%%%%%%%%
    %%% Remove unwanted triggers
    %%%%%%%%%%%%%%
    
    %%% set parameters
    New_Max_Triggers = size(New_Events, 2); % quantity of remaining triggers
    New_Events_II = New_Events(1); % new struct to hold reduced number of triggers
    New_Urevents_II = New_Urevents(1);
    
    %%% loop through all triggers and if trial is indicated as unwanted, delete
    for Indx_Trg = 2:New_Max_Triggers
        
        % add trigger to new struct
        New_Events_II(end + 1) = New_Events(Indx_Trg);
        New_Urevents_II(end + 1) = New_Urevents(Indx_Trg);
        
        % remove last added trigger if it's in the list of unwanted triggers
        if any(ismember(New_Events(Indx_Trg).Trial, Bad_Trials))
            disp(New_Events(Indx_Trg).Trial)
            New_Events_II(end) = [];
            New_Urevents_II(end) = [];
        end
    end
    
    %%% save reduced triggers to original EEG struct
    EEG.event = New_Events_II;
    EEG.urevent = New_Urevents_II;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Save as dataset
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    pop_saveset(EEG,'filename', Set_Name,...
        'filepath', EEG_Folder,'check','on',...
        'savemode','onefile','version','6');
    
end
%TODO: on top of Trial, add Condition


%%% Extra code for merging datasets
% All_EEG(Indx_R) = EEG;


% merge
% Runs = [1, 2];
% Set_Name =  ['S03_R01_Raw.set'];
% EEG = pop_mergeset(All_EEG, Runs, '0');
% 
% 
% pop_saveset(EEG,'filename', Set_Name,...
%         'filepath', EEG_Folder,'check','on',...
%         'savemode','onefile','version','6');
