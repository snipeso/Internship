% Script for pre-processing single-subject raw EEG data
% last update Jan 26 2011

clear all;
eeglab;
close all;
clc;


% PREPROCESSING DATA:
%        CNT data import (calls fillArraysWithZeros.m)
%        high- and low-pass filter
%        downsample
%        create epochs (two different epoch durations)
%        append runs
%        import channel locations 
%        re-reference to an average reference
%
% CLEANING DATA1:
%        test ICA (of short-epoch data)
%        search manually for indices of artifactual trials and channels (in short-epoch data) 
%
% CLEANING DATA2:
%        reject trials and channels (from original data (both epochs))
%        ICA (of pruned short-epoch data)
%        extract IC matrices
%        apply IC matrices to long-epoch data
%        search manually for indices of artifactual ICs
%
% CLEANING DATA3:
%        reject ICs
%        optionally: remove baseline 
%        optionally: interpolate missing channels
%
% STIMULUS-BASED EPOCHS:
%        create epochs for individual stimulus conditions
%
% PERCEPT-BASED EPOCHS:
%        create epochs for individual percept conditions by splitting ambiguous stimulus conditions according to discontinuity/continuity ratings
%        balance percept-conditions for number of trials
%
% PREPARE GROUP ANALYSIS
%        relabel events
%        shorten filenames

       

%%% SELECT PROCESSES (1=yes 0=no) AND CHANNELS TO BE PLOTTED 
preprocessing       = 0;  
cleaning1           = 0;  
cleaning2           = 0;
cleaning3           = 0;
stimulusEpochs      = 1; 
perceptEpochs       = 1;  
prepareGroup        = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                            DATA IMPORT                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% indices of artifactual trials (1st inspection)
rejTr1_1   = [45:47, 68:69, 81, 174, 195:200, 232, 393, 481:493, 501, 596, 570:571, 681, 721, 814, 945, 947:948, 984, 1007, 1087, 1145, 1154];
rejTr1_2   = [59, 150, 224, 236, 287:288, 392, 429:430, 472, 476:480, 488:489, 551:553, 517, 581:582, 721, 731, 780, 830, 890, 901:903, ...
              905, 917:918, 1031, 1038, 1059, 1081:1082, 1119, 1125, 1128:1133];
rejTr1_3   = [100, 151, 174, 344, 364, 435, 494, 562, 607, 617, 650, 741, 928, 978, 1021, 1040, 1095:1096, 1126, 1161, 1169, 1178, 1199];
rejTr1_4   = [89, 94, 101, 104, 107:110, 121:123, 140, 367, 388, 510, 530, 556, 568, 586, 623, 696, 735, 758, 778, 813, 820, 844, 868, ...
              932, 961:963, 973, 976, 1001, 1010, 1070:1071, 1081, 1087, 1092, 1123, 1135, 1170, 1174, 1178:1179, 1182:1194, 1198:1199];
rejTr1_5   = [40, 49, 92, 117, 140, 159, 175, 199, 209, 221, 225, 230, 251, 257, 292, 306, 346, 366, 377, 379, 388, 442, 448, 462, 469, ...
              473, 516, 530:531, 569, 595, 617, 634, 655:656, 692, 704, 707, 738, 742, 747, 764, 808, 814, 823, 835, 839, 862, 881:882, ...
              887:889, 892:893, 956, 978, 1007, 1010, 1015, 1041:1042, 1079, 1111, 1128, 1169, 1192, 1199];
rejTr1_6   = [5, 94, 104, 108, 303, 316, 336, 338, 357, 360, 365:366, 394, 411, 522, 530, 536, 555:556, 558, 611, 616, 618, 641, 660, ...
              681, 686, 722, 739, 744, 750, 756, 767, 770, 775, 785, 803, 805:806, 816, 859, 889, 940, 946:947, 950, 961, 977, 981, 989, ...
              1123, 1129, 1132, 1140, 1151, 1153, 1170, 1176:1177, 1184];
rejTr1_7   = [2, 46, 51, 95, 178, 241, 243, 253, 309, 311, 330, 380, 447, 460, 479, 501, 508, 519, 523, 527, 561, 563, 601, 608, 611:612, ...
              654:655, 671:672, 679, 699:700, 702, 718, 721, 737, 767, 800, 803:804, 808, 812, 820:822, 828, 836, 937:938, 961, 963, ...
              987, 1000, 1026, 1037, 1043:1046, 1081, 1183];
rejTr1_8   = [76, 341, 363:364, 527, 732, 766, 797, 837, 949, 1001:1002, 1032, 1047, 1056, 1074:1075, 1079:1081, 1102, 1135, 1146, 1152, 1157, 1159];
rejTr1_9   = [121, 293, 361:362, 417, 441:442, 555, 629:630, 734:735, 745, 752, 824:825, 830, 841, 851, 857, 887, 908, 1021, 1028, 1064, ...
              1081, 1113, 1117, 1152, 1171:1172, 1179, 1181];
rejTr1_10  = [37:38, 78:79, 98, 126, 157, 176, 241:242, 282, 286, 291, 361, 398, 400, 444:448, 455, 473, 481, 490, 552, 575, 580, 588, ...
              601:602, 607, 635, 650, 681, 721, 820, 869:871, 882, 890, 904, 913, 969, 1011, 1050:1051, 1060:1062, ....
              1079, 1100, 1128, 1137:1141, 1173:1175];
rejTr1_11  = [123, 169, 171, 189, 198, 221, 235, 237, 275, 295, 302, 309, 338, 342, 363, 390, 407, 411, 435, 477, 507:508, ...
             522, 524, 547, 556, 647, 652, 745, 760, 773, 782, 792, 852, 879, 903, 911, 915:916, ...
             921, 1008, 1013:1014, 1028, 1087, 1106, 1115, 1131, 1136, 1151, 1168, 1193];
rejTr1_12  = [61, 119, 140, 152, 202, 330, 333, 348, 388, 406:407, 430:431, 458:468, 479:480, 629, 719:720, 824, 870, 925:926, ...
              958, 996:997, 1039, 1166, 1186, 1189];
rejTr1_13  = [35, 82, 96, 110, 127, 134, 173, 176, 196, 200, 203, 213, 226:227, 246, 280, 294, 338, 380, 382, 427, 430, 482, 486, ...
              501, 565, 602, 617, 627, 629, 632, 723, 772:773, 831, 838, 842, 850, 944, 966, 973, 985, 987, 990, 994, 1025, 1044, ... 
              1051:1053, 1069, 1082, 1092:1093, 1109, 1117, 1124, 1127, 1143, 1155, 1188, 1198];
rejTr1_14  = [22, 60, 90, 115, 162:164, 219, 232, 281, 296, 374, 480, 520, 533, 595, 620, 644, 702, 723, 777, 807, 867, 902, 927, 932, ...
              1098, 1017, 1065, 1098:1099, 1101, 1119, 1141, 1145, 1150:1151, 1189, 1191];
rejTr1_15  = [51, 59, 83, 148, 235, 252, 282, 295, 303, 312, 315, 422, 481, 493, 535, 545, 606, 644, 675, 726, 752, 755, 758, 794, 798, ...
              858, 861, 900, 909, 941, 944, 995, 1009, 1036, 1046, 1051, 1075, 1080, 1118:1119, 1130:1131, 1188];

%%% indices of artifactual channels (1st inspection)
rejCh1_1   = [];
rejCh1_2   = [];
rejCh1_3   = [];
rejCh1_4   = [];
rejCh1_5   = [];
rejCh1_6   = [];
rejCh1_7   = [];
rejCh1_8   = [];
rejCh1_9   = [];
rejCh1_10  = [];
rejCh1_11  = [];
rejCh1_12  = [];
rejCh1_13  = [];
rejCh1_14  = [];
rejCh1_15  = [];

%%% indices of artifactual ICs
rejICs1_1   = [4 18 21 25 27 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 52 53 54 56 57 58 59 60 61 62 63 64 65];
rejICs1_2   = [1 2 3 4 14 16 18 20 21 23 24 25 26 27 28 31 33 34 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 59 60 61 62 63 64 65];
rejICs1_3   = [8 18 20 24 26 27 28 30 32 33 34 35 36 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_4   = [1 2 3 4 6 12 16 19 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_5   = [1 2 3 4 14 26 27 28 30 32 34 36 38 39 40 41 42 43 44 45 46 47 48 49 51 52 53 54 56 57 58 59 60 61 62 63 64 65 ];
rejICs1_6   = [1 2 3 4 18 19 24 26 28 29 30 31 32 33 34 36 37 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_7   = [1 2 12 15 16 18 20 21 23 26 29 30 31 32 33 35 36 37 39 40 41 44 45 46 47 48 49 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_8   = [1 2 7 8 9 10 11 15 24 25 27 28 29 30 32 33 34 37 39 41 42 43 45 46 48 49 50 51 52 53 54 55 56 57 58 60 61 62 63 64 65];
rejICs1_9   = [1 2 3 4 15 20 21 22 24 25 27 28 29 30 33 35 37 39 40 42 43 44 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_10  = [1 7 8 10 14 15 16 17 18 20 21 23 26 27 29 30 33 34 36 37 38 39 40 41 42 44 45 46 48 49 51 52 53 54 55 56 57 58 59 61 62 63 64 65];
rejICs1_11  = [1 2 3 4 7 9 10 13 14 17 18 19 22 25 26 31 35 38 42 43 44 45 46 48 50 51 52 53 54 55 57 58 59 60 61 62 63 64 65];
rejICs1_12  = [1 2 3 5 7 8 9 10 11 12 13 15 16 19 21 27 33 36 38 41 42 43 44 45 46 47 48 49 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_13  = [1 2 3 4 8 12 13 14 16 23 26 28 29 30 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_14  = [1 2 4 5 14 17 19 25 27 28 30 31 35 36 37 38 39 40 42 43 44 45 46 47 48 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65];
rejICs1_15  = [1 2 3 7 18 24 25 26 28 29 32 33 36 38 39 40 41 45 48 49 50 51 52 53 54 56 57 58 59 60 61 62 63 64 65];
    
fillArraysWithDigits_voiceIllusion; % this function fills up all arrays with redundant digits
rejTr1      = [rejTr1_1;rejTr1_2;rejTr1_3;rejTr1_4;rejTr1_5;rejTr1_6;rejTr1_7;rejTr1_8;rejTr1_9;rejTr1_10;rejTr1_11;rejTr1_12;rejTr1_13;rejTr1_14;rejTr1_15];  
rejCh1      = [rejCh1_1;rejCh1_2;rejCh1_3;rejCh1_4;rejCh1_5;rejCh1_6;rejCh1_7;rejCh1_8;rejCh1_9;rejCh1_10;rejCh1_11;rejCh1_12;rejCh1_13;rejCh1_14;rejCh1_15];  
rejICs1     = [rejICs1_1;rejICs1_2;rejICs1_3;rejICs1_4;rejICs1_5;rejICs1_6;rejICs1_7;rejICs1_8;rejICs1_9;rejICs1_10;rejICs1_11;rejICs1_12;rejICs1_13;rejICs1_14;rejICs1_15];  

%%% names and labels for conditions/epochs
% 61	disc000         discontinuous, no-notch         P0oc
% 62	disc050         discontinuous, nb-notch         P0.1oc
% 63	disc125         discontinuous, wb-notch         P1.2oc
% 64	cont125         continuous, wb-notch            Pc
% 44	aC              continuous adaptor              C
% 55	aD              discontinuous adaptor           D
% 77	aCdisc125       after continuous adaptor        CD
% 88	aDdisc125       after discontinuous adaptor     DD
condType{1}  = {'disc000', 'disc050', 'disc125', 'cont125'}; % for short and long epochs
condType{2}  = {'aC', 'aD'};                                 % for very long epochs
condLabel{1} = {'61', '62', '63', '64'};                     % for short and long epochs
condLabel{2} = {'44', '55'};                                 % for very long epochs

%%% preprocessing parameters
sampFreq     = 125;  % EEG sampling frequency in Hz
LP           = 50;   % lowpass filter cutoff in Hz
HP           = 0.5;  % highpass filter cutoff in Hz
epochType    = {'0000-3000', '-1000-5250', '-1000-9200', '-1000-3500'}; 
epochDur     = [[-0.05 3]; [-1 5.25]; [-1 9.2]; [-1 3.5]];
baselineDur  = [[0 1400]; [-1000 0]; [-1000 0]; [-1000 0]];       % baseline intervals
percType     = {'DISC', 'CONT'};

%%% raw EEG data
%mainfolder  = ['D:\EEG\EEG voice illusion study\EEG data\EEG analysis\'];
mainfolder  = ['D:\MATLAB\L.Riecke\EEG\Voice illusion\'];
folder      = {[mainfolder,'S01\'],[mainfolder,'S02\'],[mainfolder,'S03\'],[mainfolder,'S04\'],[mainfolder,'S05\'],[mainfolder,'S06\'],[mainfolder,'S07\'],[mainfolder,'S08\'],[mainfolder,'S09\'],[mainfolder,'S10\'],[mainfolder,'S11\'],[mainfolder,'S12\'],[mainfolder,'S13\'],[mainfolder,'S14\'],[mainfolder,'S15\'],[mainfolder,'S16\'],[mainfolder,'S17\']};
filelist_1   = {'S01_sce01.cnt', 'S01_sce02.cnt', 'S01_sce03.cnt', 'S01_sce04.cnt', 'S01_sce05.cnt', 'S01_sce06.cnt', 'S01_sce07.cnt', 'S01_sce08.cnt', 'S01_sce09.cnt', 'S01_sce10.cnt'};
filelist_2   = {'S02_sce01.cnt', 'S02_sce02.cnt', 'S02_sce03.cnt', 'S02_sce04.cnt', 'S02_sce05.cnt', 'S02_sce06.cnt', 'S02_sce07.cnt', 'S02_sce08.cnt', 'S02_sce09.cnt', 'S02_sce10.cnt'};
filelist_3   = {'S03_sce01.cnt', 'S03_sce02.cnt', 'S03_sce03.cnt', 'S03_sce04.cnt', 'S03_sce05.cnt', 'S03_sce06.cnt', 'S03_sce07.cnt', 'S03_sce08.cnt', 'S03_sce09.cnt', 'S03_sce10.cnt'};
filelist_4   = {'S04_sce01.cnt', 'S04_sce02.cnt', 'S04_sce03.cnt', 'S04_sce04.cnt', 'S04_sce05.cnt', 'S04_sce06.cnt', 'S04_sce07.cnt', 'S04_sce08.cnt', 'S04_sce09.cnt', 'S04_sce10.cnt'};
filelist_5   = {'S05_sce01.cnt', 'S05_sce02.cnt', 'S05_sce03.cnt', 'S05_sce04.cnt', 'S05_sce05.cnt', 'S05_sce06.cnt', 'S05_sce07.cnt', 'S05_sce08.cnt', 'S05_sce09.cnt', 'S05_sce10.cnt'};
filelist_6   = {'S06_sce01.cnt', 'S06_sce02.cnt', 'S06_sce03.cnt', 'S06_sce04.cnt', 'S06_sce05.cnt', 'S06_sce06.cnt', 'S06_sce07.cnt', 'S06_sce08.cnt', 'S06_sce09.cnt', 'S06_sce10.cnt'};
filelist_7   = {'S07_sce01.cnt', 'S07_sce02.cnt', 'S07_sce03.cnt', 'S07_sce04.cnt', 'S07_sce05.cnt', 'S07_sce06.cnt', 'S07_sce07.cnt', 'S07_sce08.cnt', 'S07_sce09.cnt', 'S07_sce10.cnt'};
filelist_8   = {'S08_sce01.cnt', 'S08_sce02.cnt', 'S08_sce03.cnt', 'S08_sce04.cnt', 'S08_sce05.cnt', 'S08_sce06.cnt', 'S08_sce07.cnt', 'S08_sce08.cnt', 'S08_sce09.cnt', 'S08_sce10.cnt'};
filelist_9   = {'S09_sce01.cnt', 'S09_sce02.cnt', 'S09_sce03.cnt', 'S09_sce04.cnt', 'S09_sce05.cnt', 'S09_sce06.cnt', 'S09_sce07.cnt', 'S09_sce08.cnt', 'S09_sce09.cnt', 'S09_sce10.cnt'};
filelist_10  = {'S10_sce01.cnt', 'S10_sce02.cnt', 'S10_sce03.cnt', 'S10_sce04.cnt', 'S10_sce05.cnt', 'S10_sce06.cnt', 'S10_sce07.cnt', 'S10_sce08.cnt', 'S10_sce09.cnt', 'S10_sce10.cnt'};
filelist_11  = {'S11_sce01.cnt', 'S11_sce02.cnt', 'S11_sce03.cnt', 'S11_sce04.cnt', 'S11_sce05.cnt', 'S11_sce06.cnt', 'S11_sce07.cnt', 'S11_sce08.cnt', 'S11_sce09.cnt', 'S11_sce10.cnt'};
filelist_12  = {'S12_sce01.cnt', 'S12_sce02.cnt', 'S12_sce03.cnt', 'S12_sce04.cnt', 'S12_sce05.cnt', 'S12_sce06.cnt', 'S12_sce07.cnt', 'S12_sce08.cnt', 'S12_sce09.cnt', 'S12_sce10.cnt'};
filelist_13  = {'S13_sce01.cnt', 'S13_sce02.cnt', 'S13_sce03.cnt', 'S13_sce04.cnt', 'S13_sce05.cnt', 'S13_sce06.cnt', 'S13_sce07.cnt', 'S13_sce08.cnt', 'S13_sce09.cnt', 'S13_sce10.cnt'};
filelist_14  = {'S14_sce01.cnt', 'S14_sce02.cnt', 'S14_sce03.cnt', 'S14_sce04.cnt', 'S14_sce05.cnt', 'S14_sce06.cnt', 'S14_sce07.cnt', 'S14_sce08.cnt', 'S14_sce09.cnt', 'S14_sce10.cnt'};
filelist_15  = {'S15_sce01.cnt', 'S15_sce02.cnt', 'S15_sce03.cnt', 'S15_sce04.cnt', 'S15_sce05.cnt', 'S15_sce06.cnt', 'S15_sce07.cnt', 'S15_sce08.cnt', 'S15_sce09.cnt', 'S15_sce10.cnt'};
filelist    = {filelist_1;filelist_2;filelist_3;filelist_4;filelist_5;filelist_6;filelist_7;filelist_8;filelist_9;filelist_10;filelist_11;filelist_12;filelist_13;filelist_14;filelist_15};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                        DATA PRE-PROCESSING                         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if preprocessing == 1
    
    for s=1:length(filelist)    % loop for subjects

         for r=1:length(filelist{s})   % loop for runs

            %%% import CNT data
            EEG         = pop_loadcnt([folder{s}, filelist{s}{r}] , 'dataformat', 'int16');
            EEG         = eeg_checkset( EEG );

            %%% highpass filter (HP) and lowpass filter (LP) using sharp FFT
            EEG         = pop_eegfilt( EEG, HP, 0, [], 0, 1);
            EEG         = eeg_checkset( EEG );
            EEG         = pop_eegfilt( EEG, 0, LP, [], 0, 1);
            EEG         = eeg_checkset( EEG );

            %%% downsample (sampFreq), save as xy_scex_HP-LP_sampFreq.set
            EEG         = pop_resample( EEG, sampFreq);
            saveName    = [num2str(s),'_sce',num2str(r), '_', num2str(HP), '-', num2str(LP),'_', num2str(sampFreq)];
            EEG.setname = saveName;
            EEG         = eeg_checkset( EEG );
            EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
            EEG         = eeg_checkset( EEG );

            %%% epoch all stimulus events using three different epoch lengths, save as xy_scex_HP-LP_sampFreq_start-end.set 
            loadName    = saveName;
            for ep=1:3 % extract three different epoch durations
                EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                EEG         = eeg_checkset( EEG );
                saveName    = [loadName,'_', epochType{ep}];
                EEG.setname = saveName;
                EEG         = eeg_checkset( EEG );
                if ep == 1 % short epochs: extract probes and adaptors -> intervals do not overlap
                    EEG = pop_epoch( EEG, {'61', '62', '63', '64', '44', '55', '77', '88'}, epochDur(ep,:), 'newname', saveName, 'epochinfo', 'yes');
                elseif ep == 2 % long epochs: extract probes -> intervals do not overlap
                    EEG = pop_epoch( EEG, {'61', '62', '63', '64'}, epochDur(ep,:), 'newname', saveName, 'epochinfo', 'yes');
                else % ep == 3 % very long epochs: extract adaptors and subsequent probe -> intervals do not overlap
                    EEG = pop_epoch( EEG, {'44', '55'}, epochDur(ep,:), 'newname', saveName, 'epochinfo', 'yes');
                end
                EEG         = eeg_checkset( EEG );
                EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
                EEG         = eeg_checkset( EEG );
            end  % end for epoch durations
            
            %%% delete redundant data 
            loadName = [num2str(s),'_sce',num2str(r), '_', num2str(HP), '-', num2str(LP),'_', num2str(sampFreq)];
            delete([folder{s}, loadName, '.set']);

        end    % loop for runs 

        for ep=1:3 % apply to all epoch durations 

            %%% append all runs using default settings
            for r=1:length(filelist{s})    % loop for runs
                loadName      = [num2str(s),'_sce', num2str(r), '_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}];
                EEG           = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                EEG           = eeg_checkset( EEG );
                allEEG(r)     = EEG;
                allIndices(r) = r;
            end  % loop for runs
            EEG         = pop_mergeset( allEEG, allIndices, '0');
            EEG         = eeg_checkset( EEG );
            clear allEEG;
            clear allIndices;

            %%% import channel locations from CED File
            if s < 10
                EEG = pop_chanedit(EEG,  'load',{ [folder{s}, 'S0', num2str(s), '_removedFirst3Lines+Last2Lines.dat'], 'filetype', 'autodetect'});
            else % s >= 10
                EEG = pop_chanedit(EEG,  'load',{ [folder{s}, 'S', num2str(s), '_removedFirst3Lines+Last2Lines.dat'], 'filetype', 'autodetect'});
            end
            EEG = eeg_checkset( EEG );
            
            %%% re-reference data to an average reference, save as xy_sce1-r_HP-LP_sampFreq_start-end_REF.set
            EEG         = pop_reref( EEG, [], 'refstate',65, 'method', 'withref');
            EEG         = eeg_checkset( EEG );
            saveName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF'];
            EEG.setname = saveName;
            EEG         = eeg_checkset( EEG );
            EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
            EEG         = eeg_checkset( EEG );
            
            %%% delete redundant data 
            for r=1:length(filelist{s})    % loop for runs
                loadName = [num2str(s),'_sce', num2str(r), '_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep},'.set'];
                delete([folder{s}, loadName]);
            end  
            
        end    % loop for epoch durations
        
    end    % loop for subjects 

end     % preprocessing=1




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                ICA & BAD TRIAL/CHANNEL IDENTIFICATION              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if cleaning1 == 1

     for s=1:length(filelist)    % loop for subjects

        %%% only for short epoch duration: run ICA, save as xy_sce1-r_HP-LP_sampFreq_start-end_REF_ICA1.set
        loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{1}, '_REF'];
        saveName    = [loadName, '_ICA1'];
        EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
        EEG         = pop_runica(EEG,  'icatype', 'runica', 'dataset',1, 'options',{ 'extended',1, 'stop',1e-007});
        EEG.setname = saveName;
        EEG         = eeg_checkset( EEG );
        EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
        EEG         = eeg_checkset( EEG );
            
     end    % loop for subjects

end % cleaning1=1

%%% visually inspect ICA1 results, identify indices 
%%% of bad trials and channels for each subject, and 
%%% fill these in variables rejTr1_x and rejCh1_x in header (see above)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       TRIAL/CHANNEL REJECTION & ICA & BAD IC IDENTIFICATION        %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if cleaning2 == 1 

     for s=1:length(filelist)    % loop for subjects

        for ep=1:3 % apply to all epoch durations 
         
            %%% determine indices of artifactual trials
            for indTr=1:length(rejTr1(s,:))
                if rejTr1(s,indTr)~=0, rejTr(indTr)=rejTr1(s,indTr); end % remove redundant zeros from end
            end
            rejTr=sort(rejTr);

            if ep==2 || ep==3 % create reduced list of artifactual trials
                rejTrNew  = rejTr;
                indPrevTr = 1;
                loadName  = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{1}, '_REF'];
                EEG       = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                EEG       = eeg_checkset( EEG );
                for indArt=1:length(rejTr) % loop for all artifacts
                    for indTr=indPrevTr:rejTr(indArt) % loop from last artifact to actual artifact: scan for unwanted conditions 
                        if EEG.epoch(end).event <= 1200
                            temp=EEG.epoch(indTr).eventtype;
                        else % EEG.epoch(end).event > 1200
                            temp=cell2mat(EEG.epoch(indTr).eventtype);
                        end
                        if ep==2 % reject adaptors
                            if max(temp==44)>0 || max(temp==55)>0 || max(temp==77)>0 || max(temp==88)>0 % current trial is an adaptor or post-adaptor trial -> reject trial
                                rejTrNew(indArt:end)=rejTrNew(indArt:end)-1; % shift actual+remaining artifact indices
                                if indTr==rejTr(indArt) % current trial is an artifact 
                                    rejTrNew(indArt)=nan; % remove actual artifact index
                                end
                            end
                        else % ep==3 % reject notches
                            if max(temp==61)>0 || max(temp==62)>0 || max(temp==63)>0 || max(temp==64)>0  % current trial is a notch trial -> reject trial
                                rejTrNew(indArt:end)=rejTrNew(indArt:end)-1; % shift actual+remaining artifact indices
                                if indTr==rejTr(indArt) % current trial is an artifact 
                                    rejTrNew(indArt)=nan; % remove actual artifact index
                                end
                            end
                            if max(temp==77)>0 || max(temp==88)>0 % current trial is a post-adaptor trial -> reject trial
                                rejTrNew(indArt:end)=rejTrNew(indArt:end)-1; % shift actual+remaining artifact indices
                                if indTr==rejTr(indArt) &&  max(rejTr==indTr-1)>0  % current trial is an artifact and preceding trial (adaptor) was also already an artifact
                                    rejTrNew(indArt)=nan; % remove actual artifact index
                                end
                            end
                        end
                        clear temp
                    end % loop from last artifact to actual artifact
                    indPrevTr=rejTr(indArt)+1;
                end % loop for all artifacts
                clear EEG rejTr indArt indPrevTr indTr 

                % remove redundant nans 
                rejTrNew=sort(rejTrNew);
                for indArt=1:length(rejTrNew)
                    if isfinite(rejTrNew(indArt))
                        rejTr(indArt)=rejTrNew(indArt);
                    end
                end
                clear rejTrNew
            end % create reduced list of artifactual trials

            %%% remove artifactual trials and channels (after visual inspection)
            loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF'];
            EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
            EEG         = eeg_checkset( EEG );
            EEG         = pop_select( EEG, 'notrial', rejTr);  
            EEG         = eeg_checkset( EEG );
%             EEG         = pop_select( EEG, 'nochannel', rejCh1(s,:) );
%             EEG         = eeg_checkset( EEG );
            clear rejTr 

            %%% run ICA, save as xy_sce1-10_HP-LP_sampFreq_start-end_REF_CL1_ICA.set
            if ep==1  % only short epoch duration
                EEG         = pop_runica(EEG,  'icatype', 'runica', 'dataset',1, 'options',{ 'extended',1, 'stop',1e-007});
                saveName    = [loadName, '_CL1_ICA'];
                EEG.setname = saveName;
                EEG         = eeg_checkset( EEG );
                EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                EEG         = eeg_checkset( EEG );
            else % for longer epoch durations: apply ICA matrices to other epochTypes, save as xy_sce1-10_HP-LP_sampFreq_start-end_REF_CL1_ICA.set
                loadName1    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{1}, '_REF_CL1_ICA'];
                EEG1         = pop_loadset( 'filename', [loadName1, '.set'], 'filepath', folder{s});
                EEG1         = eeg_checkset( EEG1 );
                EEG          = pop_editset(EEG, 'icaweights',  'EEG1.icaweights', 'icasphere',  'EEG1.icasphere');                
                saveName     = [loadName, '_CL1_ICA'];
                EEG.setname  = saveName;
                EEG          = eeg_checkset( EEG );
                EEG          = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                EEG          = eeg_checkset( EEG );
                clear EEG EEG1;
                delete([folder{s}, loadName, '.set']); % delete redundant data 
            end   % for longer epoch durations
            
        end    % loop for epoch durations            
            
     end    % loop for subjects

end % cleaning=2

%%% visually inspect ICA results and identify indices 
%%% of very bad ICs (liberal) for each subject and 
%%%  fill these in variable rejICs1_x in header (see above)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       IC REJECTION & BL REMOVAL & CHANNEL INTERPOLATION            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
if cleaning3 == 1
    
    for s=1:length(filelist)  % loop for subjects    
        
        for ep=2:3  % loop for epoch durations           

            %%% remove artifactual ICs
            loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA'];
            EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
            EEG         = eeg_checkset( EEG );
            EEG         = pop_subcomp( EEG, [rejICs1(s,1:end)], 0);    
            EEG         = eeg_checkset( EEG );
            saveName    = [loadName, '_CL2'];
            EEG.setname = saveName;
            EEG         = eeg_checkset( EEG );
            EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 

%             %%% remove EOG channel, save as xy_sce1-10_HP-LP_sampFreq_start-end_REF_CL1_ICA_CL2.set
%             EEG         = pop_select( EEG, 'nochannel', 1 ); % EOG channel has index 1
%             EEG         = eeg_checkset( EEG );
%             EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 

%             %%% remove baseline 
%             EEG         = pop_rmbase( EEG, baselineDur(ep,:));
%             EEG         = eeg_checkset( EEG );
%             EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
%             EEG         = eeg_checkset( EEG );
% 
%             %%% interpolate missing channels
%             % FIX THIS: export and save the actual channel structure x_channelStructure_CL.mat (variable EEG.chanlocs) for each subject
%             if EEG.nbchan < 65 % interpolate missing channels
%                 %path(path, mainfolder);
%                 load ([num2str(s), '_channelStructure_CL.mat']); % reimport the variable EEG.chanlocs and use it for interpolation
%                 EEG = eeg_interp(EEG, SixtyTwoChannels);
%                 EEG = eeg_checkset( EEG );
%                 EEG = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', sourceFolder{s});
%                 EEG = eeg_checkset( EEG );
%             end

        end     % loop for epoch durations           
    
    end    % loop for subjects    
       
end    % cleaning3=1 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                     STIMULUS-BASED EPOCHING                        %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if stimulusEpochs == 1
    
    for s=1:length(filelist)  % loop for subjects    
        
        for ep=2:3  % loop for epoch durations           

            %%% epoch stimulus conditions, save as xy_sce1-10_HP-LP_sampFreq_start-end_REF_CL1_ICA_CL2_disc000.set
            loadName = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2'];
            if ep==2 % only for long epochs: extract 4 main conditions (61, 62, 63, 64) 
                for c=1:length(condType{1})   % loop for conditions: create long epochs
                    EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                    saveName    = [loadName, '_', condType{1}{c}];
                    EEG.setname = saveName;
                    EEG         = pop_epoch( EEG, {condLabel{1}{c}}, epochDur(ep,:), 'newname', saveName, 'epochinfo', 'yes');
                    EEG         = eeg_checkset( EEG );
                    EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                end    % loop for conditions
%                 for c=1:length(condType{1})   % loop for conditions: create shorter epochs
%                     EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
%                     EEG         = eeg_checkset( EEG );
%                     saveName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{4}, '_REF_CL1_ICA_CL2_', condType{1}{c}];
%                     EEG.setname = saveName;
%                     EEG         = pop_epoch( EEG, {condLabel{1}{c}}, epochDur(4,:), 'newname', saveName, 'epochinfo', 'yes'); % extract shorter epochs
%                     EEG         = eeg_checkset( EEG );
%                     EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
%                     EEG         = eeg_checkset( EEG );
%                 end    % loop for conditions
            else % ep==3 % for longest epochs: extract 2 adaptor conditions + subsequent probe (44+77, 55+88)
                for c=1:length(condType{2})   % loop for conditions
                    EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                    saveName    = [loadName, '_', condType{2}{c}];
                    EEG.setname = saveName;
                    EEG         = pop_epoch( EEG, {condLabel{2}{c}}, epochDur(ep,:), 'newname', saveName, 'epochinfo', 'yes');
                    EEG         = eeg_checkset( EEG );
                    EEG         = pop_saveset( EEG,  'filename', [saveName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                end    % loop for conditions
            end  

        end    % loop for epoch durations

    end    % loop for subjects    
       
end    % stimulusEpochs=1 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                     PERCEPT-BASED EPOCHING                         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if perceptEpochs == 1 

    %%% create percept-based epochs for each condition (disc000_DISC,disc000_CONT,...) 
    for s=1:length(filelist)  % loop for subjects    

        %%% check long-epoch dataset:
        %%% identify trials with more or less than 1 button response
        %%% extract indices of trials with continuity ratings (buttons 1|2) and discontinuity ratings (buttons 3|4) 
        for c=1:2  % loop for conditions, 1=disc000 2=disc050
            tooFew  = 0;
            tooMany = 0;
            contInd = 0;
            discInd = 0;
            loadName = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{2}, '_REF_CL1_ICA_CL2_', condType{1}{c}];
            EEG      = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
            EEG      = eeg_checkset( EEG );
            for ind=1:EEG.trials
               if length(EEG.epoch(ind).eventtype) < 2 % count number of button pushes per trial
                   tooFew=tooFew+1; end
               if length(EEG.epoch(ind).eventtype) > 2 % count number of button pushes per trial
                   tooMany=tooMany+1; end
               if EEG.epoch(ind).eventtype{end} == 1 || EEG.epoch(ind).eventtype{end} == 2 % count number of cont button pushes per trial
                   contInd=contInd+1; 
                   contButtons{c}(contInd)=ind; end
               if EEG.epoch(ind).eventtype{end} == 3 || EEG.epoch(ind).eventtype{end} == 4 % count number of disc button pushes per trial
                   discInd=discInd+1; 
                   discButtons{c}(discInd)=ind; end
            end
            % display trial summary
            if tooFew ~= 0
                disp(['WARNING: Subject ', num2str(s), ' omitted a button response on ', num2str(tooFew),' trials in condition ', (num2str(condType{1}{c})), ' -> check percept-epochs for correctness!']); end
            if tooMany ~= 0
                disp(['WARNING: Subject ', num2str(s), ' repeated a button response on ', num2str(tooMany),' trials in condition ', (num2str(condType{1}{c})), ' -> check percept-epochs for correctness!']); end
            disp(['         Subject ', num2str(s), ' reported FAs on ', num2str(length(contButtons{c})),' out of ', num2str(EEG.trials-tooFew), ' trials in condition ', (num2str(condType{1}{c}))]);             
            clear EEG 
        end % loop for conditions, 1=disc000 2=disc050

        
        %%% extract percept-based epochs
        for ep=2 % only for long epochs
%       for ep=4 % only for shorter epochs
    
            for c=1:2                % loop for conditions, 1=disc000 2=disc050

                %%% load data, remove trials with no or undesired button presses, save as xy_sce1-10_HP-LP_sampFreq_start-end_REF_CL1_ICA_CL2_disc000_CONT.set etc.
                for dc=1:length(percType) % discontinuity/continuity percepts
                    loadName = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2_', condType{1}{c}];
                    EEG      = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG      = eeg_checkset( EEG );
                    if dc==1    % extract discontinuity percepts
                        EEG = pop_select( EEG, 'trial', discButtons{c});  
                    else % DC==2, extract continuity percepts
                        EEG = pop_select( EEG, 'trial', contButtons{c});                              
                    end
                    saveName     = [loadName, '_', percType{dc}];
                    EEG.setname  = saveName;
                    EEG          = eeg_checkset( EEG );
                    EEG          = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                    EEG          = eeg_checkset( EEG );
                end   % loop for dc (discontinuity/continuity percepts)

                %%% balance the number of trials by randomly rejecting trials from the condition that was reported less often
                nbRejectTrials = length(discButtons{c})-length(contButtons{c});
                randInd = zeros;
                if nbRejectTrials~=0   % unbalanced number of disc and cont buttons -> reject trials
                    if nbRejectTrials > 0     % more disc buttons -> reject disc trials
                        dc=1; % discontinuity conditions
                    else % nbRejectTrials < 0 % more cont buttons -> reject cont trials
                        dc=2; % continuity conditions
                    end
                    loadName = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2_',condType{1}{c}, '_', percType{dc}];
                    EEG      = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG      = eeg_checkset( EEG );
                    randInd  = randperm(EEG.trials);  % permute all trials in dataset
                    EEG      = pop_select( EEG, 'notrial', randInd(1:abs(nbRejectTrials)));  % reject first trials of permuted dataset
                    EEG      = eeg_checkset( EEG );
                    EEG      = pop_saveset (EEG, 'filename', [loadName, '.set'], 'filepath', folder{s}); 
                    EEG      = eeg_checkset( EEG );
                end
                randIndices{c}=randInd;
                clear randInd

            end    % loop for conditions
                    
        end     % loop for epoch durations           
    
        rejPercTrials.discButtons = discButtons;
        rejPercTrials.contButtons = contButtons;
        rejPercTrials.randIndices = randIndices;
        save([folder{s},num2str(s),'_rejectedPerceptTrials.mat'],'rejPercTrials');
        clear discButtons contButtons randIndices rejPercTrials
        
    end    % loop for subjects    
    
end  % perceptEpochs = 1



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                     PREPARE GROUP ANALYSIS                         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if prepareGroup == 1
   
    for s=1:length(filelist)  % loop for subjects    
        for ep=2:3 % loop for epoch durations
       %for ep=3:4 % loop for epoch durations
                
           if ep==2 % only for long epochs
           %if ep==4 % only for shorter epochs
                for c=1:length(condType{1})     % loop for conditions
                    %%% load dataset, relabel events, shorten filename, save dataset, delete old dataset
                    loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2_', condType{1}{c}];
                    EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                    EEG         = pop_selectevent( EEG, 'type', str2num(condLabel{1}{c}), 'renametype', condType{1}{c}, 'deleteevents', 'off', 'deleteepochs', 'off', 'invertepochs', 'off');
                    EEG         = eeg_checkset( EEG );
                    saveName    = [num2str(s), '_', epochType{ep}, '_', condType{1}{c}];
                    EEG.setname = saveName;
                    EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                    EEG         = eeg_checkset( EEG );
                    delete([folder{s}, loadName, '.set']);

                    if c==1 || c==2 % only for disc000 and disc050
                        for dc=1:length(percType)   % loop for discontinuity/continuity percepts
                            loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2_',condType{1}{c},'_', percType{dc}];
                            EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                            EEG         = eeg_checkset( EEG );
                            EEG         = pop_selectevent( EEG, 'type', str2num(condLabel{1}{c}), 'renametype', condType{1}{c}, 'deleteevents', 'off', 'deleteepochs', 'off', 'invertepochs', 'off');                            
                            EEG         = eeg_checkset( EEG );
                            saveName    = [num2str(s), '_', epochType{ep}, '_', condType{1}{c},'_', percType{dc}];
                            EEG.setname = saveName;
                            EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                            EEG         = eeg_checkset( EEG );
                            delete([folder{s}, loadName, '.set']);
                        end
                    end
                end     % loop for conditions

            else % ep==3 % only for longest epochs
                for c=1:length(condType{2})     % loop for conditions
                    %%% load dataset, relabel events, shorten filename, save dataset, delete old dataset
                    loadName    = [num2str(s),'_sce1-10_', num2str(HP), '-', num2str(LP), '_', num2str(sampFreq), '_', epochType{ep}, '_REF_CL1_ICA_CL2_', condType{2}{c}];
                    EEG         = pop_loadset( 'filename', [loadName, '.set'], 'filepath', folder{s});
                    EEG         = eeg_checkset( EEG );
                    EEG         = pop_selectevent( EEG, 'type', str2num(condLabel{2}{c}), 'renametype', condType{2}{c}, 'deleteevents', 'off', 'deleteepochs', 'off', 'invertepochs', 'off');
                    EEG         = eeg_checkset( EEG );
                    saveName    = [num2str(s), '_', epochType{ep}, '_', condType{2}{c}];
                    EEG.setname = saveName;
                    EEG         = pop_saveset (EEG, 'filename', [saveName, '.set'], 'filepath', folder{s}); 
                    EEG         = eeg_checkset( EEG );
                    delete([folder{s}, loadName, '.set']);
                end     % loop for conditions
            end
                
        end     % loop for epoch durations
    end    % loop for subjects        
   
end % prepareGroup = 1

