close all
%%% Set parameters
Main_Folder = 'C:\Users\Sophia\Projects\Internship\Experiment\';
Sessions = 2:10;


Conditions = [7, 8]; % which conditions to get filters of
ERP_Limits = [-0.4, 1];
Parameters = struct();
N1_Window = [.075, .250];

% auditory
Parameters(7).Plot_Channels = [16, 17, 47, 21]; % plot ERP of specific channels

% tactile
Parameters(8).Plot_Channels = [52, 17];

% plot parameters
Interval = .02; % seconds spacing of topologies

for Indx_C = Conditions % loop through conditions
    Indx_P_Start = 0;
    for Session_Num = Sessions
        Analysis_Parameters
        Dataset = ['S', num2str(Session_Num, Padding), '_All_From_Start.set'];
        
        % brain data
        EEG = pop_loadset(Dataset, EEG_Folder);
        
        % Session trial information
        Trials = readtable(CSV_Path);
        Trials = Trials(Trials.Type == Type & ismember(Trials.Run, Runs) & Trials.EEG == 1, :);
        
        
        % initialize variables
        fs = EEG.srate;
        epochLimits= [EEG.xmin, EEG.xmax]; % epoch onset and offset times relative to trigger
        Whole_Window = epochLimits(2)-epochLimits(1); % time of epoch
        Start_Points = (N1_Window(1)-epochLimits(1))*fs:fs*Interval:(N1_Window(2)-epochLimits(1))*fs;
        ERP_Window = fs*(ERP_Limits(2)-ERP_Limits(1)); % converts limits to range
        Figure_Indx = 0;
        Indx_Ch = 1;
        chWeights = ones(size(EEG.chanlocs))';
        Peak = zeros(1, length(Conditions));
        
        %%%%%%%%%%%%%%%%%
        %%% Safety checks
        %%%%%%%%%%%%%%%%%
        
        % check that number of trials in CSV and epoch numbers match
        CSV_Trials = size(Trials, 1);
        Epochs = size(EEG.data, 3);
        if CSV_Trials ~= Epochs
            error(['trial numbers do not match. CSV: ', num2str(CSV_Trials), ' EEG data: ', num2str(Epochs)])
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% average data into ERPs
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        % load condition specific variables
        Trial_IDs = Trials.Trial_Number(Trials.Condition == Indx_C); % in order of presentation
        Epoch_Indexes = find(Trials.Condition == Indx_C);
        EEG_C = EEG.data(:, :, Epoch_Indexes); % epochs of the current condition. chanels, by time by epoch
        
        
        
        Start_Epoch = round((ERP_Limits(1) - epochLimits(1))*fs); %TODO: FIX time selection
        if Start_Epoch == 0
            Start_Epoch = 1;
        end
        EEG_C = EEG_C(:, Start_Epoch:(Start_Epoch + ERP_Window -1 ), :);
        
        
        %%% average trials
        ERP_C = mean(EEG_C, 3); % averaging epochs
        
        
        %%% plot topologies at different onsets
        
        
        for Indx_P = 1:length(Start_Points)
            % average all timepoints in interval
            Map_Values = mean(ERP_C(:, Start_Points(Indx_P):(Start_Points(Indx_P)+Interval*fs)), 2);
            figure(Indx_C)
            subplot(length(Sessions), length(Start_Points), Indx_P + Indx_P_Start) % plot on two rows
            topoplot(Map_Values, EEG.chanlocs);
            if Indx_P + Indx_P_Start < 10
                title(num2str(Start_Points(Indx_P)/fs + epochLimits(1) - .01))
            end
        end
        Indx_P_Start = Indx_P_Start + length(Start_Points);
    end
    
end