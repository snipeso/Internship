clear
Sessions = 14;


for Session_Num = Sessions
    Analysis_Parameters
    

    Filtering(Session_Num, Runs)
    Compare_Epochs(Session_Num, Runs)
    Epoch_Data(Session_Num, Runs, Main_Epoch, Epoch_Reference)

    Create_Mega_Matrix(Session_Num, Runs)

    Chop_Starts(Session_Num, Runs)

end