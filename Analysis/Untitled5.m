Counter = 0;
Stops = table();
for Indx_R = 1:7
   Run_Name = ['S', num2str(Session_Num, Padding), '_R', num2str(Indx_R, Padding), '_Filtered.set']; 
   EEG(Indx_R) = pop_loadset(Run_Name, EEG_Folder);
   EEG = EEG(Indx_R);
   Events = size(EEG.event, 2);
   Trials = cell(Events, 2);
   for Indx = 1:Events
       Event = EEG.event(Indx);
       Trials(Indx, 1) = {Event.Trial};
       Trials(Indx, 2) = {Event.type};
       if strcmp(Event.type, 'Stop')
           Counter = Counter + 1;
           Stops = [Stops; table(Indx_R, Event.Trial, Indx)];
       end
           
   end
   
end