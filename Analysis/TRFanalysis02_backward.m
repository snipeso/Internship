clear all;clc;close all;
subjInds=2;
tic

%% set parameters
tmin = -400; % earliest lag used for tracking analysis
tmax = 700;  % latest lag used for tracking analysis
fs = 100; 
lag_vec = tmin:(1000/fs):tmax;


%% import channel locations
dp = 'C:\Users\L.Riecke\Dropbox\FPN\Internship Sophia\05_Software';
load(fullfile(dp,'ChannelLocations.mat')); % load channel location file
stim_ch = [63 64]; % indices of channels carrying the reference signals (stim1: auditory envelope, stim2: tactile envelope)
eeg_ch = setdiff(1:numel(CL),stim_ch);


for indSubj=subjInds % loop for subjects

    if indSubj>9
        subjInd=num2str(indSubj);
    else
        subjInd=['0' num2str(indSubj)];
    end
    disp(['Subject ' subjInd]);
    dpIN = ['\\ca-um-nas201\fpn_rdm$\DM0703_LR_SSTASP\10_DataAnalysis\S' subjInd '\S' subjInd '_EEG\'];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%% EEG tracking analysis (backward modelling for multimodal expt. as Crosse et al JN 2015)
    load(fullfile([dpIN 'S' subjInd '_All_Runs.mat'])); % EEG data including reference signals


    %% duplicate unimodal stimuli so that unimodal trials are also associated with two stimuli
    for c = [7 8] % 7:A condition 8:T condition
        if c==7 % A condition
            missingStim=2; % tactile stimulus missing
            missingCond=8; % tactile stimulus missing
        else % c==8 % T condition
            missingStim=1; % auditory stimulus missing
            missingCond=7; % auditory stimulus missing
        end
        cix = find(Trials(:,3)==c); % trials of the current condition
        ntrials = numel(cix);
        for tr=1:ntrials % loop through all trials of current condition
            trN=intersect(find(Trials(:,1)==Trials(cix(tr),1)),find(Trials(:,3)==missingCond));
            if ~isempty(trN)
                Data(stim_ch(missingStim),:,cix(tr))=Data(stim_ch(missingStim),:,trN); % stimulus from other modality
            end
            clear temp trN
        end
    end % 7:A condition 8:T condition


    %% z-score all trials
    for ix=1:size(Data,1)
        for iz=1:size(Data,3)
            Data(ix,:,iz)=zscore(Data(ix,:,iz),0,2);
        end
    end

    warning off;
    for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
        disp(['Reference signal ' num2str(refSig) '/' num2str(numel(stim_ch))]);

        %% for each unimodal condition (A and T), train a model for a range of lambdas
%         lambda_init = 10.^(-10:.5:10); % range of lambdas to be sampled
        lambda_init{1} = 2.^(14:34); % range of lambdas to be sampled in A condition (Crosse JN 2015)
        lambda_init{2} = 2.^(14:34); % range of lambdas to be sampled in T condition (Crosse JN 2015)
%         lambda_init{1} = 2.^(14:15); % range of lambdas to be sampled in A condition (Crosse JN 2015)
%         lambda_init{2} = 2.^(14:15); % range of lambdas to be sampled in T condition (Crosse JN 2015)
        cN=0;

        % train model for each unimodal condition
        for c = [7 8] % 7:A condition 8:T condition
            cN=cN+1;
            disp(['   Unimodal condition ' num2str(cN) '/2']);
            cix = find(Trials(:,3)==c);
            ntrials = numel(cix);
            model{cN}=zeros(numel(eeg_ch),numel(lag_vec),numel(lambda_init{cN}));
            biasC{cN}=zeros(numel(eeg_ch),numel(lambda_init{cN}));
            indLambda=0;

            % build model for a range of lambdas
            for lambda=lambda_init{cN}
                indLambda=indLambda+1;
                disp(['      Lambda ' num2str(indLambda) '/' num2str(numel(lambda_init{cN}))]);

                % train unimodal model 
                modelTr=zeros(numel(eeg_ch),numel(lag_vec),ntrials);
                biasCtr=zeros(numel(eeg_ch),ntrials);
                trialsWithStimuli=[]; % use only trials that contain a stimulus
                for tr=1:ntrials % loop through all trials of current condition
                    STIM = squeeze(Data(stim_ch(refSig),:,cix(tr))'); % reference signal of the current condition
                    if any(STIM~=0) % trial contains a stimulus
                        trialsWithStimuli=[trialsWithStimuli tr];
                        DATA = squeeze(Data(eeg_ch,:,cix(tr))');      % EEG data of the current condition
                        [modelTr(:,:,tr),~,biasCtr(:,tr)] = mTRFtrain(STIM,DATA,fs,-1,tmin,tmax,lambda);   % training on single trial
                    end
                end
                model{cN}(:,:,indLambda)=mean(modelTr(:,:,trialsWithStimuli),3); clear modelTr; % average model across training trials
                biasC{cN}(:,indLambda)=mean(biasCtr(:,trialsWithStimuli),2); clear trialsWithStimuli; % average bias across training trials
            end
        end


        %% for each combination (lambda_A * lambda_T), add the two unimodal models and validate the resulting summed model on each multimodal condition
        cN=0;
        for c = 1:6 % SOAs 1:-300 6:+200 multimodal conditions
            cN=cN+1;
            disp(['   Validate on multimodal condition ' num2str(cN) '/6']);
            cix = find(Trials(:,3)==c);
            ntrials = numel(cix);
            Rtr=zeros(1,ntrials);

            %% validate summed models and choose the one that results in the highest correlation         
            R=zeros(size(lambda_init{1},2),size(lambda_init{2},2));
            lambdaIndN=0;
            for lambda_A=1:size(lambda_init{1},2)
                for lambda_T=1:size(lambda_init{2},2)
                    lambdaIndN=lambdaIndN+1;
                    disp(['        Lambda combination ' num2str(lambdaIndN) '/' num2str(size(lambda_init{1},2)*size(lambda_init{2},2))]);
                    modelAplusT =  model{1}(:,:,lambda_A) + model{2}(:,:,lambda_T);   % sum
                    biasCAplusT = (biasC{1}(:,lambda_A) +   biasC{2}(:,lambda_T))./2; % mean

                    % validate the summed model on current multimodal condition
                    for tr=1:ntrials % loop through all trials of current multimodal condition
                        STIM = squeeze(Data(stim_ch(refSig),:,cix(tr))'); % reference signal per trial of the current condition
                        DATA = squeeze(Data(eeg_ch,:,cix(tr))');          % EEG data per trial of the current condition
                        [~,Rtr(tr),~,~] = mTRFpredict(STIM,DATA,modelAplusT,fs,-1,tmin,tmax,biasCAplusT); % testing:  STIM and DATA come from only the current (left-out) trial        
                    end
                    R(lambda_A,lambda_T) = mean(atanh(Rtr)); % Fisher-transformed correlation coefficient (averaged across all test trials)
                    clear modelAplusT biasCAplusT Rtr
                end
            end
            R_AplusT(refSig,cN)=max(max(R));
            [lambda_Amax,lambda_Tmax]=find(R==R_AplusT(refSig,cN)); clear R;
            model_AplusT{refSig,cN} =  model{1}(:,:,lambda_Amax) + model{2}(:,:,lambda_Tmax);   % sum


            %% repeat for each unimodal model
            cUniN=0;
            for cUni = [7 8] % 7:A condition 8:T condition
                cUniN=cUniN+1;
                R=zeros(1,size(lambda_init{cUniN},2));
                for lambda_uni=1:size(lambda_init{cUniN},2)
                    modelUni = model{cUniN}(:,:,lambda_uni);
                    biasCuni = biasC{cUniN}(:,lambda_uni);

                    % validate the unimodal model on current multimodal condition
                    for tr=1:ntrials % loop through all trials of current multimodal condition
                        STIM = squeeze(Data(stim_ch(refSig),:,cix(tr))'); % reference signal per trial of the current condition
                        DATA = squeeze(Data(eeg_ch,:,cix(tr))');          % EEG data per trial of the current condition
                        [~,Rtr(tr),~,~] = mTRFpredict(STIM,DATA,modelUni,fs,-1,tmin,tmax,biasCuni); % testing:  STIM and DATA come from only the current (left-out) trial        
                    end
                    R(lambda_uni) = mean(atanh(Rtr)); % Fisher-transformed correlation coefficient (averaged across all test trials)
                    clear modelUni biasCuni Rtr
                end
                if cUni==7 % A condition
                    [R_A(refSig,cN),lambda_unimax]=max(R); clear R;
                    model_A{refSig,cN} = model{cUniN}(:,:,lambda_unimax);   
                else % cUni==8 % T condition
                    [R_T(refSig,cN),lambda_unimax]=max(R); clear R;
                    model_T{refSig,cN} = model{cUniN}(:,:,lambda_unimax);   
                end
            end % 7:A condition 8:T condition


            %% compare to the correlation obtained with a true multimodal model (obtained using cross-validation)
            disp(['        Cross-validation on multisensory condition...']);
            model_AT{refSig,cN}=zeros(numel(eeg_ch),numel(lag_vec),ntrials);
            for tr=1:ntrials % loop through all trials of current condition

                % find best lambda for current channel and set of trials
                STIM = cell(1,ntrials-1);
                DATA = cell(1,ntrials-1);
                tr2N=0;
                for tr2 = setdiff(1:ntrials,tr) % all trials except the current one
                    tr2N=tr2N+1;
                    STIM{tr2N} = squeeze(Data(stim_ch(refSig),:,cix(tr2))'); % reference signal per trial of the current condition
                    DATA{tr2N} = squeeze(Data(eeg_ch,:,cix(tr2))');          % EEG data per trial of the current condition
                end
                [~,~,MSE] = mTRFcrossval(STIM,DATA,fs,-1,tmin,tmax,lambda_init{refSig}); % lambda estimation: STIM and DATA come from all trials except the current one
                [~,lambda_star_ix] = min(mean(MSE,1)); % select the lambda producing the smallest prediction error
                lambda_star = lambda_init{refSig}(lambda_star_ix); % best lambda
                clear STIM DATA

                % train multimodal model 
                modelTr=zeros(numel(eeg_ch),numel(lag_vec),ntrials);
                biasCtr=zeros(numel(eeg_ch),ntrials);
                tr2N=0;
                for tr2 = setdiff(1:ntrials,tr) % all trials except the current one
                    tr2N=tr2N+1;
                    STIM = squeeze(Data(stim_ch(refSig),:,cix(tr2))'); % reference signal per trial of the current condition
                    DATA = squeeze(Data(eeg_ch,:,cix(tr2))');         % EEG data per trial of the current condition
                    [modelTr(:,:,tr2N),~,biasCtr(:,tr2N)] = mTRFtrain(STIM,DATA,fs,-1,tmin,tmax,lambda_star);   % training: STIM and DATA come from all trials except the current one
                end
                model_AT{refSig,cN}(:,:,tr)=mean(modelTr,3); clear modelTr; % average model across training trials
                biasCAT=mean(biasCtr,2); % average bias across training trials

                % validate model
                STIM = squeeze(Data(stim_ch(refSig),:,cix(tr))'); % reference signal per trial of the current condition
                DATA = squeeze(Data(eeg_ch,:,cix(tr))');          % EEG data per trial of the current condition
                [~,R(tr),~,~] = mTRFpredict(STIM,DATA,model_AT{refSig,cN}(:,:,tr),fs,-1,tmin,tmax,biasCAT); % testing:  STIM and DATA come from only the current (left-out) trial
                clear STIM DATA

            end % loop through all trials of current condition

            R_AT(refSig,cN) = mean(atanh(R));    % Fisher-transformed correlation coefficient (averaged across all splits/test trials)
            model_AT{refSig,cN} = mean(model_AT{refSig,cN},3); % TRF model (averaged across all splits/test trials)

        end % SOAs 1:-300 6:+200 multimodal conditions

    end % reference signal 1: auditory envelope, 2: tactile envelope

    condLabels={'-300','-200','-100','0','+100','+200','A+T'}; % negative values = tactile leading


    %% plot correlation 
    figure
    % per condition 
    % per reference signal (light: auditory, dark: tactile)
    % red:    A
    % blue:   T
    % black:  AT
    % green:  A+T
    % auditory reference signal (light)
    plot(1:6,R_AT(1,:),'k','color',[.7 .7 .7],'linewidth',2); hold on 
    plot(1:6,R_AplusT(1,:),'g','color',[.7 1 .7],'linewidth',2);
    plot(1:6,R_A(1,:),'r','color',[1 .7 .7],'linewidth',2); 
    plot(1:6,R_T(1,:),'b','color',[.7 .7 1],'linewidth',2);
    % tactile reference signal (dark)
    plot(1:6,R_AT(2,:),'k','linewidth',2); 
    plot(1:6,R_AplusT(2,:),'g','linewidth',2); 
    plot(1:6,R_A(2,:),'r','linewidth',2); 
    plot(1:6,R_T(2,:),'b','linewidth',2);
    hold off
    set(gca,'xtick',1:6,'xticklabel',condLabels)
    xlabel('condition');ylabel('correlation coefficient');
    title('dark=tacModel, light=audModel');
    xlim([0 8]);
    savefig([dp,'\S' subjInd '_backwardModel_corr.fig']);


    %% plot TRF model per condition (for tactile reference signal)
    for c = 1:6
        figure;
        set(gcf,'name',['condition ' condLabels{c} ' ms']);
        hold on;
        plot(lag_vec,mean(model_AT{2,c},1),'color',repmat((c-1)*(9/50),1,3),'linewidth',3);  % darker gray = tactile leading more
        plot(lag_vec,mean(model_AplusT{2,c},1),'color',[0 1 0],'linewidth',3);               % green = AplusT
        plot(lag_vec,mean(model_A{2,c},1),'color',[1 0 0],'linewidth',2);                    % red = A
        plot(lag_vec,mean(model_T{2,c},1),'color',[0 0 1],'linewidth',2);                    % blue = T
        xlabel('lag (ms)');ylabel('TRF amplitude');
        savefig([dp,'\S' subjInd '_backwardModel_TRF_' condLabels{c} '_tacStim.fig']);
    end

    save([dp,'\S' subjInd '_backwardModel_results.mat'],'R_AT','R_AplusT','R_A','R_T','model_AT','model_AplusT','model_A','model_T');
    disp([' ']);
    
end % loop for subjects

toc

%% further explore the difference AT > (A+T)
% do the following analyses for each AT, (A+T), A, and T:
% -in space (channel weights)
% -in frequency (redo the analysis after BP filtering the stimuli into a range of narrow bands)
