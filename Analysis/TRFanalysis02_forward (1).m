clear all;clc;eeglab;close all;
subjInds=1:2;
alignTacStimFlag=0; % 1: when training on tactile stimuli, first align those stimuli


%% set parameters
tic;
tmin = -400; % earliest lag used for tracking analysis
tmax = 700;  % latest lag used for tracking analysis
fs = 100; 
lag_vec = tmin:(1000/fs):tmax;
SOAs=[-300:100:200 0 0];
maxStimShiftsmp=ceil((max(abs(SOAs))/1000)*fs); % shift stimuli by this number of samples or less (based on max(abs(SOA))=300ms)


%% import channel locations
dp = 'C:\Users\L.Riecke\Dropbox\FPN\Internship Sophia\05_Software';
load(fullfile(dp,'ChannelLocations.mat')); % load channel location file
stim_ch = [63 64]; % indices of channels carrying the reference signals (stim1: auditory envelope, stim2: tactile envelope)
eeg_ch = setdiff(1:numel(CL),stim_ch);


%% EEG analysis
for indSubj=subjInds % loop for subjects

    if indSubj>9
        subjInd=num2str(indSubj);
    else
        subjInd=['0' num2str(indSubj)];
    end
    disp(['*** Subject ' subjInd]);
    dpIN = ['\\ca-um-nas201\fpn_rdm$\DM0703_LR_SSTASP\10_DataAnalysis\S' subjInd '\S' subjInd '_EEG\'];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%% EEG tracking analysis (forward modelling)
    load(fullfile([dpIN 'S' subjInd '_All_Runs.mat'])); % EEG data including reference signals

    %% extract analysis channels (apply spatial filter)
    weightLabels={'audChn','tacChn'};
    loadName=fullfile([dpIN 'S' subjInd '_channelWeights.mat']);
%     loadName=fullfile([dpIN 'S01_channelWeights.mat']);
    if exist(loadName); 
        load(loadName); % channel weights (ERP amplitude at fixed latency)
        if size(chWeights,1)~=numel(eeg_ch);disp('ERROR: incorrect number of channel weights!');end
        chWeightsNorm=zeros(size(chWeights));
        Data_filt=zeros(size(chWeights,2),size(Data,2),size(Data,3));
        for ch = 1:size(chWeights,2)
            figure;hold on; set(gcf,'name',weightLabels{ch});
            chWeightsNorm(:,ch)=chWeights(:,ch)/sqrt(sum((chWeights(:,ch).^2),1)); % Euclidean norm of channel weights
            topoplot(chWeightsNorm(:,ch),CL(eeg_ch));
            savefig([dp,'\S' subjInd '_forwardModel_spatFilt_' weightLabels{ch} '.fig']);
            temp=Data(eeg_ch,:,:).*repmat(chWeightsNorm(:,ch),1,size(Data,2),size(Data,3));
            Data_filt(ch,:,:)=(squeeze(sum(temp,1))); % weighted average of all channels
            clear temp
        end
    else % temporary selection of 2ch
        ana_ch = [38 47]; % ch1: auditory, ch2: tactile
        Data_filt = Data(ana_ch,:,:);
    end


    %% duplicate unimodal stimuli so that unimodal trials are also associated with two stimuli
    for c = [7 8] % 7:A condition 8:T condition
        if c==7 % A condition
            missingStim=2; % tactile stimulus missing
            missingCond=8; % tactile stimulus missing
        else % c==8 % T condition
            missingStim=1; % auditory stimulus missing
            missingCond=7; % auditory stimulus missing
        end
        cix = find(Trials(:,3)==c); % trials of the current condition
        ntrials = numel(cix);
        for tr=1:ntrials % loop through all trials of current condition
            trN=intersect(find(Trials(:,1)==Trials(cix(tr),1)),find(Trials(:,3)==missingCond));
            if ~isempty(trN)
                Data(stim_ch(missingStim),:,cix(tr))=Data(stim_ch(missingStim),:,trN); % stimulus from other modality
            end
            clear trN
        end
    end % 7:A condition 8:T condition


    %% z-score all trials
    for ix=1:size(Data_filt,1)
        for iz=1:size(Data_filt,3)
            Data_filt(ix,:,iz)=zscore(Data_filt(ix,:,iz),0,2);
        end
    end


    %% compute correlation and TRF model per condition and per analysis channel
    ncond = unique(Trials(:,3));
    % lambda_init = 10.^(-10:.5:10); % range of lambdas to be sampled
    lambda_init = 2.^(14:34); % range of lambdas to be sampled (Crosse JN 2015)
    TRF_R = cell(1,numel(stim_ch));
    TRF_P = cell(1,numel(stim_ch));
    TRF_model = cell(1,numel(stim_ch));
    for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
        TRF_R{refSig} = zeros(numel(ncond),size(Data_filt,1));
        TRF_P{refSig} = zeros(numel(ncond),size(Data_filt,1));
        TRF_model{refSig} = zeros(numel(ncond),size(Data_filt,1),length(lag_vec));
    end
    warning off;
    for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
        for c = 1:length(ncond)  % for each condition
            disp(['Condition ' num2str(c) '/' num2str(length(ncond))]);
            cix = find(Trials(:,3)==c);
            ntrials = numel(cix);
            trialsWithoutStimuli=[]; % discard trials that contain no stimulus
            for tr=1:ntrials % loop through all trials of current condition
                if all(Data(stim_ch(refSig),:,cix(tr))==0) % trial contains no stimulus
                    trialsWithoutStimuli=[trialsWithoutStimuli tr];
                end
            end % loop through all trials of current condition
            cix(trialsWithoutStimuli) = []; disp([   'number of trials without stimulus: ' num2str(numel(trialsWithoutStimuli))]); clear trialsWithoutStimuli; % discard trials that contain no stimulus
            ntrials = numel(cix); 
            for ch = 1:size(Data_filt,1)  % for each analysis channel (ch1: auditory, ch2: tactile)
                disp(['   Channel ' num2str(ch) '/' num2str(size(Data_filt,1))]);
                for tr=1:ntrials % loop through all trials of current condition

                    % find best lambda for current channel and set of trials
                    STIM = cell(1,ntrials-1);
                    DATA = cell(1,ntrials-1);
                    tr2N=0;
                    for tr2 = setdiff(1:ntrials,tr) % all trials except the current one
                        tr2N=tr2N+1;
                        if alignTacStimFlag==1 % when training on tactile stimuli, first align those stimuli 
                            tPtsSTIM=1+floor(maxStimShiftsmp/2):size(Data,2)-ceil(maxStimShiftsmp/2);
                            tPtsDATA=tPtsSTIM;
                            stimShiftsmp=(abs(SOAs(c))/1000)*fs; % shift AT stimuli by this number of samples
                            if SOAs(c)<0 && refSig==2 % tacile leading
                                tPtsSTIM=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                                tPtsDATA=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                            elseif SOAs(c)>0 && refSig==2 % tacile lagging
                                tPtsSTIM=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                                tPtsDATA=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                            end
                        else % do not align tactile stimuli
                            tPtsSTIM=1:size(Data,2); % use whole interval
                            tPtsDATA=tPtsSTIM;
                        end
                        STIM{tr2N} = squeeze(Data(stim_ch(refSig),tPtsSTIM,cix(tr2))'); % reference signal per trial of the current condition
                        DATA{tr2N} = squeeze(Data_filt(ch,tPtsDATA,cix(tr2))');         % EEG data per trial of the current condition
                        clear tPtsSTIM tPtsDATA
                    end
                    [~,~,MSE] = mTRFcrossval(STIM,DATA,fs,1,tmin,tmax,lambda_init); % lambda estimation: STIM and DATA come from all trials except the current one
                    [~,lambda_star_ix] = min(mean(MSE,1)); % select the lambda producing the smallest prediction error
                    lambda_star = lambda_init(lambda_star_ix); % best lambda
                    clear STIM DATA

                    % train model
                    tr2N=0;
                    for tr2 = setdiff(1:ntrials,tr) % all trials except the current one
                        tr2N=tr2N+1;
                        if alignTacStimFlag==1 % when training on tactile stimuli, first align those stimuli 
                            tPtsSTIM=1+floor(maxStimShiftsmp/2):size(Data,2)-ceil(maxStimShiftsmp/2);
                            tPtsDATA=tPtsSTIM;
                            stimShiftsmp=(abs(SOAs(c))/1000)*fs; % shift AT stimuli by this number of samples
                            if SOAs(c)<0 && refSig==2 % tacile leading
                                tPtsSTIM=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                                tPtsDATA=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                            elseif SOAs(c)>0 && refSig==2 % tacile lagging
                                tPtsSTIM=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                                tPtsDATA=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                            end
                        else % do not align tactile stimuli
                            tPtsSTIM=1:size(Data,2); % use whole interval
                            tPtsDATA=tPtsSTIM;
                        end
                        STIM = squeeze(Data(stim_ch(refSig),tPtsSTIM,cix(tr2))'); % reference signal per trial of the current condition
                        DATA = squeeze(Data_filt(ch,tPtsDATA,cix(tr2))');         % EEG data per trial of the current condition
                        clear tPtsSTIM tPtsDATA
                        [modelTr(tr2N,:),~,biasC(tr2N)] = mTRFtrain(STIM,DATA,fs,1,tmin,tmax,lambda_star);   % training: STIM and DATA come from all trials except the current one
                    end
                    model(tr,:)=mean(modelTr,1); clear modelTr; % average model across training trials
                    biasC=mean(biasC); % average bias across training trials

                    % validate model
                    if alignTacStimFlag==1 % when training on tactile stimuli, first align those stimuli 
                        tPtsSTIM=1+floor(maxStimShiftsmp/2):size(Data,2)-ceil(maxStimShiftsmp/2);
                        tPtsDATA=tPtsSTIM;
                        stimShiftsmp=(abs(SOAs(c))/1000)*fs; % shift AT stimuli by this number of samples
                        if SOAs(c)<0 && refSig==2 % tacile leading
                            tPtsSTIM=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                            tPtsDATA=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                        elseif SOAs(c)>0 && refSig==2 % tacile lagging
                            tPtsSTIM=1+ceil(maxStimShiftsmp-stimShiftsmp):size(Data,2)-floor(stimShiftsmp);
                            tPtsDATA=1+floor(stimShiftsmp):size(Data,2)-ceil(maxStimShiftsmp-stimShiftsmp);
                        end
                    else % do not align tactile stimuli
                        tPtsSTIM=1:size(Data,2); % use whole interval
                        tPtsDATA=tPtsSTIM;
                    end
                    STIM = squeeze(Data(stim_ch(refSig),tPtsSTIM,cix(tr))'); % reference signal per trial of the current condition
                    DATA = squeeze(Data_filt(ch,tPtsDATA,cix(tr))');         % EEG data per trial of the current condition
                    clear tPtsSTIM tPtsDATA
                    [~,R(tr),P(tr),~] = mTRFpredict(STIM,DATA,model(tr,:),fs,1,tmin,tmax,biasC); % testing:  STIM and DATA come from only the current (left-out) trial
                    clear STIM DATA

                end
                TRF_R{refSig}(c,ch) = mean(atanh(R)); % Fisher-transformed correlation coefficient (averaged across all splits/test trials)
                TRF_P{refSig}(c,ch) = mean((P)); % P-value (averaged across all splits/test trials)
                TRF_model{refSig}(c,ch,:) = mean(model,1); % TRF model (averaged across all splits/test trials)
                clear R P model
            end
        end
    end
    condLabels={'-300','-200','-100','0','100','200','AUD','TAC'};
    stimAlignFlagLabel={'','_tacAlign'};
    
    
    %% plot correlation 
    % per condition (solid: AT, dotted: A and T) 
    % per analysis channel (red: auditory channel, blue: tactile channel)
    % per reference signal (light: auditory, dark: tactile)
    figure
    plot(1:6,TRF_R{1}(1:6,1),'r.-','color',[1 .7 .7],'linewidth',2); hold on 
    plot(1:6,TRF_R{1}(1:6,2),'b.-','color',[.7 .7 1],'linewidth',2);
    plot(7:8,TRF_R{1}(7:8,1),'r.','color',[1 .7 .7],'MarkerSize',22);
    plot(7:8,TRF_R{1}(7:8,2),'b.','color',[.7 .7 1],'MarkerSize',22); 
    plot(1:6,TRF_R{2}(1:6,1),'r.-','linewidth',2); 
    plot(1:6,TRF_R{2}(1:6,2),'b.-','linewidth',2);
    plot(7:8,TRF_R{2}(7:8,1),'r.','MarkerSize',22);
    plot(7:8,TRF_R{2}(7:8,2),'b.','MarkerSize',22); 
    hold off
    set(gca,'xtick',1:8,'xticklabel',condLabels)
    xlabel('condition');ylabel('correlation coefficient');
    title(['dark=tacStim, light=audStim, blue=tacCh, red=audCh ' stimAlignFlagLabel{alignTacStimFlag+1}]);
    xlim([0 9])
    savefig([dp,'\S' subjInd '_forwardModel_corr' stimAlignFlagLabel{alignTacStimFlag+1} '.fig']);
    

    %% plot TRF model per condition and per analysis channel (for tactile reference signal)
    refSigLabels={'audStim','tacStim'};
    for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
        figure; 
        hold on;
        set(gcf,'name',refSigLabels{refSig});
        for c = 1:length(ncond)-2
            subplot(211)
            plot(lag_vec,squeeze(TRF_model{refSig}(c,1,:)),'color',repmat((c-1)*(9/50),1,3),'linewidth',3); hold on % darker gray = tactile leading more
            xlabel('lag (ms)');ylabel('TRF amplitude');title('auditory ch');
            subplot(212)
            plot(lag_vec,squeeze(TRF_model{refSig}(c,2,:)),'color',repmat((c-1)*(9/50),1,3),'linewidth',3); hold on % darker gray = tactile leading more
            xlabel('lag (ms)');ylabel('TRF amplitude');title('tactile ch');
        end
        subplot(211)
        plot(lag_vec,squeeze(TRF_model{refSig}(7,1,:)),'color',[1 0 0],'linewidth',3); hold on % condition A=red
        plot(lag_vec,squeeze(TRF_model{refSig}(8,1,:)),'color',[0 0 1],'linewidth',3); hold on % condition T=blue
        subplot(212)
        plot(lag_vec,squeeze(TRF_model{refSig}(7,2,:)),'color',[1 0 0],'linewidth',3); hold on % condition A=red
        plot(lag_vec,squeeze(TRF_model{refSig}(8,2,:)),'color',[0 0 1],'linewidth',3); hold on % condition T=blue
        savefig([dp,'\S' subjInd '_forwardModel_TRF_' refSigLabels{refSig} stimAlignFlagLabel{alignTacStimFlag+1} '.fig']);
    end

    save([dp,'\S' subjInd '_forwardModel_results' stimAlignFlagLabel{alignTacStimFlag+1} '.mat'],'TRF_R','TRF_P','TRF_model');

end % loop for subjects
toc



%% TO DO: further explore AT, A, and T:
% -in space (channel weights)
% -in frequency (redo the analysis after BP filtering the stimuli into a range of narrow bands)
