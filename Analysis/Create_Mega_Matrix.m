function Create_Mega_Matrix(Session_Num, Runs)

%%% load parameters
Analysis_Parameters

Set_Name = ['S', num2str(Session_Num, Padding), '_Epoched_Runs', '.set'];

%%% load data and csv
EEG = pop_loadset(Set_Name, EEG_Folder);
EEG = eeg_checkset(EEG);
CSV = readtable(CSV_Path);

% remove dummy trials
CSV = CSV(CSV.Type == Type & ismember(CSV.Run, Runs) & CSV.EEG == 1, :);

% check that number of trials match
if size(CSV, 1) ~= EEG.trials
    warndlg(['There are ', num2str(size(CSV, 1)), ' in the CSV, and ', ...
        num2str(EEG.trials), ' in the EEG file']);    
end

% create conditions matrix
Trials = [CSV.Trial_Number, CSV.Run, CSV.Condition];
Data = EEG.data;

New_Filename = [EEG_Folder, 'S', num2str(Session_Num, Padding), '_All_Runs.mat'];
save(New_Filename, 'Trials', 'Data')

