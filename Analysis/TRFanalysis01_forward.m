clear all;clc;close all;
dp = 'C:\Users\L.Riecke\Dropbox\FPN\Internship Sophia\05_Software';

%% set parameters
tmin = -200; % earliest lag used for tracking analysis
tmax = 800;  % latest lag used for tracking analysis
fs = 100; 
lag_vec = tmin:(1000/fs):tmax;


%% import channel locations
load(fullfile(dp,'ChannelLocations.mat')); % load channel location file
stim_ch = [63 64]; % indices of channels carrying the reference signals (stim1: auditory envelope, stim2: tactile envelope)
eeg_ch = setdiff(1:numel(CL),stim_ch);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% lambda estimation (done on data obtained from a separate run)
load(fullfile(dp,'S1_R0_Epoched_Matrix.mat')); % EEG data including reference signals

%% extract analysis channels (apply spatial filter)
loadName=fullfile(dp,'S1_channelWeights.mat');
if exist(loadName); 
    load(loadName); % channel weights (ERP amplitude at fixed latency)
    if size(chWeights,1)~=numel(eeg_ch);disp('ERROR: incorrect number of channel weights!');end
    chWeightsNorm=zeros(size(chWeights));
    Data_filt=zeros(size(chWeights,2),size(Data,2),size(Data,3));
    for ch = 1:size(chWeights,2)
        chWeightsNorm(:,ch)=chWeights(:,ch)/sqrt(sum((chWeights(:,ch).^2),1)); % Euclidean norm of channel weights
        temp=Data(eeg_ch,:,:);
        for indCh=1:size(temp,1)
            temp(indCh,:,:)=temp(indCh,:,:).*chWeightsNorm(indCh,ch);
        end
        Data_filt(ch,:,:)=(sum(temp(indCh,:,:),1))./sum(chWeightsNorm(:,ch)); % weighted average of all channels
    end
else % temporary selection of 2ch
    ana_ch = [38 47]; % ch1: auditory, ch2: tactile
    Data_filt = Data(ana_ch,:,:);
end

%% find best lambda for each analysis channel
ntrials = size(Data,3);
DATA = cell(1,ntrials);
STIM = cell(1,ntrials);
lambda_init = 10.^(-10:.5:10); % range of lambdas to be sampled
% lambda_init = 2.^(14:34); % range of lambdas to be sampled (Crosse JN 2015)
lambda_star_ix = zeros(1,size(Data_filt,1));
lambda_star = cell(1,numel(stim_ch));
for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
    for tr = 1:ntrials
        DATA{tr} = squeeze(Data_filt(:,:,tr)');          % EEG data per trial
        STIM{tr} = squeeze(Data(stim_ch(refSig),:,tr)'); % reference signal per trial
    end
    [R,P,MSE] = mTRFcrossval(STIM,DATA,fs,1,tmin,tmax,lambda_init);
    for ch = 1:size(Data_filt,1) % for each analysis channel
        [~,lambda_star_ix(ch)] = min(squeeze(mean(MSE(:,:,ch),1))); % select the lambda producing the smallest prediction error
    end
    lambda_star{refSig} = lambda_init(lambda_star_ix); % best lambda for each analysis channel
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% EEG tracking analysis (forward modelling)
load(fullfile(dp,'S1_All_Runs_Matrix.mat')); % EEG data including reference signals

%% extract analysis channels (apply spatial filter)
loadName=fullfile(dp,'S1_channelWeights.mat');
if exist(loadName); 
    load(loadName); % channel weights (ERP amplitude at fixed latency)
    if size(chWeights,1)~=numel(eeg_ch);disp('ERROR: incorrect number of channel weights!');end
    chWeightsNorm=zeros(size(chWeights));
    Data_filt=zeros(size(chWeights,2),size(Data,2),size(Data,3));
    for ch = 1:size(chWeights,2)
        chWeightsNorm(:,ch)=chWeights(:,ch)/sqrt(sum((chWeights(:,ch).^2),1)); % Euclidean norm of channel weights
        temp=Data(eeg_ch,:,:);
        for indCh=1:size(temp,1)
            temp(indCh,:,:)=temp(indCh,:,:).*chWeightsNorm(indCh,ch);
        end
        Data_filt(ch,:,:)=(sum(temp(indCh,:,:),1))./sum(chWeightsNorm(:,ch)); % weighted average of all channels
    end
else % temporary selection of 2ch
    ana_ch = [38 47]; % ch1: auditory, ch2: tactile
    Data_filt = Data(ana_ch,:,:);
end

%% compute correlation and TRF model per condition and per analysis channel
ncond = unique(Trials(:,3));
TRF_R = cell(1,numel(stim_ch));
TRF_model = cell(1,numel(stim_ch));
for refSig = 1:numel(stim_ch)
    TRF_R{refSig} = zeros(numel(ncond),size(Data_filt,1));
    TRF_model{refSig} = zeros(numel(ncond),size(Data_filt,1),length(lag_vec)+1);
end
for c = 1:length(ncond)
    cix = find(Trials(:,3)==c);
    ntrials = numel(cix);
    for ch = 1:size(Data_filt,1)  % for each analysis channel (ch1: auditory, ch2: tactile)
        DATA = cell(1,ntrials);
        STIM = cell(1,ntrials);
        for refSig = 1:numel(stim_ch) % reference signal 1: auditory envelope, 2: tactile envelope
            for tr = 1:ntrials
                DATA{tr} = squeeze(Data_filt(ch,:,cix(tr))');         % EEG data per trial of the current condition
                STIM{tr} = squeeze(Data(stim_ch(refSig),:,cix(tr))'); % reference signal per trial of the current condition
            end
            [R,P,MSE,~,model] = mTRFcrossval(STIM,DATA,fs,1,tmin,tmax,lambda_star{refSig}(ch));
            TRF_R{refSig}(c,ch) = mean(atanh(R)); % Fisher-transformed correlation coefficient (averaged across test trials)
            TRF_model{refSig}(c,ch,:) = squeeze(mean(model,1)); % TRF model (averaged across test trials)
        end
    end
end
condLabels={'-300','-200','-100','0','100','200','AUD','TAC'};

%% plot correlation 
% per condition (solid: AT, dotted: A and T) 
% per analysis channel (red: auditory channel, blue: tactile channel)
% per reference signal (light: auditory, dark: tactile)
figure
plot(1:6,TRF_R{1}(1:6,1),'r.-','color',[1 .8 .8]); hold on 
plot(1:6,TRF_R{1}(1:6,2),'b.-','color',[.8 .8 1]);
plot(7:8,TRF_R{1}(7:8,1),'r.','color',[1 .8 .8]);
plot(7:8,TRF_R{1}(7:8,2),'b.','color',[.8 .8 1]); 
plot(1:6,TRF_R{2}(1:6,1),'r.-'); 
plot(1:6,TRF_R{2}(1:6,2),'b.-');
plot(7:8,TRF_R{2}(7:8,1),'r.');
plot(7:8,TRF_R{2}(7:8,2),'b.'); 

hold off
set(gca,'xtick',1:8,'xticklabel',condLabels)
xlabel('condition');ylabel('correlation coefficient');
xlim([0 9])
CM = colormap('winter');
CM = CM(round(linspace(1,64,length(ncond))),:);

%% plot TRF model per condition and per analysis channel (for tactile reference signal)
figure
for c = 1:length(ncond)
    subplot(211)
    plot([lag_vec(1)-1000/fs lag_vec],squeeze(abs(TRF_model{1}(c,1,:))),'color',CM(c,:)); hold on
    xlabel('lag (ms)');ylabel('TRF amplitude');
    subplot(212)
    plot([lag_vec(1)-1000/fs lag_vec],squeeze(abs(TRF_model{1}(c,2,:))),'color',CM(c,:)); hold on
    xlabel('lag (ms)');ylabel('TRF amplitude');
end
