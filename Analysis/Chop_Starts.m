function Chop_Starts(Session_Num, Runs)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% set stimuli parameters
fs = 16000; % old fs

Analysis_Parameters


%%% set preprocessing parameters
LPeeg=45;   % upper cutoff frequency for EEG preprocessing (def:45Hz)
HPeeg=0.5;  % lower cutoff frequency for EEG preprocessing (def:0.5Hz)

fsNew=100;  % sampling rate for envelope analysis  (def:100Hz)

epochLimits= [-0.4, 2]; % [-15 0]; % epoch onset and offset times relative to trigger (def:[-15 0]s)
triggers={'Start'};     % triggers representing conditions (def: [1:7])


CSV = readtable(CSV_Path);
CSV = CSV(CSV.EEG == 1, :);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Clean Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Indx_R = Runs % loop for runs
    
    % import data
    Run_Name = ['S', num2str(Session_Num, Padding), '_R', num2str(Indx_R, Padding)];
    Set_Name = [Run_Name, '_Cleaned.set'];
    EEG = pop_loadset(Set_Name, EEG_Folder);
    EEG = eeg_checkset(EEG);
    
    Events = struct2table(EEG.event); % maybe this process should be elsewhere?
    Events = Events(strcmp(Events.code, 'Stimulus'), :); % removes non related triggers
    Events.Trial = cell2mat(Events.Trial); % turns cells to array
    Events_Redux = Events(ismember(Events.Trial, CSV.Trial_Number), :);
    EEG.event = table2struct(Events_Redux);
    
    load([Stimuli_Folder, Run_Name, '.mat'], 'stimuli')
    
    % downsample
    EEG = pop_resample(EEG,fsNew);
    EEG = eeg_checkset(EEG);
    
    
    if Indx_R >= 1 % save to struct the main experiment, to be merged later
        All_EEG(Indx_R) = EEG;
    end
    clear EEG
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Merge to single file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EEG = pop_mergeset(All_EEG, Runs, '0');
EEG = eeg_checkset(EEG);
% extract epochs
EEG = pop_epoch(EEG,triggers,epochLimits);
EEG = eeg_checkset(EEG);

pop_saveset(EEG,'filename',['S', num2str(Session_Num, Padding), '_All_From_Start.set'],...
    'filepath', EEG_Folder, 'check','on',...
    'savemode','onefile','version','6');


