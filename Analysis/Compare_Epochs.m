function Compare_Epochs(Session_Num, Runs)
% makes sure that event labels match PRT file
Analysis_Parameters

for Indx_R = Runs
    Set_Name = ['S', num2str(Session_Num, Padding), '_R',num2str(Indx_R, Padding), '_Filtered.set'];

    %%% load data and csv
    EEG = pop_loadset(Set_Name, EEG_Folder);
    EEG = eeg_checkset(EEG);
    CSV = readtable(CSV_Path);
    CSV = CSV(CSV.EEG == 1, :);
    Events = size(EEG.event, 2);
    Epochs = zeros(ceil(Events/3), 1);
    Counter = 1;
    for Indx_E = 1:Events
        if strcmp(EEG.event(Indx_E).type, 'Stop')
            Epochs(Counter) = EEG.event(Indx_E).Trial;
            Counter = Counter + 1;
        end
    end
    Epochs(Epochs == 0) = [];
    Trials = CSV.Trial_Number(CSV.Run == Indx_R & CSV.Type == 1);
    
    if length(Epochs) == length(Trials) && all(Epochs == Trials)
        disp(['Run ', num2str(Indx_R), ' is correct, with ', num2str(length(Epochs)), ' Epochs'])
    else
            Epochs_Unique = unique(Epochs);
            Trials_Unique = unique(Trials);
            Correct = intersect(Epochs, Trials);
            Epoch_Repeats = histc(Epochs, Correct);
            Trials_Repeats = histc(Trials, Correct); 
            if length(Epochs) == length(Trials)
                Mismatch = find(Epochs ~= Trials);
                disp(['Mismatch at epochs: ', num2str(Mismatch)])
            end
            Extra_Trials = setdiff(Trials_Unique, Epochs_Unique);
            Extra_Epochs = setdiff(Epochs_Unique, Trials_Unique);
            Mismatch_Repeats = Correct(find(Epoch_Repeats ~= Trials_Repeats));
            
            
            disp(['Unique trials not in epochs: ', num2str(Extra_Trials')])
            disp(['Unique epochs not in trials: ', num2str(Extra_Epochs')])
            disp(['Missing repeats in trial : ', num2str(Mismatch_Repeats')])
            warning(['something is wrong with run ', num2str(Indx_R)])

    end
end