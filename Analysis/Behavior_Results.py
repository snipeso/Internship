import os
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

from scipy import stats
from scipy.stats import ttest_ind
import matplotlib.pyplot as plt
from statsmodels.stats.multicomp import pairwise_tukeyhsd
from scipy.stats import ttest_ind

Session = 'Session_02'
Main_Folder = os.path.join('C:\\', 'Users', 'Sophia', 'Projects', 'Internship', 'Experiment')
CSV_Path = os.path.join(Main_Folder, Session, Session + '_PRT_Scored.csv')

# CSV_2 = pd.read_csv(CSV_Path)
CSV = pd.read_csv(CSV_Path)
# CSV = CSV.append(CSV_2)

Group = ['Run', 'Condition', 'Gender'][0]

CSV['Scores'] = CSV.Correct/CSV.Total
CSV['All_Correct'] = CSV.Correct == CSV.Total

Scores = CSV.groupby([Group])['Correct'].sum()/CSV.groupby(Group)['Total'].sum()
Plot = Scores.plot()
plt.show()

CSV_Run = CSV[CSV.Run == 12]

CSV_A = CSV[CSV.Condition == 7]
Scores_A = CSV_A.groupby([Group])['Correct'].sum()/CSV_A.groupby(Group)['Total'].sum()

CSV_B = CSV[CSV.Condition == 4]
Scores_B = CSV_B.groupby([Group])['Correct'].sum()/CSV_B.groupby(Group)['Total'].sum()
Plot = Scores_A.plot()
Plot = Scores_B.plot()
axes = plt.gca()
axes.set_ylim([0, 1])
plt.show()

Boxplot = CSV.boxplot('Scores', by=Group)
plt.show()

CSV[CSV.Condition == 7]['Scores'].plot()



def ANOVA(DF, DV, GpC):
    ''' run anova on dataframe
    :param DF: dataframe
    :param DV: dependent variable column name
    :param GpC: grouping column name
    :return: statistic and p value
    '''
    grouped = DF.groupby(GpC)
    groups = [grouped[DV].get_group(key) for key in grouped.groups.keys()]
    F, p = stats.f_oneway(*groups)
    if p >= 0.05:
        return(F, p, 'no tuk')
    else:
        #pairwise comparisons
        tukey = pairwise_tukeyhsd(endog=DF[DV],  # Data
                                  groups=DF[GpC],  # Groups
                                  alpha=0.05)  # Significance level

        tukey.plot_simultaneous(figsize=(5,3))  # Plot group confidence intervals
        axes = plt.gca()
        # axes.set_ylim([0.34, 0.41])
        plt.show()

        return(F, p, tukey.summary())


F, p, Tukey = ANOVA(CSV, 'Scores', 'Run')

a, p = ttest_ind(CSV[CSV.Run == 5]['Scores'], CSV[CSV.Run == 8]['Scores'])

