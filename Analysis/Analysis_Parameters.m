
% Constants
Padding = '%02.f';
fs = 16000; % old fs
Type = 1; % if just analysing test stimuli (dummy is 2)

% Folders
Main_Folder = 'C:\Users\Sophia\Projects\Internship\Experiment\';
Session = ['S', num2str(Session_Num, Padding)]; % need to define Session_Num before calling parameters
EEG_Folder= [Main_Folder, Session, '\', Session,'_EEG\'];
Stimuli_Folder = [Main_Folder, Session, '\', Session,'_Stimuli\'];
CSV_Path = [Main_Folder, Session, '\', Session, '_PRT.csv'];
Scores_Path = [Main_Folder, Session, '\', Session, '_PRT_Scored.csv'];

% Labels
Condition_Labels = {'-300', '-200', '-100', '0', '100', '200', 'A'};

% Runs
Session_Info = struct();
Session_Info(1).Runs = 2:12;
Session_Info(2).Runs = 1:8;
Session_Info(3).Runs = 1:7;
Session_Info(4).Runs = 1:9;
Session_Info(5).Runs = 1:8;
Session_Info(6).Runs = 1:10;
Session_Info(7).Runs = 1:8;
Session_Info(8).Runs = 1:10;
Session_Info(9).Runs = 1:10;
Session_Info(10).Runs = 1:10;
Session_Info(11).Runs = 1:10;
Session_Info(12).Runs = 1:10;
Session_Info(13).Runs = 1:10;
Session_Info(14).Runs = 1:9;

Runs = Session_Info(Session_Num).Runs;


%%% preprocessing parameters
LPeeg=45;   % upper cutoff frequency for EEG preprocessing (def:45Hz)
HPeeg=0.5;  % lower cutoff frequency for EEG preprocessing (def:0.5Hz)

LPenv=16;    % upper cutoff frequency for envelope analysis (def:8Hz)
HPenv=1;    % lower cutoff frequency for envelope analysis (def:1Hz)

fsNew=100;  % sampling rate for envelope analysis  (def:100Hz)

Main_Epoch = [-15.3, -0.3];
Epoch_Reference = {'Stop'};