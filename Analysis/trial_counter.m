Sessions = 1:14;

Data = table();

for Session_Num = Sessions
    Analysis_Parameters
    CSV = readtable(CSV_Path);

    Data.Session(Session_Num) = Session_Num;
    Data.Test_EEG(Session_Num)  =  size(CSV(CSV.EEG == 1 & CSV.Type == 1, :), 1);
    Data.Dummy_EEG(Session_Num)  = size(CSV(CSV.EEG == 1 & CSV.Type == 2, :), 1);
    Data.Recordings(Session_Num)  = size(CSV(CSV.Recording == 1, :), 1);
    
end
writetable(Data, 'Amount_of_Data.csv')
    