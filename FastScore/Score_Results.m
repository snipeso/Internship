function Score_Results(Mat_Filename)
close all
load(Mat_Filename, 'Score_Matrix', 'Trial_Matrix')
Target = 0.4;
Categories = unique(Trial_Matrix(:, end));
Proportions = Categories*0;

for Indx_C = 1:length(Categories)
    Scores = Score_Matrix(Trial_Matrix(:, end) == Categories(Indx_C), :);
    Scores = Scores(Scores(:, end) == 2, 1:end-1);
    Missed = length(Scores(Scores == 1));
    Correct = length(Scores(Scores == 2));
    Proportions(Indx_C) = Correct/(Missed + Correct);
end

disp([Categories, Proportions])
Proportions = Proportions';
% Proportions = [0.1, 0.3, 0.5, 1]; % Fake nice results
[Line, Index] = plotPF(Proportions, Target);
Lower = floor(Index);
Upper = ceil(Index);


Value = (Categories(Upper) - Categories(Lower))*(Index - Lower) + Categories(Lower);


figure(1)

plot(Line)
hold on
plot(Proportions)
plot(Index, Target, 'r*')
text(Index, Target, num2str(Value))
legend('Model', 'Data')
title(['Target = ', num2str(Target)])
