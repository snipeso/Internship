%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Auto GUI functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = FastScoreGUI(varargin)
% FASTSCOREGUI MATLAB code for FastScoreGUI.fig
%      FASTSCOREGUI, by itself, creates a new FASTSCOREGUI or raises the existing
%      singleton*.
%
%      H = FASTSCOREGUI returns the handle to a new FASTSCOREGUI or the handle to
%      the existing singleton*.
%
%      FASTSCOREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FASTSCOREGUI.M with the given input arguments.
%
%      FASTSCOREGUI('Property','Value',...) creates a new FASTSCOREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FastScoreGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FastScoreGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FastScoreGUI

% Last Modified by GUIDE v2.5 17-Apr-2018 17:24:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @FastScoreGUI_OpeningFcn, ...
    'gui_OutputFcn',  @FastScoreGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FastScoreGUI is made visible.
function FastScoreGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FastScoreGUI (see VARARGIN)

% Choose default command line output for FastScoreGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FastScoreGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FastScoreGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Unused functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






% --- Executes on button press in word_1_tgl.
function word_1_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_1_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_1_tgl


% --- Executes on button press in word_2_tgl.
function word_2_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_2_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_2_tgl


% --- Executes on button press in word_3_tgl.
function word_3_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_3_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_3_tgl


% --- Executes on button press in word_4_tgl.
function word_4_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_4_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_4_tgl


% --- Executes on button press in word_5_tgl.
function word_5_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_5_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_5_tgl


% --- Executes on button press in word_6_tgl.
function word_6_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_6_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_6_tgl


% --- Executes on button press in word_7_tgl.
function word_7_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_7_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_7_tgl


% --- Executes on button press in word_8_tgl.
function word_8_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_8_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_8_tgl


% --- Executes on button press in word_9_tgl.
function word_9_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_9_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_9_tgl


% --- Executes on button press in word_10_tgl.
function word_10_tgl_Callback(hObject, eventdata, handles)
% hObject    handle to word_10_tgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of word_10_tgl


% --- Executes on key press with focus on shortcuts and none of its controls.
function shortcuts_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to shortcuts (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Button Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Create global variables
handles.Sentence_Number = 1;

% load user file
[filename,pathname] = uigetfile('*.csv', 'Select the csv file with sentences');

% handles.Sentence_Table = readtable([pathname, filename]);
[handles.Sentence_Table, handles.Trial_Matrix] = Convert_Table(pathname, filename);
set(handles.filename_box, 'String', filename)

% create score matrix, saved to .mat file
Rows = size(handles.Sentence_Table, 1);
Cols = size(handles.Sentence_Table, 2);
handles.Score_Matrix = zeros(Rows, Cols + 1);
Mat_Filename = [pathname, extractBefore(filename, '.csv'), '_Scores.mat']; % save score to location of trials
if not(exist(Mat_Filename, 'file'))
    handles.Scores = matfile(Mat_Filename, 'Writable', true);
    handles.Scores.Score_Matrix = handles.Score_Matrix;
else
    handles.Scores = matfile(Mat_Filename, 'Writable', true);
    handles.Score_Matrix = handles.Scores.Score_Matrix;
    disp(handles.Score_Matrix)
end
handles.Mat_Filename = Mat_Filename;
handles.Scores.Trial_Matrix = handles.Trial_Matrix;
% calls function to set up first trial, updating the global sentence number
handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');


guidata(hObject, handles)



% --- Executes on button press in Next_Trial_Button.
function Next_Trial_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Next_Trial_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% proceeds to next trial unless at end
if handles.Sentence_Number <= size(handles.Sentence_Table, 1)
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');
    guidata(hObject, handles);
end

guidata(hObject, handles);



% --- Executes on button press in Previous_Trial_Button.
function Previous_Trial_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Previous_Trial_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% returns to previous trial
if handles.Sentence_Number > 1
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'previous');
    guidata(hObject, handles);
end

guidata(hObject, handles);



% --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.Score_Matrix = Save_Row(hObject, eventdata, handles, 2);
handles.Scores.Score_Matrix = handles.Score_Matrix; % saves to Scores.mat file

% moves to next trial
Remaining_Trials = handles.Score_Matrix(handles.Score_Matrix(:, end) < 2, end);
if length(Remaining_Trials) >= 1
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');
    guidata(hObject, handles)
else
    guidata(hObject, handles)
    End_Popup(hObject, eventdata, handles, 'You have scored everything! ')
end




% --- Executes on button press in skip_button.
function skip_button_Callback(hObject, eventdata, handles)
% hObject    handle to skip_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save score matrix, flagging trial as 'skipped'
handles.Score_Matrix = Save_Row(hObject, eventdata, handles, 1);
handles.Scores.Score_Matrix = handles.Score_Matrix;

% proceeds to next trial
handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');

guidata(hObject, handles)



% --- Executes on button press in Clear_button.
function Clear_button_Callback(hObject, eventdata, handles)
% hObject    handle to Clear_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% cycles through words, unselecting them
for Col = 1:size(handles.Sentence_Table, 2)
    set(handles.(['word_', num2str(Col), '_tgl']),'Value', 0)
end

guidata(hObject, handles); %maybe not needed



% --- Executes on button press in Select_All_Button.
function Select_All_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Select_All_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% cycles through words, selecting all of them
for Col = 1:size(handles.Sentence_Table, 2)
    set(handles.(['word_', num2str(Col), '_tgl']),'Value', 1)
end

guidata(hObject, handles); %maybe not needed



% --- Executes on button press in review_button.
function review_button_Callback(hObject, eventdata, handles)
% hObject    handle to review_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of review_button

handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');



% --- Executes on button press in score_button.
function score_button_Callback(hObject, eventdata, handles)
% hObject    handle to score_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if any(ismember([0,1], handles.Score_Matrix(:, end)))
    String = 'But wait! You did not finish! ';
    whos
    disp(String)
    guidata(hObject, handles); 
    End_Popup(hObject, eventdata, handles, String)
    
else
    Score_Results(handles.Mat_Filename)
end




% --- Executes on button press in shortcuts.
function shortcuts_Callback(hObject, eventdata, handles)
% hObject    handle to shortcuts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of shortcuts

% displays shortcut keys close to respective buttons
if get(handles.shortcuts, 'Value') == 1
    set(handles.shortcut_values, 'Visible', 'on')
    set(handles.shortcuts_2, 'Visible', 'on')
    set(handles.shortcut_3, 'Visible', 'on')
else
    set(handles.shortcut_values, 'Visible', 'off')
    set(handles.shortcuts_2, 'Visible', 'off')
    set(handles.shortcut_3, 'Visible', 'off')
end


guidata(hObject, handles); %maybe not needed



% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% when selected, allows button selection with key presses
if get(handles.shortcuts, 'Value') == 1
    switch  eventdata.Key
        case 'leftarrow'
            Previous_Trial_Button_Callback(hObject, eventdata, handles)
        case 'rightarrow'
            Next_Trial_Button_Callback(hObject, eventdata, handles)
        case 'return'
            save_button_Callback(hObject, eventdata, handles)
        case 'a'
            Select_All_Button_Callback(hObject, eventdata, handles)
        case 'x'
            Clear_button_Callback(hObject, eventdata, handles)
        case 's'
            skip_button_Callback(hObject, eventdata, handles)
        otherwise
            % sees if key is a number, which corresponds to toggles
            if not(isempty(strfind('0123456789', eventdata.Key)))
                % sees if associated toggle is visible
                Visible = strcmp(get(handles.(['word_', eventdata.Key, '_tgl']), 'Visible'), 'on');
                if Visible && get(handles.(['word_', eventdata.Key, '_tgl']), 'Value') == 0
                    set(handles.(['word_', eventdata.Key, '_tgl']), 'Value', 1)
                else % if not visible, nothing gets selected, so isn't saved to matrix
                    set(handles.(['word_', eventdata.Key, '_tgl']), 'Value', 0)
                end
            else
                disp(eventdata.Key) % for debuging
            end
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Added Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Sentence_Table, Trial_Matrix] = Convert_Table(pathname, filename)
try
    Session_Table = readtable([pathname, filename]);
    % Session_Table = Session_Table(Session_Table.Type <= 1, :); % TODO: either make sure it is in threshold test, or remove from here
    Session_Table.Properties.VariableNames{1} = 'Trial_Number';
    test = Session_Table.Sentence(1);
catch
    warndlg('Something is wrong with your table. Does it have sentences?')
end
    
Trials = size(Session_Table, 1);
Sentence_Table = cell2table(cell([Trials, 10]));
Sentence_Table(:, :) = {''};
Trial_Matrix = zeros(Trials, 3);

for Indx_T = 1:Trials
    Text = Session_Table.Sentence{Indx_T};
    Text_List = strsplit(Text);
    
    for Indx_W = 1:length(Text_List)
        Sentence_Table(Indx_T, Indx_W) = Text_List(Indx_W);
    end
    Trial_Matrix(Indx_T, 1) = Session_Table.Trial_Number(Indx_T); % trial number
    Trial_Matrix(Indx_T, 2) = Session_Table.Gender(Indx_T); % trial gender
    Trial_Matrix(Indx_T, 3) = Session_Table.Condition(Indx_T); % trial condition
    
end



%%% sets up new trial in figure
function Row = Set_Sentence(hObject, eventdata, handles, direction)
Rows = size(handles.Sentence_Table, 1);
Cols = size(handles.Sentence_Table, 2);

% selects either all trials, or unfinished trials
Tags = [[1:Rows]', handles.Score_Matrix(:, end)];
if get(handles.review_button, 'Value') % only gets trials that aren't saved
    Tags = Tags(Tags(:, 1) == handles.Sentence_Number | Tags(:, 2) < 2, 1);
    disp(['remaining sentences: ', num2str(length(Tags))])
else % gets all trials
    Tags = Tags(:, 1);
end


% based on command, advances or retreats from current trial
Indx = find(Tags == handles.Sentence_Number); % finds location of current trial
switch direction
    case 'previous'
        if Indx < 2
            Row = Tags(end); % loops to last trial
        else
            Row = Tags(Indx - 1);
        end
    case 'next'
        if Indx >= length(Tags)
            Row = Tags(1); % loops to first trial
        else
            Row = Tags(Indx + 1);
        end
    case 'first' % for load setting
        Row = Tags(1);
    case 'current'
        Row = str2double(get(handles.Sentence_Number_Box, 'String'));
end


% cycles through words to set up toggle buttons
for Col = 1:Cols
    % retrieves word string
    word = handles.Sentence_Table{Row, Col};
    word = word{1};
    
    % shows toggles only for words and not blanks
    if length(word) < 1
        set(handles.(['word_', num2str(Col), '_tgl']),'Visible', 'off')
    else
        set(handles.(['word_' num2str(Col), '_tgl']), 'String', word, 'Visible', 'on')
    end
    
    % unselects all toggles. Toggles get restored if saved to matrix
    set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 0)
end


% displays strings of neighboring trials
if Row >1
    Prev_Sent = strjoin(string(handles.Sentence_Table{Row-1, :}));
    set(handles.previous_sentence_box, 'String', Prev_Sent)
end

if Row < size(handles.Sentence_Table, 1)
    Next_Sent = strjoin(string(handles.Sentence_Table{Row+1, :}));
    set(handles.next_sentence_box, 'String', Next_Sent)
end


% displays current trial number in GUI
set(handles.Sentence_Number_Box, 'String', Row)


% toggles previously chosen words
for Col = 1:size(handles.Sentence_Table, 2)
    Box_Value = handles.Score_Matrix(Row, Col);
    if Box_Value == 2
        set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 1)
    else
        set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 0)
    end
end

% toggle skip if skipped
Skipped = handles.Score_Matrix(Row, end);
if Skipped == 1
    set(handles.skip_button, 'Value', 1);
else
    set(handles.skip_button, 'Value', 0);
end


guidata(hObject, handles); %maybe not needed



% popup indicating end of scoring. lets save or continue editing
function End_Popup(hObject, eventdata, handles, Pop_String)

answer = questdlg([Pop_String, 'Click Edit if you still wish to make changes, or Score to finish.'], ...
    'Time to Score?', ...
    'Edit', 'Score', 'Edit');
switch answer
    case 'Edit'
        set(handles.review_button, 'Value', 1)
        handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');
        guidata(hObject, handles);
    case 'Score'
        guidata(hObject, handles);
        Score_Results(handles.Mat_Filename)
end




% outputs new matrix with row based on selected words
function Matrix = Save_Row(hObject, eventdata, handles, Value)

Matrix = handles.Score_Matrix; % gets old matrix
Row = handles.Sentence_Number; % gets current trial number

% identifies selected words from toggles
for Col = 1:size(handles.Sentence_Table, 2)
    if strcmp(get(handles.(['word_' num2str(Col), '_tgl']), 'Visible'), 'on') % sees if word is even there
        Matrix(Row, Col) = get(handles.(['word_' num2str(Col), '_tgl']), 'Value') + 1; %gets toggled value per word
    end
end

Matrix(Row, end) = Value; % sets status to last column (2 if saved, 1 if skipped)
disp(Matrix) % visualizes matrix for debug purposes


% TODO
% - make threshold test CSV
% - make array of conditions (flexible) save to mat file
% - internal score function
% - makes "review skipped" work. it currently needs to go to the last not
% done



function Sentence_Number_Box_Callback(hObject, eventdata, handles)
% hObject    handle to Sentence_Number_Box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sentence_Number_Box as text
%        str2double(get(hObject,'String')) returns contents of Sentence_Number_Box as a double

handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'current');
disp(handles.Sentence_Number)
guidata(hObject, handles);
