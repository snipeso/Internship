function [fitLine,threshInd]=plotPF(data,targetPerform)

%%% plots PF
%%% extracts threshold expressed as a non-integer index (thus this only works for equidistant conditions)


x=data;
y=1:numel(x);

%%% fit psychometric function
A=[1;1];
sigfunc=@(A,x)(A(1)./(A(2)+exp(-x))); % sigmoid function (logistic function)
A0=ones(size(A)); % initial values fed into the iterative algorithm nlinfit
A_fit=nlinfit(y',x',sigfunc,A0); % nonlinear regression

%%% plot fitted curve
fitLine=A_fit(1)./(A_fit(2)+exp(-y));
%plot(y,fitLine,'--r','LineWidth',2); hold on; 
clear y

%%% find points surrounding desired performance level
[~,indMin]=min(abs(fitLine-targetPerform));
if fitLine(indMin)<targetPerform && indMin<numel(fitLine)
    indMins(1)=indMin;
    indMins(2)=indMin+1;
elseif fitLine(indMin)>targetPerform && indMin>1
    indMins(1)=indMin-1;
    indMins(2)=indMin;
else
    indMins(1)=indMin;
    indMins(2)=indMin;
end    

%%% compute threshold=weighted average
w(1)=abs(fitLine(indMins(1))-targetPerform); % distance from lower point
w(2)=fitLine(indMins(2))-targetPerform;      % distance from upper point
if sum(w)~=0
    threshInd=(indMins(1)*w(2)+indMins(2)*w(1))/sum(w); % closer point is weighted more, absolute value (not dB)
else
    threshInd=mean(indMins);
end

