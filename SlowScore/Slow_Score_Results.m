function Slow_Score_Results(Mat_Filename)
close all
load(Mat_Filename, 'Score_Matrix', 'Trial_Matrix')
Target = 0.5;
Categories = unique(Trial_Matrix(:, end));
Proportions = Categories*0;

for Indx_C = 1:length(Categories)
    Scores = Score_Matrix(Trial_Matrix(:, end) == Categories(Indx_C), :);
    Scores = Scores(Scores(:, end) == 2, 1:end-1);
    Missed = length(Scores(Scores == 1));
    Correct = length(Scores(Scores == 2));
    Proportions(Indx_C) = Correct/(Missed + Correct);
end

disp([Categories, Proportions])
Proportions = Proportions';
% Proportions = [0.1, 0.3, 0.5, 1]; % Fake nice results
[Line, Index] = plotPF(Proportions, Target);
Lower = floor(Index);
Upper = ceil(Index);


Value = (Categories(Upper) - Categories(Lower))*(Index - Lower) + Categories(Lower);


figure(1)
plot(Line)
hold on
plot(Proportions)
plot(Index, Target, 'r*')
text(Index, Target, num2str(Value))
legend('Model', 'Data')


% TODO
% - load Session Summary CSV, select sentence and trial number
% - save all to matfile
% - when click "score" this also saves scores to CSV as correct words vs
% total words
% - this file shouldn't do much
% - adjust to max 15 words
% - add shortcuts
% - check that keyboard shortcut for selecting a trial doesnt actually
% select the trial!!

