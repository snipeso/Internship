%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Auto GUI functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = SlowScoreGUI(varargin)
% SLOWSCOREGUI MATLAB code for SlowScoreGUI.fig
%      SLOWSCOREGUI, by itself, creates a new SLOWSCOREGUI or raises the existing
%      singleton*.
%
%      H = SLOWSCOREGUI returns the handle to a new SLOWSCOREGUI or the handle to
%      the existing singleton*.
%
%      SLOWSCOREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SLOWSCOREGUI.M with the given input arguments.
%
%      SLOWSCOREGUI('Property','Value',...) creates a new SLOWSCOREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SlowScoreGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SlowScoreGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SlowScoreGUI

% Last Modified by GUIDE v2.5 08-Jun-2018 19:03:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SlowScoreGUI_OpeningFcn, ...
    'gui_OutputFcn',  @SlowScoreGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SlowScoreGUI is made visible.
function SlowScoreGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SlowScoreGUI (see VARARGIN)

% Choose default command line output for SlowScoreGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SlowScoreGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SlowScoreGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Unused functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in word_1_tgl.
function word_1_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_2_tgl.
function word_2_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_3_tgl.
function word_3_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_4_tgl.
function word_4_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_5_tgl.
function word_5_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_6_tgl.
function word_6_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_7_tgl.
function word_7_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_8_tgl.
function word_8_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_9_tgl.
function word_9_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_10_tgl.
function word_10_tgl_Callback(hObject, eventdata, handles)


% --- Executes on button press in word_11_tgl.
function word_11_tgl_Callback(hObject, eventdata, handles)



% --- Executes on button press in word_12_tgl.
function word_12_tgl_Callback(hObject, eventdata, handles)



% --- Executes on button press in word_13_tgl.
function word_13_tgl_Callback(hObject, eventdata, handles)



% --- Executes on button press in word_14_tgl.
function word_14_tgl_Callback(hObject, eventdata, handles)



% --- Executes on button press in word_15_tgl.
function word_15_tgl_Callback(hObject, eventdata, handles)




% --- Executes on key press with focus on shortcuts and none of its controls.

% TODO: check if needed
function shortcuts_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to shortcuts (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Button Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- gets the CSV and sets the first trial
function Load_Button_Callback(hObject, eventdata, handles)

%%% Create global variables
handles.Sentence_Number = 1;
handles.Audio_Number = 0;

%%% load user file
[filename,pathname] = uigetfile('*.csv', 'Select the csv file with sentences');
handles.filename = filename;
handles.pathname = pathname;
set(handles.filename_box, 'String', filename)


%%% create tables
% individual words & trial information matrix
[handles.Sentence_Table, handles.Trial_Matrix] = Convert_Table(pathname, filename);

% score matrix, saved to .mat file
Rows = size(handles.Sentence_Table, 1);
Cols = size(handles.Sentence_Table, 2);
handles.Score_Matrix = zeros(Rows, Cols + 1);
Mat_Filename = [pathname, '\' extractBefore(filename, '.csv'), '_Scores.mat']; % save score to location of trials

if not(exist(Mat_Filename, 'file'))
    handles.Scores = matfile(Mat_Filename, 'Writable', true); % i really don't know why I have to repeat this, but it crashes otherwise
    handles.Scores.Score_Matrix = handles.Score_Matrix;
else
    handles.Scores = matfile(Mat_Filename, 'Writable', true);
    handles.Score_Matrix = handles.Scores.Score_Matrix;
    disp(handles.Score_Matrix)
end

handles.Mat_Filename = Mat_Filename;
handles.Scores.Trial_Matrix = handles.Trial_Matrix;

%%% set up first trial, updating the global sentence number
handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');

guidata(hObject, handles)



% --- gets folder with audio files
function Load_Audio_Callback(hObject, eventdata, handles)

handles.audio_path = uigetdir(handles.pathname);
set(handles.audiofolder_box, 'String', handles.audio_path)

guidata(hObject, handles)



% --- goes to next trial without saving anything
function Next_Trial_Button_Callback(hObject, eventdata, handles)

% proceeds to next trial unless at end
if handles.Sentence_Number <= size(handles.Sentence_Table, 1)
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');
    guidata(hObject, handles);
end

guidata(hObject, handles);



% --- goes to previous trial without saving anything
function Previous_Trial_Button_Callback(hObject, eventdata, handles)

% returns to previous trial
if handles.Sentence_Number > 1
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'previous');
    guidata(hObject, handles);
end

guidata(hObject, handles);



% --- saves selected configuration of words
function save_button_Callback(hObject, eventdata, handles)

% updates score matrices
handles.Score_Matrix = Save_Row(hObject, eventdata, handles, 2); % 2 indicates skipped
handles.Scores.Score_Matrix = handles.Score_Matrix; % saves to Scores.mat file

% moves to next trial
Remaining_Trials = handles.Score_Matrix(handles.Score_Matrix(:, end) < 2, end);
if length(Remaining_Trials) >= 1
    handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');
    guidata(hObject, handles)
else
    guidata(hObject, handles)
    End_Popup(hObject, eventdata, handles, 'You have scored everything! ')
end



% --- saves selected configuration, but flags trial as skipped
function skip_button_Callback(hObject, eventdata, handles)

% Save score matrix
handles.Score_Matrix = Save_Row(hObject, eventdata, handles, 1); % 1 indicates skipped
handles.Scores.Score_Matrix = handles.Score_Matrix;

% proceeds to next trial
handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'next');

guidata(hObject, handles)



% --- unselects all the words
function Clear_button_Callback(hObject, eventdata, handles)

for Col = 1:size(handles.Sentence_Table, 2)
    set(handles.(['word_', num2str(Col), '_tgl']),'Value', 0)
end

guidata(hObject, handles); %maybe not needed



% --- selects all the words
function Select_All_Button_Callback(hObject, eventdata, handles)

for Col = 1:size(handles.Sentence_Table, 2)
    set(handles.(['word_', num2str(Col), '_tgl']),'Value', 1)
end

guidata(hObject, handles); %maybe not needed



% --- go to first unfinished trial, and subsequently only view incompleted
function review_button_Callback(hObject, eventdata, handles)

handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');

guidata(hObject, handles);



% --- saves scores to a csv
function Save_CSV_button_Callback(hObject, eventdata, handles)

if any(ismember([0,1], handles.Score_Matrix(:, end)))
    String = 'But wait! You did not finish! ';
    guidata(hObject, handles);
    End_Popup(hObject, eventdata, handles, String)
    
else
    Save_CSV(hObject, eventdata, handles)
    guidata(hObject, handles);
end



% --- shows keyboard shortcuts next to respective buttons
function shortcuts_Callback(hObject, eventdata, handles)

% displays shortcut keys close to respective buttons
if get(handles.shortcuts, 'Value') == 1
    set(handles.shortcut_values, 'Visible', 'on')
    set(handles.shortcuts_2, 'Visible', 'on')
    set(handles.shortcut_3, 'Visible', 'on')
    set(handles.shortcuts_4, 'Visible', 'on')
    set(handles.shortcuts_5, 'Visible', 'on')
else
    set(handles.shortcut_values, 'Visible', 'off')
    set(handles.shortcuts_2, 'Visible', 'off')
    set(handles.shortcut_3, 'Visible', 'off')
    set(handles.shortcuts_4, 'Visible', 'off')
    set(handles.shortcuts_5, 'Visible', 'off')
end

guidata(hObject, handles); %maybe not needed



% --- lets you jump to any trial
function Sentence_Number_Box_Callback(hObject, eventdata, handles)

handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'current');

guidata(hObject, handles);



% --- gets associated audio to play
function Play_Button_Callback(hObject, eventdata, handles)

if not(handles.Sentence_Number == handles.Audio_Number) % checks that audio isn't already loaded
    
    % get variables and strings
    R = handles.Trial_Matrix(handles.Sentence_Number, 2); % gets run number
    Trial = handles.Trial_Matrix(handles.Sentence_Number, 1); % gets trial ID
    S = regexp(handles.filename,'\d*','Match'); % gets session number
    Recordings_Folder = [handles.audio_path, '\S', S{1}, '_R', num2str(R, '%02.f'), '\'];
    
    % get all trials in recording run folder
    Recordings = ls(Recordings_Folder);
    try
        Recording_Trials = str2double(string(Recordings(:, 7:9))); % extracts trial number from string
        Row = find(Recording_Trials == Trial); % for getting full audio name again
        
        % load audio
        [s, fs] = audioread([Recordings_Folder, Recordings(Row, :)]);
        handles.Audio_Number = handles.Sentence_Number; % this is to keep track of current audio so you don't keep downloading it
        handles.Sound = audioplayer(s, fs);
        
    catch
        warndlg(['Trial ', num2str(Trial), ' from Run ', num2str(R), ' Is not found.'])
        return
    end
end

play(handles.Sound)

guidata(hObject, handles);



% --- stops currently playing audio
function Stop_Button_Callback(hObject, eventdata, handles)

stop(handles.Sound)



% --- sets shortcut keys
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)

% when selected, allows button selection with key presses
if get(handles.shortcuts, 'Value') == 1
    switch  eventdata.Key
        case 'leftarrow'
            Previous_Trial_Button_Callback(hObject, eventdata, handles)
        case 'rightarrow'
            Next_Trial_Button_Callback(hObject, eventdata, handles)
        case 'return'
            save_button_Callback(hObject, eventdata, handles)
        case 'a'
            Select_All_Button_Callback(hObject, eventdata, handles)
        case 'x'
            Clear_button_Callback(hObject, eventdata, handles)
        case 's'
            skip_button_Callback(hObject, eventdata, handles)
        case 'o'
            Stop_Button_Callback(hObject, eventdata, handles)
        case 'p'
            Play_Button_Callback(hObject, eventdata, handles)
        otherwise
            % sees if key is a number, which corresponds to toggles
            if not(isempty(strfind('0123456789qwert', eventdata.Key)))
                % sees if associated toggle is visible
                switch eventdata.Key
                    case '0'
                        Key = '10';
                    case 'q'
                        Key = '11';
                    case 'w'
                        Key = '12';
                    case 'e'
                        Key = '13';
                    case 'r'
                        Key = '14';
                    case 't'
                        Key = '15';
                    otherwise
                        Key = eventdata.Key;
                end
                Visible = strcmp(get(handles.(['word_', Key, '_tgl']), 'Visible'), 'on');
                if Visible && get(handles.(['word_', Key, '_tgl']), 'Value') == 0
                    set(handles.(['word_', Key, '_tgl']), 'Value', 1)
                else % if not visible, nothing gets selected, so isn't saved to matrix
                    set(handles.(['word_',Key, '_tgl']), 'Value', 0)
                end
            else
                disp(eventdata.Key) % for debuging
            end
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Added Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% creates trial tables. TODO: check if trial matrix is ever needed
function [Sentence_Table, Trial_Matrix] = Convert_Table(pathname, filename)

%%% load CSV
try
    Session_Table = readtable([pathname, filename]);
    Session_Table.Properties.VariableNames{1} = 'Trial_Number'; % in case theres CSV junk
    
    % remove tactile only conditions
    Session_Table = Session_Table(Session_Table.Condition ~= 8, :);
catch
    warndlg('Something is wrong with your table. Does it have sentences?')
end


% prepare new table/matrix
Trials = size(Session_Table, 1);
Sentence_Table = cell2table(cell([Trials, 15]));
Sentence_Table(:, :) = {''};
Trial_Matrix = zeros(Trials, 3);

for Indx_T = 1:Trials
    % split sentences into single word per column
    Text = Session_Table.Sentence{Indx_T};
    Text_List = strsplit(Text);
    
    for Indx_W = 1:length(Text_List)
        Sentence_Table(Indx_T, Indx_W) = Text_List(Indx_W);
    end
    
    % save trial data
    Trial_Matrix(Indx_T, 1) = Session_Table.Trial_Number(Indx_T); % trial number
    Trial_Matrix(Indx_T, 2) = Session_Table.Run(Indx_T); % trial Run
    Trial_Matrix(Indx_T, 3) = Session_Table.Condition(Indx_T); % trial condition
end



%%% sets up new trial in figure
function Row = Set_Sentence(hObject, eventdata, handles, direction)
Rows = size(handles.Sentence_Table, 1);
Cols = size(handles.Sentence_Table, 2);

% selects either all trials, or unfinished trials
Tags = [[1:Rows]', handles.Score_Matrix(:, end)];
if get(handles.review_button, 'Value') % only gets trials that aren't saved
    Tags = Tags(Tags(:, 1) == handles.Sentence_Number | Tags(:, 2) < 2, 1);
    disp(['remaining sentences: ', num2str(length(Tags))])
else % gets all trials
    Tags = Tags(:, 1);
end


% based on command, advances or retreats from current trial
Indx = find(Tags == handles.Sentence_Number); % finds location of current trial
switch direction
    case 'previous'
        if Indx < 2
            Row = Tags(end); % loops to last trial
        else
            Row = Tags(Indx - 1);
        end
    case 'next'
        if Indx >= length(Tags)
            Row = Tags(1); % loops to first trial
        else
            Row = Tags(Indx + 1);
        end
    case 'first' % for load setting
        Row = Tags(1);
    case 'current'
        Row = str2double(get(handles.Sentence_Number_Box, 'String'));
end


% cycles through words to set up toggle buttons
for Col = 1:Cols
    % retrieves word string
    word = handles.Sentence_Table{Row, Col};
    word = word{1};
    
    % shows toggles only for words and not blanks
    if length(word) < 1
        set(handles.(['word_', num2str(Col), '_tgl']),'Visible', 'off')
    else
        set(handles.(['word_' num2str(Col), '_tgl']), 'String', word, 'Visible', 'on')
    end
    
    % unselects all toggles. Toggles get restored if saved to matrix
    set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 0)
end


% displays strings of neighboring trials
if Row >1
    Prev_Sent = strjoin(string(handles.Sentence_Table{Row-1, :}));
    set(handles.previous_sentence_box, 'String', Prev_Sent)
end

if Row < size(handles.Sentence_Table, 1)
    Next_Sent = strjoin(string(handles.Sentence_Table{Row+1, :}));
    set(handles.next_sentence_box, 'String', Next_Sent)
end


% displays current trial number in GUI
set(handles.Sentence_Number_Box, 'String', Row)


% toggles previously chosen words
for Col = 1:size(handles.Sentence_Table, 2)
    Box_Value = handles.Score_Matrix(Row, Col);
    if Box_Value == 2
        set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 1)
    else
        set(handles.(['word_' num2str(Col), '_tgl']), 'Value', 0)
    end
end

% toggle skip if skipped
Skipped = handles.Score_Matrix(Row, end);
if Skipped == 1
    set(handles.skip_button, 'Value', 1);
else
    set(handles.skip_button, 'Value', 0);
end


guidata(hObject, handles); %maybe not needed



%%% popup indicating end of scoring. lets save or continue editing
function End_Popup(hObject, eventdata, handles, Pop_String)

answer = questdlg([Pop_String, 'Click Edit if you still wish to make changes, or Save to finish.'], ...
    'Time to Save?', ...
    'Edit', 'Save', 'Edit');
switch answer
    case 'Edit'
        % review skipped
        set(handles.review_button, 'Value', 1)
        handles.Sentence_Number = Set_Sentence(hObject, eventdata, handles, 'first');
        guidata(hObject, handles);
    case 'Save'
        guidata(hObject, handles);
        Save_CSV(hObject, eventdata, handles)
end



%%% outputs new matrix with row based on selected words
function Matrix = Save_Row(hObject, eventdata, handles, Value)

Matrix = handles.Score_Matrix; % gets old matrix
Row = handles.Sentence_Number; % gets current trial number

% identifies selected words from toggles
for Col = 1:size(handles.Sentence_Table, 2)
    if strcmp(get(handles.(['word_' num2str(Col), '_tgl']), 'Visible'), 'on') % sees if word is even there
        Matrix(Row, Col) = get(handles.(['word_' num2str(Col), '_tgl']), 'Value') + 1; %gets toggled value per word
    end
end

Matrix(Row, end) = Value; % sets status to last column (2 if saved, 1 if skipped)
disp(Matrix) % visualizes matrix for debug purposes




% function saving new CSV
function Save_CSV(hObject, eventdata, handles)
Matrix = handles.Score_Matrix;

Correct = sum(Matrix(:, 1:end-1) == 2, 2); % counts how many correct words for every trial
Total = size(Matrix, 2) - 1 - sum(Matrix(:, 1:end-1) == 0, 2); % counts total words for every trial

% get old CSV
Old_CSV = readtable([handles.pathname, handles.filename]);
Old_CSV.Properties.VariableNames{1} = 'Trial_Number'; % in case theres CSV junk
Old_CSV = Old_CSV(Old_CSV.Condition ~= 8, :); % remove tactile only

% load new columns
Old_CSV.Correct = Correct;
Old_CSV.Total = Total;

% save new CSV
writetable(Old_CSV, [handles.pathname, extractBefore(handles.filename, '.csv'), '_Scored.csv'])
