[s, fs] = audioread('CGN_M_059.wav');

% filter signal
Filter_order = 3;
freq_range = 16;

[b,a]=butter(Filter_order,freq_range/(fs/2),'low');
envel=abs(hilbert(s));
envel=filtfilt(b,a,envel); % zero-phase filtering (doubles the filter order)

TotalTime = length(s)./fs;
t = 0:TotalTime/(length(s)):TotalTime-TotalTime/length(s);
t = t';

Carrier = sin(t*2*pi*100);
Tactile = Carrier .* envel; 

envel = (envel-min(envel))/(max(envel)-min(envel));
Tactile = (Tactile-min(Tactile))/(max(Tactile)-min(Tactile));
% plot(t, s, 'Color', [0.216 0.451 0.608], 'LineWidth', 1.5) %
plot(t, Tactile, 'Color', [0.216 0.451 0.608], 'LineWidth', 1.5) % 
xlim([0.22, 1.5])
% axis off