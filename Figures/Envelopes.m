%%% envelope
close all
[s, fs] = audioread('CGN_M_059.wav');

% filter signal
Filter_order = 3;
freq_range = 16;

[b,a]=butter(Filter_order,freq_range/(fs/2),'low');
envel=abs(hilbert(s));
envel=filtfilt(b,a,envel); % zero-phase filtering (doubles the filter order)

TotalTime = length(s)./fs;
t = 0:TotalTime/(length(s)):TotalTime-TotalTime/length(s);
t = t';

Carrier = sin(t*2*pi*100);
Tactile = Carrier .* envel; 

hold on
subplot(3, 1, 1)
s = (s-min(s))/(max(s)-min(s));
plot(t, s)
title('Speech Signal')
xlim([0.22, 1.5])
axis off

subplot(3, 1, 2)
envel = (envel-min(envel))/(max(envel)-min(envel));
plot(t, envel, 'LineWidth', 1.5)
title('Envelope')
xlim([0.22, 1.5])
ylim([-0.25, 1])
axis off

subplot(3, 1, 3)
Tactile = (Tactile-min(Tactile))/(max(Tactile)-min(Tactile));
plot(t, Tactile)
title('Envelope x 100 Hz Carrier')
xlim([0.22, 1.5])
axis off

hold off