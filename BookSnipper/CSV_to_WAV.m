%%% snips and saves wav files
close all
clear
tic

Main_Folder = 'C:\Users\Sophia\Projects\Internship\BookSnipper\';
Audio_Location = [Main_Folder, 'audio\CGN_wav\']; % location of audio files
CSV_Location = [Main_Folder, 'CGN_CSVs\'];
Snippet_Location = [Main_Folder, 'audio\CGN_Snips\'];

%%% Get filenames
Audio_File_Names = ls(Audio_Location);
Audio_File_Names = Audio_File_Names(3: end, :); % removes initial . and ..
CSV_File_Names = ls(CSV_Location);
CSV_File_Names = CSV_File_Names(3: end, 1:end);
Batches = size(CSV_File_Names, 1);

% load first audiobook
audiofile_open = Audio_File_Names(1, :);
[s, fs] = audioread([Audio_Location, audiofile_open]);

% create destination of audio snips
New_Location = [Snippet_Location, 'Batch_1\'];
if exist(New_Location, 'dir') == 0
    mkdir(New_Location)
end

for CSV = 1:Batches % loop through CSVs
    DataTable = readtable(strtrim([CSV_Location, CSV_File_Names(CSV, :)]));
    Rows = size(DataTable, 1);
    
    % creates new folder for every batch
    New_Location = [Snippet_Location, extractBefore(CSV_File_Names(CSV, :),'.csv'), '\'];
    if exist(New_Location, 'dir') == 0
        mkdir(New_Location)
    end
    
    for Row = 1:Rows % loop through sentences
        
        % load audiobook
        audiofile = [extractBefore(DataTable.Original_txt{Row}, '.txt'), '.wav']; % get associated audiobook name
        if not(strcmp(audiofile_open, audiofile)) % if the current sentence has a different audiobook from previous, get new audiobook
            audiofile_open = audiofile;
            [s, fs] = audioread([Audio_Location, audiofile_open]);
        end
        
        % get start and stop positions
        Start = round(fs*DataTable.Start_Time(Row));
        if Start < 1
            Start = 1;
        end
        End =  round(fs*DataTable.End_Time(Row));
        if End >= size(s, 1)
            End = size(s, 1);
        end
        new_audiofile = DataTable.New_Filename{Row}; % get new name
        
        % create new audiofile
        audiowrite([New_Location, new_audiofile], s(Start:End, 1), fs)
        
    end
    disp(CSV)
end

toc
