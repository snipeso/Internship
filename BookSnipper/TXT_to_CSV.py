import os
import pandas as pd
from Word_Freq import Common_Words

Main_Path = os.path.join(os.getcwd(), 'nl', '')
TXTs = os.listdir(Main_Path) # get all txt files with sentences
Cutoff_Frequency = 30000 #filter words that are in the top XX,XXX

####################################
### Parse text files and return dictionary of sentence: [min time, max time]
####################################

Full_Sentences = {}
Sentence_Location = {}
Counter = 0
for File in TXTs:
    with open(Main_Path + File, 'r') as f:
        full_text = f.read()

    # produce list of times and sentences
    full_text = full_text.split('0.000') # remove header, start from time 0
    body_text = max(full_text, key = len) # select main body (between 0 start time and 0 near end)
    body_text = body_text.replace('""', '<>').replace('"', '') #replace empty strings with noticeable character, and remove quotation marks from text
    body_texts = body_text.splitlines() # split based on \n, which is every number, and after every text string
    body_texts = list(filter(lambda x: x, body_texts)) # remove empty strings
    body_texts = ['0.000'] + body_texts # return time 0 to have uniform strings

    # Combine times with sentences and add to dictionary
    Number = [] # buffer of timestamps until end of sentence reached
    Sentence = [] # buffer of sentence parts until period reached
    Numbers = set('1234567890.') # recognize number
    Letters = set('abcdeéèëfghiïjklmnopqrstuvwxyz .') # recognize string, remove strings with capitals (names) or weird characters

    for Text in body_texts:
        if set(Text).issubset(Numbers): # adds number to buffer
            Number.append(float(Text))

        elif "." in Text[-1] and set(Text).issubset(Letters): # marks end of sentence
            Sentence.append(Text) # adds last string to buffer
            Key = ' '.join(Sentence) # creates complete sentence from strings in buffer
            Min_T = min(Number) # finds start time from buffer
            Max_T = max(Number) # finds end time

            Full_Sentences[Key] = [Min_T, Max_T] # adds sentence: [timestamps] to dictionary
            Sentence_Location[Key] = File

            Number = [] # empties buffers
            Sentence = []
        elif Text == "<>": # removes timestamps of pauses
            Number = Number[:-2]

        elif set(Text).issubset(Letters): # adds incomplete strings to buffer
            Sentence.append(Text)
        else: # resets buffer for anything else, like ending in ? or weird characters
            if Text[-1] in ["?", "!", "."]:
                Counter += 1 # maybe not needed?
            Number = []
            Sentence = []


#################################################
### Filter
#################################################
Words = set(Common_Words(Cutoff_Frequency)) # produces list of top XX,XXX words
Sentences_Dict_Redux = {}

for k in Full_Sentences:
    Text = k.replace('.', '').split(' ') # makes list of words

    Duration = 1.7 < (Full_Sentences[k][1] - Full_Sentences[k][0])  < 5
    Frequent = set(Text).issubset(Words) # condition if words are in selected frequency range
    First_Words = Text[0].lower() != 'en' and Text[0].lower() != 'maar' and Text[0].lower() != 'daar' and Text[0].lower() != 'er' and Text[0].lower() != 'met'
    Conditions = Frequent and First_Words and  Duration

    if Conditions:
        Sentences_Dict_Redux[k] = Full_Sentences[k]

print('Finished Selection')


#########################################################
### Save to CSVs
#########################################################

directory = os.path.join(os.getcwd(), 'CGN_CSVs', '')
if not os.path.exists(directory): # create folder
    os.makedirs(directory)

Rows = range(300) # number of rows per csv file
Cols = ['New_Filename', 'Original_txt', 'Start_Time', 'End_Time', 'Sentence']
df = pd.DataFrame(index=Rows, columns=Cols)

Indx = 0 #row within a batch
Batch = 1
for Sent in Sentences_Dict_Redux:
    df.loc[Indx, 'New_Filename'] = 'CGN_{}_{}.wav'.format(Batch, Indx+1)
    df.loc[Indx, 'Original_txt'] = Sentence_Location[Sent]
    df.loc[Indx, 'Start_Time'] = Sentences_Dict_Redux[Sent][0]
    df.loc[Indx, 'End_Time'] = Sentences_Dict_Redux[Sent][1]
    df.loc[Indx, 'Sentence'] = Sent

    if Indx == max(Rows): # start new csv
        df.to_csv(directory + 'Batch_{}.csv'.format(Batch))
        Indx = 0
        Batch += 1
        print(Batch)
    else:
        Indx += 1

#TODO: make sure each block and category is balanced for word frequency
# todo, if file same, keep old signal