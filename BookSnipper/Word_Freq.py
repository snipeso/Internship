import os

def Common_Words(Cutoff_Frequency):
    File_Path = os.path.join(os.getcwd(), 'totrank.txt')
    with open(File_Path, 'r') as f:
        full_text = f.read()

    Rows = full_text.split('\n')[2:]
    Characters = set('abcdeéèëfghiïjklmnopqrstuvwxyz .')

    Frequencies = []
    Common_Words_List = []
    for Row in Rows:
        Elems = Row.split()
        if len(Elems) != 3: # skip rows that aren't in the standard format of number, number, word
            continue
        elif set(Elems[2]).issubset(Characters): # take words that only contain lowercase common characters
            Elems[0] = int(Elems[0].replace(':', '')) # get rank as number
            Elems[1] = int(Elems[1]) # get absolute frequency

            if Elems[0]<Cutoff_Frequency:
                Common_Words_List.append(Elems[2]) # add words that are below cutoff rank
        else:
            continue


    return Common_Words_List


